# W800 LiteOS Cozylife SDK
W800开发板的LiteOS SDK，移植了OpenHarmony的liteos_m系统内核；并适配了Cozylife框架，可以快速开放并兼容Cozylife生态的各种产品，并且可以使用Cozylife App进行配网/远程控制。

适用于深圳四博智联生产的W800开发板，如下图所示：
![image_src](images/board.png)

## 1. 开发环境设置

### 1.1 Windows环境

1. 下载编译环境：链接：https://pan.baidu.com/s/1wL1PvbY3myPbG4dVCZsiOQ  提取码：6666
2. 解压上面下载的编译环境，将SDK即本仓库所有内容下载到MSYS/home/Administrator/w800_liteos_sdk目录下，如果没有w800_liteos_sdk目录，则手动创建
3. 进入第1步骤下载的目录中，双击打开msys.bat文件，弹出的控制台是一个Linux环境，使用cd命令进入w800_liteos_sdk目录下，执行make即可开始编译
4. 编译完成后，可烧录固件生成在w800_liteos_sdk/bin/w800目录下，文件名是w800.fls，直接使用联盛德官方烧录工具烧写入w800开发板即可

烧写工具下载地址：https://www.winnermicro.com/html/1/156/158/558.html

### 1.2 Linux环境

文档中使用的Linux系统版本为Ubuntu20
1. 下载csky工具链`csky-abiv2-elf-gcc`，本文档使用的工具链版本为csky-elfabiv2-tools-x86_64-minilibc-20210423.tar，可在平头哥芯片官方社区下载：[点击进入社区下载](https://occ.t-head.cn/community/download?id=3885366095506644992)
2. 解压下载的工具链到某个目录下，比如toolchain目录，那么解压后编译工具位于该目录下的bin文件夹内，将该路径导入到PATH环境变量中，执行下面命令：`export PATH=/user/toolchain/bin:$PATH`，注意这里的toolchain路径需要根据你的系统路径修改。
3. 下载本仓库代码，进入代码根目录直接执行`make`命令即可编译生成固件。

## 2. OpenHarmony的liteos_m内核分析与移植教程
请浏览本仓库的wiki页面：[liteos_m内核分析](https://gitee.com/nicholas3388/w800-liteos-sdk/wikis/)

## 3. Demo示例使用方法

在目录app/main.c文件中，宏定义DEMO_CONSOLE默认设置为1，即生成的固件运行联盛德官方的Demo示例。

1. 打开串口连接开发板，波特率115200
2. 通过串口发送字符串内容：t-scan()，可以实现WiFi扫描，扫描到周围的WiFi热点（发送内容时记得加换行）
3. 通过串口发送内容：t-connect("ssid","pwd")，可以实现设置w800为STA模式并连接指定的ssid，发送内容里面ssid和pwd表示连接的wifi名和密码，根据具体环境修改；
4. 通过串口发送：t-webcfg()，可以实现web方式的WiFi配网。发送该命令后，w800进入AP模式并启动webserver。然后我们打开手机WiFi设置可以查找到w800创建的热点，热点名类似“softap_xxxx”，其中xxxx是模组MAC地址后四位，如下图所示；连接到该热点，打开浏览器，输入192.168.1.1即可访问到webserver的配网页面，页面中输入WiFi账号密码点“save”即可完成配网，如下图所示。
5. 串口发送t-bt-on()，可以打开蓝牙；发送t-bt-off()，关闭蓝牙；只有打开蓝牙后，执行后续蓝牙命令才有效，否则w800无响应，例如打开蓝牙后，串口发送命令：t-ble-scan()可以执行蓝牙扫描功能，扫描周围蓝牙信号；

![image_src](images/webcfg.png)


下面是配网页面：

![image_src](images/webserver.png)

------------

手机端配网完成后，w800连接WiFi，输出连接成功信息，打印出本机IP，如下图所示
![image_src](images/connect.png)

------------

下面是蓝牙BLE扫描到的信号：
![image_src](images/bt_scan.png)

------------

## 4. Cozylife框架的使用

在目录app/main.c文件中，将宏定义DEMO_CONSOLE改为0，则UserMain函数将执行`switch_template_main`，这是将开发板初始化为一个Cozylife开关的主函数。

```c
void UserMain(void)
{
#if DEMO_CONSOLE
	CreateDemoTask();
#else
	printf("\n <--- Use dohome library ---> \n");
	switch_template_main();
#endif
}
```

### 4.1 Cozylife框架简介

Cozylife是一套强大的智能家居系统，包括了Cozylife固件开发框架，手机App和云平台。使用Cozylife框架能快速开发智能终端产品，并能接入Cozylife云平台，使用Cozylife App控制。本仓库的SDK适配兼容了Cozylife框架，该框架以libdohomelib.a静态库的方式提供给开发者使用，开发者只需更改应用层即可修改设备为不同的Cozylife终端产品。

Cozylife代码（app目录下的代码）走读：
* dohome_embed_templates： 该目录下是Cozylife应用层的代码，该代码中将设备设置成了一个WiFi+BLE的远程开关
* doit_sdk_for_w800： 该目录是硬件适配层，里面包含了Cozylife适配不同硬件平台的代码，当需要移植到不同硬件平台时，主要修改该目录下的代码
* include： 该目录包含了Cozylife所需的头文件

### 4.2 Cozylife对w800开发板外设的默认设置

在include目录下的doit_product_cfg.h文件中对设备进行外设和其他信息的设置。

* 源码中将w800开发板的PB8管脚设置为配网指示灯，w800开发板的PB8连接了一个蓝光LED，所以当固件运行时处于配网状态时可以看到蓝光LED每秒闪烁一次
* 代码将w800开发板PB5设置为开关的继电器控制管脚。当设备配网后，使用App远程控制开关设备时，可以使用万用表测量PB5脚，可以看到输出电平受远程控制，如果接入LED可以看到LED随着控制的变化。
* 代码将w800开发板的PB9设置为物理开关管脚，PB9正好是开发板TypeC接口旁边的一个物理按键，当设备下载了该固件并配网后，按下PB9按键可以看到Cozylife手机App上的对应设备开关状态随按键改变。

上面的GPIO设置都可以根据具体应用场景修改。

### 4.3 Cozylife手机App的使用简介

Cozylife手机App可以在苹果应用商店/华为/小米应用市场等各大应用商店免费下载使用。[点击下载Cozylife](https://cozylife.doiting.com/download/downLoad.html)

当设备下载本SDK固件后，打开App后，点击主界面右上角的“+”按钮根据提示，进行添加设备配网（同时支持WiFi或蓝牙配网）操作即可，设备配网完成即可远程控制。

![cozylife-img](images/cozylife-app.png)

------------

## 5. 如何更换操作系统

该SDK默认使用LiteOS操作系统，仍然兼容原有的FreeRTOS系统。在MSYS开发环境中进入SDK的`/src/liteos`目录下执行make命令，会编译生成liteos的库文件，在根目录的bin/build/w800/lib目录下生成libos.a静态库文件，将该文件拷贝覆盖根目录的lib/w800目录下的同名文件即可更换系统。同样的，进入根目录src/freertos目录执行make编译生成libos.a文件后替换lib目录下的同名文件即可更换为FreeRTOS。

------------

## 6. 关于W800开发板的更多教程

此W800开发板的更多教程请参考：[点击查看W800更多教程](https://www.showdoc.com.cn/1985164941528450/8994856430612187)

------------

## 7. 相关仓库
相关仓库：[LuaNode-w800](https://gitee.com/nicholas3388/lua-node-for-w800)