/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <stddef.h>
//#include "FreeRTOS.h"
//#include "task.h"
#include "los_task.h"
#include "nimble/nimble_port.h"

#define configMINIMAL_STACK_SIZE	( ( unsigned short ) 300 )
#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 31)

#if NIMBLE_CFG_CONTROLLER
static tls_os_task_t ll_task_h;
#endif
    
static tls_os_task_t host_task_h;

void
nimble_port_freertos_init(TSK_ENTRY_FUNC host_task_fn)
{
#if NIMBLE_CFG_CONTROLLER
    /*
     * Create task where NimBLE LL will run. This one is required as LL has its
     * own event queue and should have highest priority. The task function is
     * provided by NimBLE and in case of FreeRTOS it does not need to be wrapped
     * since it has compatible prototype.
     */
    /*xTaskCreate(nimble_port_ll_task_func, "ll", configMINIMAL_STACK_SIZE + 400,
                NULL, configMAX_PRIORITIES - 1, &ll_task_h);*/
	tls_os_task_create(&ll_task_h, "ll",
                       nimble_port_ll_task_func,
                       NULL,
                       NULL,
                       configMINIMAL_STACK_SIZE + 400,    
                       configMAX_PRIORITIES - 1,
                       0);
#endif

    /*
     * Create task where NimBLE host will run. It is not strictly necessary to
     * have separate task for NimBLE host, but since something needs to handle
     * default queue it is just easier to make separate task which does this.
     */
    /*xTaskCreate(host_task_fn, "ble", configMINIMAL_STACK_SIZE + 400,
                NULL, tskIDLE_PRIORITY + 1, &host_task_h);*/
	tls_os_task_create(&host_task_h, "ble",
                       host_task_fn,
                       NULL,
                       NULL,
                       configMINIMAL_STACK_SIZE + 400,    
                       15,
                       0);
}
