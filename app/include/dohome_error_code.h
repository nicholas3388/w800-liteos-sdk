
#ifndef DOHOME_LOGE_CODEOR_CODE_H
#define DOHOME_LOGE_CODEOR_CODE_H

#ifdef __cplusplus
extern "C" {
#endif
#include <dohome_type.h>

typedef DOHOME_UINT8_T dohome_op_ret;       // 操作结果返回值

#define OPRT_OK                             (0)        // 执行成功
#define OPRT_COM_ERROR                      (1)        // 通用错误
#define OPRT_INVALID_PARM                   (2)        // 无效参数
#define OPRT_MALLOC_FAILED                  (3)        // 内存分配失败
#define OPRT_NOT_SUPPORTED                  (4)        // 不支持
#define OPRT_NETWORK_ERROR                  (5)
#define OPRT_HALTIMER_INIT_ERR              (6)
#define OPRT_HALTIMER_START_ERR             (7)
#define OPRT_HALTIMER_STOP_ERR              (8)
#define OPRT_SOFTTIMER_INIT_ERR             (9)
#define OPRT_TASK_ID_ISEXIST                (10)
#define OPRT_TASK_ID_ISZERO                 (11)
#define OPRT_MD5_GEN_ERR                    (12)        
#define OPRT_DOHOME_PARSE_CMD_ERR             (13)        
#define OPRT_DOHOME_TCP_SERVER_INIT_ERR       (14)        
#define OPRT_DOHOME_TCP_SERVER_NOTINIT_ERR    (15)     
#define OPRT_DOHOME_TCP_SERVER_SEND_ERR       (16)     
#define OPRT_DOHOME_TCP_CLI_CONNECT_ERR       (17) 
#define OPRT_DOHOME_TCP_CLI_DNS_ERR           (18)
#define OPRT_DOHOME_TCP_CLI_POOLISFULL_ERR    (19)     // 连接池满了
#define OPRT_DOHOME_TCP_CLI_SEND_ERR          (20)     
#define OPRT_DOHOME_TCP_CLI_CLS_ERR           (21)  
#define OPRT_DOHOME_TLSF_MEM_ADDPOOL_ERR      (22) 
#define OPRT_CMD_SN_OVERDUE                 (23)
#define OPRT_OS_ADAPTER_INVALID_PARM        (24)
#define OPRT_OS_ADAPTER_OK                  (25)
#define OPRT_OS_ADAPTER_BT_INIT_FAILED      (26)
#define OPRT_OTA_FAILED                     (27)
#define OPRT_OTA_HEADER_FAILED              (28)
#define OPRT_OTA_TOOLONG_FAILED             (29)
#define OPRT_OTA_FILE_ERR                   (30)
#define OPRT_FLASH_INIT_ERR                 (31)
#define OPRT_FLASH_WRITE_ERR                (32)
#define OPRT_FLASH_READ_ERR                 (33)
#define OPRT_FLASH_ERASE_ERR                (34)
#define OPRT_TIMERPOOL_FULL_ERR             (35)
#define OPRT_TIMER_NOTFOUND_ERR             (36)
#define OPRT_MAIN_PID_TOOSHORT_ERR          (37)
#define OPRT_MAIN_PID_TOOLONG_ERR           (38)
#define OPRT_STR_TO_HEXBUF_CONVT_ERROR      (39)        // 转换错误
#define OPRT_UINT_TO_HEXSTR_CONVT_ERROR     (40)        // 转换错误

enum loge_code{
    LOGE_CODE_MEM_ERR,    
    LOGE_CODE_PARM_ERR,    
    LOGE_CODE_NOT_INIT,
    LOGE_CODE_NOT_SUPPORT,
    LOGE_CODE_DEV_INFO_ERR,
    LOGE_CODE_STR_LEN_ERR,
    LOGE_CODE_STR_DATA_ERR,
    LOGE_CODE_JSON_PARSE_ERR,
    LOGE_CODE_JSON_CREATE_ERR,
    LOGE_CODE_JSON_FORMATT_ERR,
    LOGE_CODE_BLE_NOT_MAC,
    LOGE_CODE_BLE_DATA_LEN_ERR,    
    LOGE_CODE_BLE_DATA_CRC_ERR,
    LOGE_CODE_PROTOCOL_ROOT_NULL,
    LOGE_CODE_PROTOCOL_SN_NULL,
    LOGE_CODE_PROTOCOL_SN_ERR,
    LOGE_CODE_PROTOCOL_CMD_NULL,
    LOGE_CODE_PROTOCOL_CMD_ERR,
    LOGE_CODE_PROTOCOL_PV_NULL,
    LOGE_CODE_PROTOCOL_PV_ERR,
    LOGE_CODE_PROTOCOL_MSG_NULL,
    LOGE_CODE_PROTOCOL_MSG_TYPE_ERR,
    LOGE_CODE_PROTOCOL_MSG_DATA_ERR,
    LOGE_CODE_HTTP_URL_ERR,
};


#ifdef __cplusplus
}
#endif
#endif

