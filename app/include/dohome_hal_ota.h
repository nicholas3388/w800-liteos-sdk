#ifndef _DOHOME_HAL_OTA_H_
#define _DOHOME_HAL_OTA_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

DOHOME_UINT8_T dohome_hal_ota_get_schedule(void);

dohome_op_ret dohome_hal_ota_update(DOHOME_CHAR_T *buf, DOHOME_SIZE_T len);

dohome_op_ret dohome_hal_ota_finish(void);

dohome_op_ret dohome_hal_ota_begin(void);

dohome_op_ret dohome_hal_ota_end(void);

#endif