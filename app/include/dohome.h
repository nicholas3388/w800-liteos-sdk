#ifndef __DOHOME_H__
#define __DOHOME_H__

#include <dohome_type.h>

#include <dohome_lib_cfg.h>

#define DT_LIB_MAX_DT_PROTOCOL_VERSION        0
#define DT_PROTOCOL_VERSION         DOHOME_USE_PROTOCL_VERSION
#if DOHOME_USE_PROTOCL_VERSION > DT_LIB_MAX_DT_PROTOCOL_VERSION
    #error "DOHOME_USE_PROTOCL_VERSION not support"
#endif



//standing
#include <string.h>
#include <stdarg.h>
// dohome lib header file
#include <dohome_main.h>
#include <dohome_log.h>
#include <dohome_api.h>
#include <dohome_error_code.h>
#include <dohome_lib/dohome_event.h>
#include <dohome_lib/dohome_time.h>

#include <dohome_lib/dohome_mem.h>
#include <dohome_lib/dohome_shell.h>
#include <dohome_lib/dohome_utils.h>
#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))
#include <dohome_lib/dohome_wifi_device.h>
#include <dohome_lib/dohome_tcp_server.h>
#include <dohome_lib/dohome_cloud.h>
#include <dohome_lib/dohome_wifi.h>
#include <dohome_lib/dohome_tcp_client.h>
#include <dohome_lib/dohome_http_func.h>
#include <dohome_lib/dohome_http_ota.h>
#include <dohome_lib/dohome_remote_parse_001.h>
#include <dohome_lib/dohome_http_client.h>
#include <dohome_lib/dohome_udp.h>
#include <dohome_hal_network.h>
#include <dohome_hal_wifi.h>
#include <dohome_lib/dohome_wifi_dev_main.h>
#include <dohome_lib/dohome_udp_log.h>
#include <cJSON/cJSON.h>
#if (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE)
#include <dohome_lib/dohome_ble.h>
#include <dohome_hal_ble.h>
#endif 
#endif
#if (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
#include <dohome_lib/dohome_ble.h>
#include <dohome_lib/dohome_cloud.h>
#include <dohome_lib/dohome_ble_device.h>
#include <dohome_lib/dohome_ble_dev_main.h>
#include <dohome_hal_ble.h>
#endif 
// dohome hal header file
#include <dohome_hal_timer.h>
#include <dohome_hal_storge.h>
#include <dohome_hal_system.h>
#include <dohome_hal_rtc.h>
#include <dohome_hal_network.h>
#include <dohome_hal_md5.h>
#include <dohome_hal_ota.h>

// ext lib header file
#include <easyflash.h>
#include <dohome_lib/dohome_kvs.h>
#include <dohome_libc.h>


#endif // #ifndef __DOHOME_H__