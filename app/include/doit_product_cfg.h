#ifndef _DOIT_PRODUCT_CFG_H_
#define _DOIT_PRODUCT_CFG_H_

//Konfig
#define CONFIG_CL_PRODUCT_ID "h4x0b7"
#define CONFIG_CL_SWITCH_TYPE 1
#define CONFIG_CL_SWITCH_NUM 1
#define CONFIG_CL_SWITCH_RELAY_PIN1 21 /*WM_IO_PB_05*/ //22
#define CONFIG_CL_SWITCH_KEY_PIN1 25 /*WM_IO_PB_09*/ //21
#define CONFIG_CL_SWITCH_POWER_STATE_PIN1 14
#define CONFIG_CL_SWITCH_WIFI_STATE_PIN 24 /*WM_IO_PB_08*/ //21
#define CONFIG_CL_SWITCH_WIFI_STATE_WIFICFG_ALWAYS_FLASH 1
#define CONFIG_CL_SWITCH_RELAY_ACTIVE_LEVEL 1
#define CONFIG_CL_SWITCH_KEY_ACTIVE_LEVEL 0
#define CONFIG_CL_SWITCH_STATE_ACTIVE_LEVEL 0
#define CONFIG_CL_SWITCH_KEY_CONTROL_LONG_TIME_MS 3000
#define CONFIG_CL_SWITCH_KEY_CONFIG 1
#define CONFIG_CL_SWITCH_FIRST_WIFI_CFG_STATUS 1
#define CONFIG_CL_SWITCH_POWER_ON_MEMORY 1
#define CONFIG_CL_HARDWARE_VERSION "0.0.1"
#define CONFIG_CL_LOOP_PERIOD 20
#define CONFIG_CL_LOW_POWER_MODE 1
#define CONFIG_CL_UPDATA_STATUS_NOW_TIME_MS 400
#define CONFIG_CL_SAVE_STATUS_NOW_TIME_MS 800
#define CONFIG_CL_UPDATA_STATUS_DELAY_TIME_MS 2000
#define CONFIG_CL_SAVE_STATUS_DELAY_TIME_MS 2000
#define CONFIG_CL_SWITCH_TYPE_PLUG 1
#define CONFIG_CL_CHIP_TYPE_BL602L 1
#define CONFIG_CL_BL602L_MODULE_BL02_1M 1
#define CONFIG_CL_CHIP_TYPE 102
#define MODULE CL_BL602L_MODULE_BL02_1M

#define CONFIG_USER_SW_VER "1.0.1"

#include <dohome_type.h>

/*******************************************************************
 * 版本                              
 *******************************************************************/
#define DT_SOFTWARE_VERSION             CONFIG_USER_SW_VER
#define DT_HARDWARE_VERSION             CONFIG_CL_HARDWARE_VERSION


/*******************************************************************
 * 定义                              
 *******************************************************************/

// 不使用GPIO
#define GPIO_NOT_USE                        0xFF 

#define DOIT_SWITCH_TYPE_PLUG                 1
#define DOIT_SWITCH_TYPE_ONOFF                2
#define DOIT_SWITCH_TYPE_CONTROL_PANEL        3

#define POWER_MEASURE_TYPE_HLW8012          1              
#define POWER_MEASURE_TYPE_BL0937           2    

#define DOIT_CONTROL_TYPE_ON                1
#define DOIT_CONTROL_TYPE_OFF               2
#define DOIT_CONTROL_TYPE_SWITCH            3

/*******************************************************************
 * 系统配置                              
 *******************************************************************/

// 系统运行loop周期
#define DOIT_LOOP_PERIOD                    CONFIG_CL_LOOP_PERIOD     

#ifdef CONFIG_CL_LOW_POWER_MODE
//是否启用低功耗模式
#define CONFIG_WIFI_SLEEP                   1
#endif     
         

// 立即上报产品状态的延时时间: N*200ms
#define CONFIG_UPDATA_STATUS_NOW_TICK       (CONFIG_CL_UPDATA_STATUS_NOW_TIME_MS/200)
// 立即保存产品状态的延时时间: N*200ms
#define CONFIG_SAVE_STATUS_NOW_TICK         (CONFIG_CL_SAVE_STATUS_NOW_TIME_MS/200)
// 延迟上报产品状态的延时时间: N*200ms
#define CONFIG_UPDATA_STATUS_DELAY_TICK     (CONFIG_CL_UPDATA_STATUS_DELAY_TIME_MS/200)
// 延迟保存产品状态的延时时间: N*200ms
#define CONFIG_SAVE_STATUS_DELAY_TICK       (CONFIG_CL_SAVE_STATUS_DELAY_TIME_MS/200)


/*******************************************************************
 * 用户配置                              
 *******************************************************************/

// 产品PID
#define DT_PRODUCT_ID                               CONFIG_CL_PRODUCT_ID 

// 开关数量 
#define CONFIG_SWITCH_NUM                           CONFIG_CL_SWITCH_NUM

// 继电器配置
    // 继电器引脚
#define CONFIG_RELAY_PIN1                           CONFIG_CL_SWITCH_RELAY_PIN1
#ifdef CONFIG_CL_SWITCH_RELAY_PIN2
#define CONFIG_RELAY_PIN2                           CONFIG_CL_SWITCH_RELAY_PIN2
#endif
#ifdef CONFIG_CL_SWITCH_RELAY_PIN3
#define CONFIG_RELAY_PIN3                           CONFIG_CL_SWITCH_RELAY_PIN3
#endif
#ifdef CONFIG_CL_SWITCH_RELAY_PIN4
#define CONFIG_RELAY_PIN4                           CONFIG_CL_SWITCH_RELAY_PIN4
#endif
    // 继电有效电平
#define CONFIG_GPIO_POWER_ACTIVE_LEVEL              CONFIG_CL_SWITCH_RELAY_ACTIVE_LEVEL

#define  CONFIG_CL_SWITCH_USE_WIFI_STATE //设置wifi指示灯

// 按键配置
    // 按键引脚
#if CONFIG_CL_SWITCH_KEY_PIN1 != -1
#define CONFIG_KEY_PIN1                             CONFIG_CL_SWITCH_KEY_PIN1
#else
#define CONFIG_KEY_PIN1                             GPIO_NOT_USE
#endif
#ifdef CONFIG_CL_SWITCH_KEY_PIN2
#if CONFIG_CL_SWITCH_KEY_PIN2 != -1
#define CONFIG_KEY_PIN2                             CONFIG_CL_SWITCH_KEY_PIN2
#else
#define CONFIG_KEY_PIN2                             GPIO_NOT_USE
#endif
#endif
#ifdef CONFIG_CL_SWITCH_KEY_PIN3
#if CONFIG_CL_SWITCH_KEY_PIN3 != -1
#define CONFIG_KEY_PIN3                             CONFIG_CL_SWITCH_KEY_PIN3
#else
#define CONFIG_KEY_PIN3                             GPIO_NOT_USE
#endif
#endif
#ifdef CONFIG_CL_SWITCH_KEY_PIN4
#if CONFIG_CL_SWITCH_KEY_PIN4 != -1
#define CONFIG_KEY_PIN4                             CONFIG_CL_SWITCH_KEY_PIN4
#else
#define CONFIG_KEY_PIN4                             GPIO_NOT_USE
#endif
#endif
    // 按键有效电平
#define CONFIG_GPIO_KEY_ACTIVE_LEVEL                CONFIG_CL_SWITCH_KEY_ACTIVE_LEVEL

// 指示灯配置
    // Power指示灯引脚配置 
#if CONFIG_CL_SWITCH_POWER_STATE_PIN1 != -1
#define CONFIG_POWER_STATE_PIN1                     CONFIG_CL_SWITCH_POWER_STATE_PIN1
#else
#define CONFIG_POWER_STATE_PIN1                     GPIO_NOT_USE
#endif
#ifdef CONFIG_CL_SWITCH_RELAY_PIN2
#if CONFIG_CL_SWITCH_POWER_STATE_PIN2 != -1
#define CONFIG_POWER_STATE_PIN2                     CONFIG_CL_SWITCH_POWER_STATE_PIN2
#else
#define CONFIG_POWER_STATE_PIN2                     GPIO_NOT_USE
#endif
#endif
#ifdef CONFIG_CL_SWITCH_RELAY_PIN3
#if CONFIG_CL_SWITCH_POWER_STATE_PIN3 != -1
#define CONFIG_POWER_STATE_PIN3                     CONFIG_CL_SWITCH_POWER_STATE_PIN3
#else
#define CONFIG_POWER_STATE_PIN3                     GPIO_NOT_USE
#endif
#endif
#ifdef CONFIG_CL_SWITCH_RELAY_PIN4
#if CONFIG_CL_SWITCH_POWER_STATE_PIN4 != -1
#define CONFIG_POWER_STATE_PIN4                     CONFIG_CL_SWITCH_POWER_STATE_PIN4
#else
#define CONFIG_POWER_STATE_PIN4                     GPIO_NOT_USE
#endif
#endif

#ifdef CONFIG_CL_SWITCH_USE_ALL_CONTROL
#define CONFIG_ALL_CONTROL_ENABLE                   1
#define CONFIG_ALL_CONTROL_RELAY_PIN                CONFIG_CL_SWITCH_ALL_CONTROL_RELAY_PIN
#define CONFIG_ALL_CONTROL_KEY_PIN                  CONFIG_CL_SWITCH_ALL_CONTROL_KEY_PIN
#else
#define CONFIG_USB_CONTROL_ENABLE                   0
#endif

#ifdef CONFIG_CL_SWITCH_USE_USB
#define CONFIG_USB_CONTROL_ENABLE                   1
#define CONFIG_USB_CONTROL_TYPE                     CONFIG_CL_SWITCH_USB_CONTROL_TYPE
#define CONFIG_USB_CONTROL_RELAY_PIN                CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN
#ifdef CL_SWITCH_USB_CONTROL_KEY_PIN
#define CONFIG_USB_CONTROL_KEY_PIN                  CONFIG_CL_SWITCH_USB_CONTROL_KEY_PIN
#else
#define CONFIG_USB_CONTROL_KEY_PIN                  GPIO_NOT_USE
#endif
#else
#define CONFIG_USB_CONTROL_ENABLE                   0
#endif

#ifdef CONFIG_CL_SWITCH_USE_BACKLIGHT
#define CONFIG_BACKLIGHT_ENABLE                     1
#define CONFIG_BACKLIGHT_PIN                        CONFIG_CL_SWITCH_BACKLIGHT_PIN
#define CONFIG_BACKLIGHT_ACTIVE_LEVEL               CONFIG_CL_SWITCH_BACKLIGHT_ACTIVE_LEVEL
#else
#define CONFIG_BACKLIGHT_ENABLE                     0
#endif

#ifdef CONFIG_CL_SWITCH_USE_WIFI_STATE
    //WiFi指示灯引脚配置 
#define CONFIG_WIFI_STATE_PIN                       CONFIG_CL_SWITCH_WIFI_STATE_PIN
#else
#define CONFIG_WIFI_STATE_PIN                       GPIO_NOT_USE
#endif

#ifdef CONFIG_CL_SWITCH_WIFI_STATE_REVERSE
#define CONFIG_WIFI_STATE_REVERSE                   1
#else
#define CONFIG_WIFI_STATE_REVERSE                   0
#endif
    // 指示灯有效电平
#define CONFIG_GPIO_STATE_ACTIVE_LEVEL              CONFIG_CL_SWITCH_STATE_ACTIVE_LEVEL

#ifdef CONFIG_CL_SWITCH_KEY_CONFIG
#define CONFIG_KEY_CONTROL_ENABLE                   1
//按键长按时间
#define CONFIG_KEY_CONTROL_LONG_TIME_MS             CONFIG_CL_SWITCH_KEY_CONTROL_LONG_TIME_MS
#define BTN_FUN_LONG_1                              btn_remote_fun_start_wifi_config();
#define BTN_FUN_LONG_2                              btn_remote_fun_start_wifi_config();
#define BTN_FUN_LONG_3                              btn_remote_fun_start_wifi_config();
#define BTN_FUN_LONG_4                              btn_remote_fun_start_wifi_config();
#else
#define CONFIG_KEY_CONTROL_ENABLE                   0
#endif

// 开关多次进行配网
#ifdef CONFIG_CL_POWER_SWITCH_CONFIG
#define CONFIG_USE_POWER_SWITCH_CONFIG              1
// 开关次数 
#define CONFIG_POWER_SWITCH_COUNT                   CONFIG_CL_POWER_SWITCH_CONFIG_COUNT
#else
#define CONFIG_USE_POWER_SWITCH_CONFIG              0
#endif


// 默认配置
    // 是否开始开关记忆
#ifdef CONFIG_CL_SWITCH_POWER_ON_MEMORY
#define CONFIG_DEFAULT_POWER_ON_MEMORY              1
#else 
#define CONFIG_DEFAULT_POWER_ON_MEMORY              0
#endif
    // 第一次是否进入配网模式
#ifdef CONFIG_CL_SWITCH_FIRST_WIFI_CFG_STATUS
#define CONFIG_DEFAULT_WIFI_CONFIG_MODE             1
#else 
#define CONFIG_DEFAULT_WIFI_CONFIG_MODE             0
#endif

#ifdef CONFIG_CL_SWITCH_USE_PLATE_CONTROL
#define CONFIG_ONOFF_PLATE_CONTROL_ENABLE           1
#define PLATE_CODE_1                                CONFIG_CL_SWITCH_PLATE_CONTROL_PIN
#ifdef CONFIG_CL_SWITCH_USE_PLATE_CONFIG
// 开关多次进行配网
#define CONFIG_USE_ONOFF_PLATE_CONFIG               1
// 开关次数 
#define CONFIG_ONOFF_PLATE_CFG_COUNT                CONFIG_CL_SWITCH_PLATE_CONFIG_COUNT
// 开关次数 
#define CONFIG_ONOFF_PLATE_CFG_CLS_TIME_MS          CONFIG_CL_SWITCH_PLATE_CONFIG_CLS_TIME
#else
#define CONFIG_USE_ONOFF_PLATE_CONFIG               0
#endif
#endif


#ifdef CONFIG_CL_SWITCH_USE_WOL
#define CONFIG_WOL_ENABLE                           1
#else
#define CONFIG_WOL_ENABLE                           0
#endif


#ifdef CONFIG_CL_SWITCH_USE_POWER_MEASURE                 
#define CONFIG_POWER_MEASURE_ENABLE                 1
#define CONFIG_POWER_MEASURE_TYPE                   CONFIG_CL_SWITCH_POWER_MEASURE_TYPE
#if ((CONFIG_POWER_MEASURE_TYPE == POWER_MEASURE_TYPE_HLW8012) || (CONFIG_POWER_MEASURE_TYPE == POWER_MEASURE_TYPE_BL0937))
#define CONFIG_POWER_MEASURE_CF_GPIO                            CONFIG_CL_SWITCH_POWER_MEASURE_CF_GPIO
#define CONFIG_POWER_MEASURE_CF1_GPIO                           CONFIG_CL_SWITCH_POWER_MEASURE_CF1_GPIO
#define CONFIG_POWER_MEASURE_SEL_GPIO                           CONFIG_CL_SWITCH_POWER_MEASURE_SEL_GPIO 
#endif 

#ifdef CONFIG_CL_SWITCH_USE_POWER_MEASURE_OVERCURRENT
#define CONFIG_POWER_MEASURE_OVERCURRENT_ENABLE                 1
#define CONFIG_POWER_MEASURE_OVERCURRENT_VALUE                  CONFIG_CL_SWITCH_POWER_MEASURE_OVERCURRENT_VALUE
#else  
#define CONFIG_POWER_MEASURE_OVERCURRENT_ENABLE                 0
#endif

#ifdef CONFIG_CL_SWITCH_USE_POWER_MEASURE_CORRECT
#define CONFIG_POWER_MEASURE_ENABLE_CORRECT                     1
#ifndef CONFIG_CL_SWITCH_POWER_MEASURE_REPEAT_CORRECT
#define CONFIG_POWER_MEASURE_REPEAT_CORRECT                     0
#endif
#define CONFIG_POWER_MEASURE_DEFAULT_CORRECT_VOLTAGE_EXPECTED   CONFIG_CL_SWITCH_POWER_MEASURE_CORRECT_VOLTAGE_EXPECTED
#define CONFIG_POWER_MEASURE_DEFAULT_CORRECT_CURRENT_EXPECTED   CONFIG_CL_SWITCH_POWER_MEASURE_CORRECT_CURRENT_EXPECTED
#define CONFIG_POWER_MEASURE_DEFAULT_CORRECT_POWER_EXPECTED     CONFIG_CL_SWITCH_POWER_MEASURE_CORRECT_POWER_EXPECTED
#define CONFIG_POWER_MEASURE_DEFAULT_CORRECT_FAULT_TOLERANT     CONFIG_CL_SWITCH_POWER_MEASURE_CORRECT_FAULT_TOLERANT
#define CONFIG_POWER_MEASURE_DEFAULT_CORRECT_TIME               CONFIG_CL_SWITCH_POWER_MEASURE_CORRECT_TIME
#endif
#endif

#if (CONFIG_CL_SWITCH_TYPE == DOIT_SWITCH_TYPE_ONOFF || CONFIG_CL_SWITCH_TYPE == DOIT_SWITCH_TYPE_CONTROL_PANEL)
#define HOMEKIT_PROJECT_CID                         8  
#endif

//supplement

#endif