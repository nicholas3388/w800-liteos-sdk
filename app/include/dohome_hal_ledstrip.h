#ifndef _DOIT_HAL_LED_STRIP_H_
#define _DOIT_HAL_LED_STRIP_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

typedef union {
    struct {
        uint8_t red;
        uint8_t green;
        uint8_t blue; //LSB
        uint8_t white;
    };
    uint32_t color; // 0xWWRRGGBB
} pixel_led_t;

typedef enum {
#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))
  PIXEL_RGB = 12,
  PIXEL_RGBW = 16
#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)  
  PIXEL_RGB = 24,
  PIXEL_RGBW = 8
#endif   
} pixeltype_t;


typedef enum {
  COLOR_GRB = 0,
  COLOR_RGB,
  COLOR_GBR,
  COLOR_RBG,
  COLOR_BRG,
  COLOR_BGR
} pixel_color_type_t;

dohome_op_ret dohome_hal_ledstrip_set_pixels_num(DOHOME_UINT16_T pixels_num);

dohome_op_ret dohome_hal_ledstrip_set_color_type(pixel_color_type_t color_type);

pixel_color_type_t dohome_hal_ledstrip_get_color_type(void);

dohome_op_ret dohome_hal_ledstrip_set_gpio(DOHOME_UINT16_T gpio);

dohome_op_ret dohome_hal_ledstrip_init(DOHOME_UINT16_T pixels_num, pixeltype_t type, pixel_color_type_t color_type);

void dohome_hal_ledstrip_update(pixel_led_t *pixels);

#ifdef	__cplusplus
}
#endif

#endif
