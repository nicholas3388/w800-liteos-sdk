#ifndef _DOIT_REMOTE_H_
#define _DOIT_REMOTE_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

typedef enum
{
    REMOTE_CTRL_PRESS_START = 0,
    REMOTE_CTRL_PRESS_CLICK,
    REMOTE_CTRL_PRESS_REPEAT,
    REMOTE_CTRL_PRESS_DOUBLE_CLICK,
    REMOTE_CTRL_PRESS_REPEAT_CLICK,
    REMOTE_CTRL_PRESS_LONG_START,
    REMOTE_CTRL_PRESS_LONG_UP,
    REMOTE_CTRL_PRESS_NONE,
} remote_ctrl_event_t;

typedef void (*remote_ctrl_cb_t)(remote_ctrl_event_t event, DOHOME_UINT16_T remote_addr, DOHOME_UINT16_T remote_code, DOHOME_INT16_T repeat);

void dohome_rfd_init(dohome_gpio_t gpio, remote_ctrl_cb_t callback);
void dohome_infrared_init(dohome_gpio_t gpio, remote_ctrl_cb_t callback);

#endif
