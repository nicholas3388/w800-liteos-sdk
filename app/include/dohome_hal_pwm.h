#ifndef DOHOME_HAL_PWM_H
#define DOHOME_HAL_PWM_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#ifdef __cplusplus
extern "C" {
#endif



void dohome_hal_pwm_stop(dohome_pwm_gpio_t gpio);
void dohome_hal_pwm_start(dohome_pwm_gpio_t gpio);

void dohome_hal_pwm_set_duty(dohome_pwm_gpio_t gpio, DOHOME_UINT32_T duty);

void dohome_hal_pwm_init(dohome_pwm_gpio_t gpio, DOHOME_UINT32_T freq, DOHOME_UINT32_T duty);

void dohome_hal_pwm_set_max_duty(DOHOME_UINT32_T duty);

#ifdef __cplusplus
}
#endif
#endif
