#ifndef _DOHOME_TIMER_H_
#define _DOHOME_TIMER_H_

#include <dohome_type.h>

void dohome_time_set_time(DOHOME_UINT32_T tm);
DOHOME_UINT32_T dohome_time_get_time(void);

void dohome_time_init(void);

#endif