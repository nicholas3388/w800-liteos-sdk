#ifndef _DOHOME_EVENT_H_
#define _DOHOME_EVENT_H_

#include "dohome_api.h"

dohome_op_ret dohome_event_post(dohome_event_type_t event_type, DOHOME_UINT16_T event_id, void* event_data);

#endif