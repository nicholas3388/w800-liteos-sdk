#ifndef __DOHOME_BLE_H__
#define __DOHOME_BLE_H__

#include <dohome_type.h>
#include <dohome_error_code.h>

#include <dohome_hal_ble.h>

#define DOHOME_PROTOCOL_VER01        0x01
// 注意丢包处理和乱序处理
#define DOHOME_PROTOCOL01_HEADER_SN(x)              ((x)&0x7F)       
#define DOHOME_PROTOCOL01_HEADER_LAST_FLAG(x)       ((x)&0x80)

#define DOHOME_PROTOCOL01_FIXED_DATASIZE        17

#define DT_DEV_PID_LEN          6
#define DT_DEV_MAC_LEN          6
#define DT_DEV_KEY_LEN          10
#define DT_DEV_DID_LEN          10

#define DOHOME_BLE_GET_CMD(a)           (*(a))
#define DOHOME_BLE_GET_LEN(a)           (*(a))
#define DOHOME_BLE_GET_DATA_POINT(a)    ((a)+1)
typedef struct {
    DOHOME_UCHAR_T link_flag_len;//0
    DOHOME_UCHAR_T link_flag_t;//1
    DOHOME_UCHAR_T link_flag_d;//2

    DOHOME_UCHAR_T service_uuid_len;//3
    DOHOME_UCHAR_T service_uuid_t;//4
    DOHOME_UCHAR_T service_uuid_dh;//5
    DOHOME_UCHAR_T service_uuid_dl;//6

    DOHOME_UCHAR_T service_data_len;//7
    DOHOME_UCHAR_T service_data_t;//8
    DOHOME_UCHAR_T service_name_h;//9
    DOHOME_UCHAR_T service_name_l;//10

    // dev id data
    DOHOME_UCHAR_T protocol_ver;//11
    DOHOME_UCHAR_T dev_type;//12

    DOHOME_UCHAR_T pid[DT_DEV_PID_LEN];//13 14 15 16 17 18

    // mac
    DOHOME_UCHAR_T dev_mac[DT_DEV_MAC_LEN];//19 20 21 22 23 24
    
    DOHOME_UCHAR_T end[31-25];
}dohome_dev_adv_dat_t;


typedef struct {
    DOHOME_UCHAR_T dev_name_len;//0
    DOHOME_UCHAR_T dev_name_t;//1
    DOHOME_UCHAR_T dev_name_h;//2
    DOHOME_UCHAR_T dev_name_l;//3

    DOHOME_UCHAR_T data_len;//4
    DOHOME_UCHAR_T data_t;//5
    DOHOME_UCHAR_T binded_flag;//6

#if  (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE)
    DOHOME_UCHAR_T dev_key[DT_DEV_KEY_LEN];
    DOHOME_UCHAR_T software_ver_major;
    DOHOME_UCHAR_T software_ver_minor;
    DOHOME_UCHAR_T software_ver_patch;
    DOHOME_UCHAR_T end[31-20];
#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
    DOHOME_UCHAR_T dev_id[DT_DEV_DID_LEN];//7 8 9 10 11 12 13 14 15 16
    DOHOME_UCHAR_T pid[DT_DEV_PID_LEN];//17 18 19 20 21 22
    DOHOME_UCHAR_T dev_mac[DT_DEV_MAC_LEN];//23 24 25 26 27 28
    DOHOME_UCHAR_T end[31-29];
#endif
}dohome_dev_rsp_data_t;

typedef struct {
    DOHOME_UCHAR_T protocol_ver;//0
    DOHOME_UCHAR_T pkg_sn;//1
    DOHOME_UCHAR_T data[17];
    DOHOME_UCHAR_T crc8;//
}dohome_protocol01_req_data_t;

typedef struct {
    DOHOME_UCHAR_T pkg_sn[17];
    DOHOME_UCHAR_T data[17][17];
}dohome_protocol01_rt_data_t;

typedef struct {
    DOHOME_UCHAR_T len;
    DOHOME_UINT32_T timestamp;
}__attribute__ ((packed))dohome_protocol01_devinfo_req_t;


typedef struct {
    DOHOME_UCHAR_T cmd;
    DOHOME_UCHAR_T len;
    DOHOME_UCHAR_T res;
    DOHOME_UCHAR_T dev_id[10];
    DOHOME_UCHAR_T dev_type; //wifi = 0x00/ble = 0x01/wifi+ble = 0x02
    DOHOME_UINT32_T software_ver;
    DOHOME_UINT32_T hardware_ver;
    DOHOME_UCHAR_T dev_mac[6];
    DOHOME_UCHAR_T pid[6];
}__attribute__ ((packed))dohome_protocol01_devinfo_rsp_t;


typedef struct {
    DOHOME_UCHAR_T len;
    DOHOME_UCHAR_T ssid[32];
    DOHOME_UCHAR_T passwd[64];
    DOHOME_UCHAR_T bssid[6];
    DOHOME_UCHAR_T ble_dev_key[10];
    DOHOME_UCHAR_T token[33];
    uint32_t lat;
    uint32_t lng;
    DOHOME_UCHAR_T domain[10];
}__attribute__ ((packed))dohome_protocol01_wifiinfo_req_t;

// 小端字节序  回复配网信息请求
typedef struct {
    DOHOME_UCHAR_T cmd;
    DOHOME_UCHAR_T len;
    DOHOME_UCHAR_T res;
}__attribute__ ((packed))dohome_protocol01_wifiinfo_rsp_t;

#if  (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)

typedef struct {
    DOHOME_UCHAR_T len;
    DOHOME_UINT32_T timestamp;
}__attribute__ ((packed))dohome_protocol01_hearbeat_req_t;

typedef struct {
    DOHOME_UCHAR_T cmd;
    DOHOME_UCHAR_T len;
    DOHOME_UINT32_T timestamp;
}__attribute__ ((packed))dohome_protocol01_hearbeat_rsp_t;

typedef struct {
    DOHOME_UCHAR_T len;
    DOHOME_UCHAR_T set;
}__attribute__ ((packed))dohome_protocol01_factoryreset_req_t;

typedef struct {
    DOHOME_UCHAR_T cmd;
    DOHOME_UCHAR_T len;
    DOHOME_UCHAR_T res;
}__attribute__ ((packed))dohome_protocol01_factoryreset_rsp_t;
#endif

dohome_op_ret dohome_ble_init(void);
dohome_op_ret dohome_ble_deinit(void);
void ble_send_notify_pkg(DOHOME_UCHAR_T *buf, DOHOME_INT16_T len);
#if  (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE)
DOHOME_UINT8_T dohome_ble_connect_status(void);

void dohome_ble_reg_event_handler(void);

void dohome_ble_loop(DOHOME_UINT16_T interval);

#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)

extern dohome_op_ret dohome_get_devatt_cmd_cb(DOHOME_CHAR_T *data);
extern dohome_op_ret dohome_set_devatt_cmd_cb(DOHOME_UINT8_T pkg_sn, void *data);

void dohome_ble_start_config(void);
void dohome_wifi_start_config_and_reboot(void);

void dohome_ble_set_boot_start_config(DOHOME_UINT8_T need);

void dohome_ble_adv_start_config(void);
void dohome_ble_adv_stop_config(void);

DOHOME_UINT8_T dohome_ble_has_config(void);
DOHOME_UINT8_T dohome_ble_has_boot_start_config(void);

DOHOME_UINT8_T dohome_ble_has_config_mode(void);
dohome_op_ret dohome_ble_close_config_mode(void);

void dohome_ble_config_loop(void);

void dohome_ble_connect_loop(void);

#endif


#endif // #ifndef __DOHOME_BLE_H__