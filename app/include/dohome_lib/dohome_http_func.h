#ifndef _DOHOME_HTTP_FUNC_H_
#define _DOHOME_HTTP_FUNC_H_

#include <dohome.h>


typedef void (*dohome_http_cb_t)(DOHOME_INT_T resp_code, DOHOME_UINT8_T *buf, DOHOME_UINT16_T len);

dohome_op_ret dohome_http_dev_bind(dh_dev_admin_info_t *admin, dohome_http_cb_t http_bind_cb);
dohome_op_ret dohome_http_dev_unbind(dohome_http_cb_t http_unbind_cb);
dohome_op_ret dohome_http_dev_bind_check(dohome_http_cb_t http_bind_check_cb);
#endif