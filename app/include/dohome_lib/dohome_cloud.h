#ifndef _DOHOME_CLOUD_PRARE_H_
#define _DOHOME_CLOUD_PRARE_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

//------------------
typedef DOHOME_INT8_T dh_cmd_tp_e;
#define DT_CMD_DEVICE_INFO          0
#define DT_CMD_SET_WIFI             1
#define DT_CMD_SET_BLE              1
#define DT_CMD_GET_DEVICE_STATUS    2
#define DT_CMD_SET_DEVICE_STATUS    3
#define DT_CMD_FACTORY_RESET        4
#define DT_CMD_OTA                  5
#define DT_CMD_HEARTBEAT_INFO       8
#define DT_CMD_DEBUG                9
#define DT_CMD_POST_DEVICE_STATUS   10
#define DT_CMD_DEV_BIND             11
#define DT_CMD_WIFI_CFG_STATUS      12
#define DT_CMD_NULL                -1

typedef DOHOME_INT8_T dh_cmd_origin_e;
#define DOHOME_CMD_ORIGIN_UDP          0
#define DOHOME_CMD_ORIGIN_TCPS         1
#define DOHOME_CMD_ORIGIN_TCPC         2

dohome_op_ret dohome_cloud_post_all_dps(void);
dohome_op_ret dohome_cloud_cmd(dh_cmd_origin_e origin, DOHOME_CHAR_T *data, DOHOME_CHAR_T data_len, DOHOME_CHAR_T *res_data, DOHOME_UINT16_T *res_len, dh_cmd_tp_e *cmd_num);

dohome_op_ret dohome_cloud_post_factory_msg(void);
#endif
