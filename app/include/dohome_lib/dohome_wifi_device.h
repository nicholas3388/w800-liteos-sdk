#ifndef _DOHOME_WIFI_DEVICE_H_
#define _DOHOME_WIFI_DEVICE_H_

#include <dohome_type.h>

#define DOHOME_DOMIAN_NAME_MAX_LEN                  48
#define DOHOME_SECOND_DOMIAN_NAME_MAX_LEN           10
#define DOHOME_ADMIN_TOKEN_BUFSIZE                  31

#define DOHOME_WIFI_DEV_DEVID_BUFSIZE               21
#define DOHOME_WIFI_DEV_DEVKEY_BUFSIZE              33
#define DOHOME_WIFI_DEV_STATION_MAC_BUFSIZE         6
#define DOHOME_WIFI_DEV_STATION_MAC_STR_BUFSIZE     13
#define DOHOME_WIFI_DEV_DEF_NAME_BUFSIZE            32
#define DOHOME_WIFI_DEV_MSG_MAXLEN                  1024


typedef struct{
    DOHOME_UINT8_T is_bind;
    DOHOME_CHAR_T token[DOHOME_ADMIN_TOKEN_BUFSIZE];
    DOHOME_UINT16_T lat;
    DOHOME_UINT16_T lng;
    DOHOME_UINT32_T uid;
}dh_dev_admin_info_t;

typedef struct{
    char host[DOHOME_DOMIAN_NAME_MAX_LEN];
    short port;
}dh_dev_tcp_info_t;


typedef void (*dh_dev_bind_cb_t)(dohome_op_ret ret_code, DOHOME_UINT8_T status);
typedef void (*dh_dev_unbind_cb_t)(dohome_op_ret ret_code, DOHOME_UINT8_T status);
typedef void (*dh_dev_bind_check_cb_t)(dohome_op_ret ret_code, DOHOME_UINT8_T status);

void dohome_device_get_sta_mac(DOHOME_UINT8_T *mac);
DOHOME_CHAR_T *dohome_device_get_sta_mac_str(void);

void dohome_device_get_ip_str(DOHOME_CHAR_T *ip);

dohome_op_ret dohome_device_save_dev_id(DOHOME_CHAR_T *did);
void dohome_dev_start_bind(void);

DOHOME_CHAR_T *dohome_device_get_dev_id(void);
DOHOME_CHAR_T *dohome_device_get_dev_key(void);

DOHOME_CHAR_T *dohome_device_get_default_name();
void dohome_device_set_dev_name(DOHOME_CHAR_T *name);

dohome_op_ret dohome_device_save_admin_info(dh_dev_admin_info_t *admin_info);
dohome_op_ret dohome_device_read_admin_info(dh_dev_admin_info_t *admin_info);
dh_dev_admin_info_t *dohome_device_get_admin_info(void);

dohome_op_ret dohome_device_save_tcp_info(dh_dev_tcp_info_t *tcp_info);
dohome_op_ret dohome_device_read_tcp_info(dh_dev_tcp_info_t *tcp_info);
dh_dev_tcp_info_t *dohome_device_get_tcp_info(void);

dohome_op_ret dohome_device_bind_admin(dh_dev_admin_info_t *admin, dh_dev_bind_cb_t bind_cb);
dohome_op_ret dohome_device_unbind_admin(dh_dev_unbind_cb_t dev_unbind_cb);

dohome_op_ret dohome_device_save_second_level_domain(DOHOME_CHAR_T *domain);
DOHOME_CHAR_T *dohome_device_get_api_domain(void);

void dohome_device_info_init(void);
void dohome_dev_unbind_loop(void);
void dohome_dev_bind_check_loop(void);
DOHOME_UINT8_T dohome_dev_has_unbind(void);
void dohome_dev_bind_loop(void);
void dohome_dev_start_bind(void);
void dohome_dev_stop_bind(void);
void dohome_ota_loop(void);
void dohome_dev_start_unbind(void);
void dohome_dev_start_unbind1(DOHOME_UINT8_T reset);

#endif // _DOHOME_WIFI_DEVICE_H_