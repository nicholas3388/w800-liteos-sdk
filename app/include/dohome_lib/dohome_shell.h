#ifndef _DOHOME_SHELL_H_
#define _DOHOME_SHELL_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

#if (defined(SHELL_ENABLE) && SHELL_ENABLE == 1)

extern void *dohome_shell;

void dohome_shell_print(DOHOME_CHAR_T *buffer, DOHOME_UINT16_T len);

dohome_op_ret dohome_shell_handler(DOHOME_CHAR_T c);

dohome_op_ret dohome_shell_init(void);

#endif

#endif