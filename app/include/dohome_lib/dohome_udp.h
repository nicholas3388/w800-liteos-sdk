#ifndef _DOHOME_UDP_H_
#define _DOHOME_UDP_H_

dohome_op_ret dohome_udp_init(void);

#if (defined(DOHOME_CFG_NETWORK_USE_TASK) && DOHOME_CFG_NETWORK_USE_TASK == 1)
dohome_op_ret dohome_udp_send(dohome_upd_dest_t dest, DOHOME_CHAR_T *data, DOHOME_UINT16_T len);
#endif

#endif