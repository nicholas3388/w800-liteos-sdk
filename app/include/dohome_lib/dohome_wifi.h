#ifndef _DOHOME_WIFI_H_
#define _DOHOME_WIFI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <dohome.h>
#include <dohome_hal_wifi.h>

typedef struct{
	/* data */
	DOHOME_CHAR_T ssid[SSID_MAX_LEN];
	DOHOME_CHAR_T passwd[PASSWD_MAX_LEN];
}dohome_wifi_info_t;

typedef enum {
    DOHOME_WIFI_CONFIG_SUCCESS = 0,
    DOHOME_WIFI_CONFIG_FAIL
}dohome_wifi_cfg_status_t;

typedef void (*dohome_wifi_config_cb_t)(dohome_wifi_cfg_status_t status); 

DOHOME_UINT8_T dohome_wifi_get_config_status(void);
void dohome_wifi_set_config_status(DOHOME_UINT8_T status);

dohome_op_ret dohome_wifi_get_connect_ap_rssi(DOHOME_INT8_T *rssi);

DOHOME_UINT8_T dohome_wifi_get_connect_status(void);

DOHOME_UINT8_T dohome_wifi_has_config(void);
DOHOME_UINT8_T dohome_wifi_has_boot_start_config(void);

void dohome_wifi_set_boot_start_config(DOHOME_UINT8_T need);

dohome_op_ret dohome_wifi_setup_ap(void);
dohome_op_ret dohome_wifi_setup_sta(void);

void dohome_wifi_start_connect(void);
void dohome_wifi_start_config(void);
void dohome_wifi_start_config_and_reboot(void);

void dohome_wifi_config_stop(void);

dohome_op_ret dohome_wifi_set_info(DOHOME_CHAR_T *ssid, DOHOME_CHAR_T *passwd);

void dohome_wifi_connect_check(void);

void dohome_wifi_config_loop(void);

void dohome_wifi_init(dohome_wifi_event_cb_t user_wifi_event_cb);
void dohome_wifi_connect_loop(void);
DOHOME_UINT8_T dohome_wifi_has_config_mode(void);

void dohome_wifi_config_is_get_info(void);

void dohome_wifi_config_set_timeout(DOHOME_UINT16_T seconds);

#ifdef __cplusplus
}
#endif

#endif