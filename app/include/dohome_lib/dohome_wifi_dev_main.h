#ifndef _DOHOME_WIFI_DEV_MAIN_H_
#define _DOHOME_WIFI_DEV_MAIN_H_

#include <dohome_type.h>

void dohome_device_get_sta_ip_str(DOHOME_UINT8_T *ip);
void dohome_dev_start_bind_check(void);
void dohome_dev_start_bind(void);
void dohome_wifi_main(void);

#endif // _DOHOME_WIFI_DEV_MAIN_H_