#ifndef DOHOME_MEM_H
#define DOHOME_MEM_H

/***
 *        _   _    ____      _______   _    _   _____    ______              _____       _____              ______   ______ 
 *       | \ | |  / __ \    |__   __| | |  | | |  __ \  |  ____|     /\     |  __ \     / ____|     /\     |  ____| |  ____|
 *       |  \| | | |  | |      | |    | |__| | | |__) | | |__       /  \    | |  | |   | (___      /  \    | |__    | |__   
 *       | . ` | | |  | |      | |    |  __  | |  _  /  |  __|     / /\ \   | |  | |    \___ \    / /\ \   |  __|   |  __|  
 *       | |\  | | |__| |      | |    | |  | | | | \ \  | |____   / ____ \  | |__| |    ____) |  / ____ \  | |      | |____ 
 *       |_| \_|  \____/       |_|    |_|  |_| |_|  \_\ |______| /_/    \_\ |_____/    |_____/  /_/    \_\ |_|      |______|
 *                                                                                                                          
 *                                                                                                                          
 */
#ifdef __cplusplus
extern "C" {
#endif
#include <dohome_cfg.h>
#include <dohome_type.h>
#if DOHOME_LIB_MEM_POOL_NAME == DOHOME_LIB_MEM_POOL_XMEM
#include "xtypes.h"
#include "xmem.h"
#elif DOHOME_LIB_MEM_POOL_NAME == DOHOME_LIB_MEM_POOL_TLSF
#include <tlsf/tlsf.h>
#elif !defined(DOHOME_LIB_MEM_POOL_NAME)
#error "please set DOHOME_LIB_MEM_POOL_NAME on dohome_cfg.h"
#endif

#ifdef DOHOME_MEM_ISGLOBAL
#define DOHOME_MEM_GLOBAL_FLAG extern
#else
#define DOHOME_MEM_GLOBAL_FLAG
#endif

DOHOME_MEM_GLOBAL_FLAG \
    dohome_op_ret dohome_tlsfmem_init(void);
DOHOME_MEM_GLOBAL_FLAG \
    dohome_op_ret dohome_tlsfmem_add_pool(void *mem, DOHOME_SIZE_T bytes);
DOHOME_MEM_GLOBAL_FLAG \
    DOHOME_SIZE_T dohome_tlsfmem_get_used_size(void);
DOHOME_MEM_GLOBAL_FLAG \
    void* dohome_tlsfmem_malloc(DOHOME_SIZE_T bytes);
DOHOME_MEM_GLOBAL_FLAG \
    void* dohome_tlsfmem_realloc(void* ptr, DOHOME_SIZE_T size);
DOHOME_MEM_GLOBAL_FLAG \
    void *dohome_tlsfmem_calloc(DOHOME_SIZE_T nelem, DOHOME_SIZE_T elem_size);
DOHOME_MEM_GLOBAL_FLAG \
    dohome_op_ret dohome_tlsfmem_free(void* p);
DOHOME_MEM_GLOBAL_FLAG \
    dohome_op_ret dohome_tlsfmem_deinit(void);
DOHOME_MEM_GLOBAL_FLAG \
    DOHOME_SIZE_T dohomememe_tlsfmem_get_free_size(void);
DOHOME_MEM_GLOBAL_FLAG \
    void dohome_tlsfmem_remove_pool(void *mem);

extern void *dohome_hal_mem_alloc(DOHOME_UINT32_T size);
extern void dohome_hal_mem_free(void *p);
#ifdef __cplusplus
}
#endif
#endif

