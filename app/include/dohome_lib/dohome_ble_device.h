#ifndef _DOHOME_DEVICE_INFO_H_
#define _DOHOME_DEVICE_INFO_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

#define DOHOME_DOMIAN_NAME_MAX_LEN   48
#define DOHOME_ADMIN_TOKEN_BUFSIZE         31

#if  (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE) 
#define DOHOME_WIFI_DEV_DEVID_BUFSIZE               21
#define DOHOME_WIFI_DEV_DEVKEY_BUFSIZE              33
#define DOHOME_WIFI_DEV_STATION_MAC_BUFSIZE         6
#define DOHOME_WIFI_DEV_STATION_MAC_STR_BUFSIZE     13
#define DOHOME_WIFI_DEV_DEF_NAME_BUFSIZE            14
#define DOHOME_WIFI_DEV_MSG_MAXLEN                  1024

#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
#define DOHOME_BLE_DEV_DEVID_BUFSIZE                21
#define DOHOME_BLE_DEV_DEVKEY_BUFSIZE               21
#define DOHOME_BLE_DEV_DEVBIND_BUFSIZE              11
#define DOHOME_BLE_DEV_MAC_BUFSIZE                  6
#define DOHOME_BLE_DEV_BLE_MAC_STR_BUFSIZE          13

#define DOHOME_BLE_DEV_DEVBIND_SU                   "bind_succe"
#define DOHOME_BLE_DEV_DEVBIND_FA                   "bind_fail"
#endif


typedef struct{
    DOHOME_UINT8_T is_bind;
    DOHOME_CHAR_T token[DOHOME_ADMIN_TOKEN_BUFSIZE];
    DOHOME_UINT16_T lat;
    DOHOME_UINT16_T lng;
    DOHOME_UINT32_T uid;
}dh_dev_admin_info_t;

typedef struct{
    char host[DOHOME_DOMIAN_NAME_MAX_LEN];
    short port;
}dh_dev_tcp_info_t;


typedef void (*dh_dev_bind_cb_t)(dohome_op_ret ret_code, DOHOME_UINT8_T status);
typedef void (*dh_dev_unbind_cb_t)(dohome_op_ret ret_code, DOHOME_UINT8_T status);
typedef void (*dh_dev_bind_check_cb_t)(dohome_op_ret ret_code, DOHOME_UINT8_T status);

void dohome_device_get_sta_mac(DOHOME_UINT8_T *mac);
DOHOME_CHAR_T *dohome_device_get_sta_mac_str(void);

dohome_op_ret dohome_device_save_dev_id(DOHOME_CHAR_T *did);
dohome_op_ret dohome_device_read_dev_id(void);
DOHOME_CHAR_T *dohome_device_get_dev_id(void);
DOHOME_CHAR_T *dohome_device_get_dev_key(void);

void dohome_device_info_init(void);
DOHOME_UINT8_T dohome_dev_has_unbind(void);
#if  (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE) 
DOHOME_CHAR_T *dohome_device_get_default_name();

dohome_op_ret dohome_device_save_admin_info(dh_dev_admin_info_t *admin_info);
dohome_op_ret dohome_device_read_admin_info(dh_dev_admin_info_t *admin_info);
dh_dev_admin_info_t *dohome_device_get_admin_info(void);

dohome_op_ret dohome_device_save_tcp_info(dh_dev_tcp_info_t *tcp_info);
dohome_op_ret dohome_device_read_tcp_info(dh_dev_tcp_info_t *tcp_info);
dh_dev_tcp_info_t *dohome_device_get_tcp_info(void);

dohome_op_ret dohome_device_bind_admin(dh_dev_admin_info_t *admin, dh_dev_bind_cb_t bind_cb);
dohome_op_ret dohome_device_unbind_admin(dh_dev_unbind_cb_t dev_unbind_cb);

void dohome_dev_unbind_loop(void);
void dohome_dev_bind_check_loop(void);

void dohome_dev_bind_loop(void);
void dohome_ota_loop(void);


#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)

dohome_op_ret dohome_device_save_dev_key(DOHOME_CHAR_T *dkey);
dohome_op_ret dohome_device_read_dev_key(void);

dohome_op_ret dohome_device_save_dev_bind(DOHOME_CHAR_T *dbind);
dohome_op_ret dohome_device_read_dev_bind(void);
DOHOME_CHAR_T *dohome_device_get_dev_bind(void);

DOHOME_CHAR_T *dohome_device_get_dev_mac(void);

void dohome_dev_unbind_start_remove(void);
DOHOME_UINT8_T dohome_dev_unbind_remove_check(void);
void dohome_dev_unbind_remove(void);

void dohome_dev_unbind_start_reset(void);
void dohome_dev_unbind_reset(void);

void dohome_dev_unbind_loop(void);

#endif

#endif