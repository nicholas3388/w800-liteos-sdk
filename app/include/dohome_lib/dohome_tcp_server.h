#ifndef _DOHOME_TCP_SERVER_H_
#define _DOHOME_TCP_SERVER_H_

#include <dohome_type.h>
#ifdef __cplusplus
    extern "C" {
#endif
#ifdef DOHOME_TCP_SERVER_ISGLOBAL
#define DOHOME_TCP_SERVER_GLOBAL_FLAG extern
#else
#define DOHOME_TCP_SERVER_GLOBAL_FLAG 
#endif

DOHOME_TCP_SERVER_GLOBAL_FLAG \
    dohome_op_ret dohome_tcp_server_init(void);
DOHOME_TCP_SERVER_GLOBAL_FLAG \
    dohome_op_ret dohome_tcps_send_cmd_msg(DOHOME_CHAR_T *msg, DOHOME_UINT16_T msg_len);

#ifdef __cplusplus
}
#endif

#endif // _DOHOME_TCP_SERVER_H_