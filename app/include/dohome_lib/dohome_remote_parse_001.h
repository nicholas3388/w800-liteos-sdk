#ifndef _DOHOME_DATA_PARSE_001_H_
#define _DOHOME_DATA_PARSE_001_H_

#include <dohome_type.h>
#include <dohome_hal_network.h>
#ifdef __cplusplus
    extern "C" {
#endif
#ifdef DOHOME_DATA_PARSE_001_ISGLOBAL
#define DOHOME_DATA_PARSE_001_GLOBAL_FLAG extern
#else
#define DOHOME_DATA_PARSE_001_GLOBAL_FLAG 
#endif

typedef void (*dohome_remote_parse_cb_t)(DOHOME_CHAR_T res, DOHOME_CHAR_T *data, DOHOME_INT16_T len); 

DOHOME_DATA_PARSE_001_GLOBAL_FLAG \
    dohome_op_ret dohome_lib_tcp_cli_init(void);
DOHOME_DATA_PARSE_001_GLOBAL_FLAG \
    dohome_op_ret dohome_lib_tcp_cli_time_cb(void);
DOHOME_DATA_PARSE_001_GLOBAL_FLAG \
    void remote_data_parse_v001(void *arg,DOHOME_CONST DOHOME_CHAR_T *buf,DOHOME_UINT16_T len);

DOHOME_DATA_PARSE_001_GLOBAL_FLAG \
    void dohome_pv001_set_keep_cb(dohome_remote_parse_cb_t parse_cb);
DOHOME_DATA_PARSE_001_GLOBAL_FLAG \
    void dohome_pv001_set_publish_cb(dohome_remote_parse_cb_t parse_cb);
DOHOME_DATA_PARSE_001_GLOBAL_FLAG \
    void dohome_pv001_set_subscribe_cb(dohome_remote_parse_cb_t parse_cb);
#ifdef __cplusplus
}
#endif

#endif // _DOHOME_DATA_PARSE_001_H_