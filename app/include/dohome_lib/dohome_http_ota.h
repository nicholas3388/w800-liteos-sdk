#ifndef _DOHOME_OTA_FUNC_H_
#define _DOHOME_OTA_FUNC_H_

#include <dohome.h>

typedef enum {
    DH_OTA_START = 0,
    DH_OTA_GET_IP,
    DH_OTA_CONNECTD,
    DH_OTA_GET_DTAT_START,
    DH_OTA_GET_DTAT_FINISH,
    DH_OTA_SUCCESS,
    DH_OTA_FAIL
}dohome_ota_event_e;


typedef void (*dohome_ota_event_cb_t)(dohome_ota_event_e evnet);

DOHOME_UINT8_T dohome_ota_get_schedule(void);

dohome_op_ret dohome_ota_http_url(DOHOME_CHAR_T *url, dohome_ota_event_cb_t event_cb);

#endif