#ifndef _DOHOME_UTILS_H_
#define _DOHOME_UTILS_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

DOHOME_UCHAR_T dohome_ble_crc_high_first(DOHOME_UCHAR_T *ptr, DOHOME_UCHAR_T len);

dohome_op_ret dohome_utils_str_is_ip(DOHOME_CHAR_T *str);

dohome_op_ret dohome_utils_str_to_ip(DOHOME_CONST DOHOME_CHAR_T* str, void *ip);

dohome_op_ret dohome_utils_str_to_u64(DOHOME_CHAR_T *str, DOHOME_UINT64_T *value);

dohome_op_ret dohome_utils_u64_to_str(DOHOME_UINT64_T num, DOHOME_CHAR_T *str);

dohome_op_ret dohome_utils_uint32_to_str(DOHOME_UINT32_T num, DOHOME_CHAR_T *str);

dohome_op_ret dohome_utils_hexstr_to_uint(DOHOME_UINT8_T *str, DOHOME_UINT16_T len , DOHOME_UINT32_T *res);

dohome_op_ret dohome_utils_version_to_int(DOHOME_CHAR_T *str, DOHOME_UINT32_T *value);

DOHOME_SIZE_T dohome_safe_strlen(DOHOME_CONST DOHOME_CHAR_T *str, DOHOME_SIZE_T max_len);

dohome_op_ret dohome_safe_strcpy(DOHOME_CHAR_T *dest, DOHOME_CONST DOHOME_CHAR_T *src, DOHOME_SIZE_T max_size);
#endif