#ifndef _DOHOME_UDP_LOG_H_
#define _DOHOME_UDP_LOG_H_

dohome_op_ret dohome_udplog_set_ip_port(DOHOME_CHAR_T *ip_str, DOHOME_UINT16_T port);
void dohome_udplog_open(void);
void dohome_udplog_close(void);
void dohome_udplog_reset_default(void);
dohome_op_ret dohome_udplog_parm_save(void);
#endif // _DOHOME_UDP_LOG_H_