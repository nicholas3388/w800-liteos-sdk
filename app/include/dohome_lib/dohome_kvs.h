#ifndef _DOHOME_KVS_H_
#define _DOHOME_KVS_H_

#include <ef_def.h>

#include <dohome_type.h>

dohome_op_ret dohome_kvs_init(void);

DOHOME_SIZE_T dohome_kvs_get_env_blob(DOHOME_CONST DOHOME_CHAR_T *key, void *value_buf, DOHOME_SIZE_T buf_len, DOHOME_SIZE_T *saved_value_len);

DOHOME_CHAR_T dohome_kvs_get_env_obj(DOHOME_CONST DOHOME_CHAR_T *key, env_node_obj_t env);

DOHOME_SIZE_T dohome_kvs_read_env_value(env_node_obj_t env, DOHOME_UINT8_T *value_buf, DOHOME_SIZE_T buf_len);

dohome_op_ret dohome_kvs_set_env_blob(DOHOME_CONST DOHOME_CHAR_T *key, DOHOME_CONST void *value_buf, DOHOME_SIZE_T buf_len);

DOHOME_CHAR_T *dohome_kvs_get_env(DOHOME_CONST DOHOME_CHAR_T *key);

dohome_op_ret dohome_kvs_set_env(DOHOME_CONST DOHOME_CHAR_T *key, DOHOME_CONST DOHOME_CHAR_T *value);

dohome_op_ret dohome_kvs_del_env(DOHOME_CONST DOHOME_CHAR_T *key);

void dohome_kvs_print_env(void);

void dohome_kvs_del_all(void);

#endif