#ifndef _DOHOME_TCP_CLIENT_H_
#define _DOHOME_TCP_CLIENT_H_

#include <dohome_type.h>
#include <dohome_hal_network.h>
#ifdef __cplusplus
    extern "C" {
#endif
#ifdef DOHOME_TCP_CLIENT_ISGLOBAL
#define DOHOME_TCP_CLIENT_GLOBAL_FLAG extern
#else
#define DOHOME_TCP_CLIENT_GLOBAL_FLAG 
#endif


DOHOME_TCP_CLIENT_GLOBAL_FLAG \
    dohome_op_ret dohome_lib_tcp_cli_init(void);
DOHOME_TCP_CLIENT_GLOBAL_FLAG \
    dohome_op_ret dohome_lib_tcp_cli_time_cb(void);
DOHOME_TCP_CLIENT_GLOBAL_FLAG \
    dohome_op_ret dohome_tcp_client_start_conn(void);
DOHOME_TCP_CLIENT_GLOBAL_FLAG \
    dohome_op_ret dohome_tcpc_send_cmd_msg(char *msg, int msg_len);

void dohome_tcpc_loop(DOHOME_UINT16_T interval);
#ifdef __cplusplus
}
#endif

#endif // _DOHOME_TCP_CLIENT_H_