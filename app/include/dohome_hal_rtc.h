#ifndef _DOHOME_HAL_RTC_H_
#define _DOHOME_HAL_RTC_H_

#include <dohome_error_code.h>

void dohome_hal_rtc_set_time(DOHOME_UINT32_T time);

DOHOME_UINT32_T dohome_hal_rtc_get_time(void);

void dohome_hal_rtc_init(void);

#endif