#ifndef DOHOME_TYPES_H
#define DOHOME_TYPES_H

#include <stdint.h>
#ifdef __cplusplus
extern "C" {
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif


#define dohome_gpio_t       uint8_t 
#define dohome_pwm_gpio_t       uint8_t

#define DOHOME_CONST          const
#define DOHOME_STATIC         static
#define DOHOME_INLINE         inline
#define DOHOME_VOLATILE       volatile

typedef signed int				ptrdiff_t;


typedef unsigned char   DOHOME_UINT8_T;
typedef unsigned char   DOHOME_UCHAR_T;
typedef signed char     DOHOME_INT8_T;
typedef char            DOHOME_CHAR_T;
typedef unsigned int    DOHOME_UINT_T;
typedef unsigned int    DOHOME_UINT16_T;
typedef unsigned long int    DOHOME_UINT32_T;
typedef double          DOHOME_DOUBLE_T;
typedef float           DOHOME_FLOAT_T;
typedef int             DOHOME_INT_T;
typedef int             DOHOME_INT16_T;
typedef unsigned long long int        DOHOME_UINT64_T;

typedef unsigned int    size_t;
typedef size_t          DOHOME_SIZE_T;

#ifdef __cplusplus
}
#endif

#endif // DOHOME_TYPES_H