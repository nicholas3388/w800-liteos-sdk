#ifndef _DOHOME_HAL_STORGE_H_
#define _DOHOME_HAL_STORGE_H_

#include "dohome_cfg.h"
#include <dohome_error_code.h>

void dohome_hal_storge_init(void);

dohome_op_ret dohome_hal_storge_read(DOHOME_UINT32_T addr, DOHOME_UINT32_T *buf, size_t size);
dohome_op_ret dohome_hal_storge_write(DOHOME_UINT32_T addr,DOHOME_CONST DOHOME_UINT32_T *buf, size_t size);
dohome_op_ret dohome_hal_storge_erase(DOHOME_UINT32_T addr, size_t size);

void dohome_hal_storge_lock(void);
void dohome_hal_storge_unlock(void);

dohome_op_ret dohome_hal_storge_verify_init(void);
dohome_op_ret dohome_hal_storge_verify_write(DOHOME_UINT8_T *verify, DOHOME_UINT16_T len);
dohome_op_ret dohome_hal_storge_verify_read(DOHOME_UINT8_T *verify, DOHOME_UINT16_T len);


#endif