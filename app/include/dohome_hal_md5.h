#ifndef DOHOME_MD5_API_H
#define DOHOME_MD5_API_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#ifdef __cplusplus
extern "C" {
#endif
#ifdef DOHOME_MD5_API_ISGLOBAL
#define DOHOME_MD5_API_GLOBAL_FLAG extern
#else
#define DOHOME_MD5_API_GLOBAL_FLAG
#endif

DOHOME_MD5_API_GLOBAL_FLAG \
    dohome_op_ret dohome_hal_md5_gen(DOHOME_INT8_T *input,DOHOME_INT8_T ilen,DOHOME_UINT8_T output[16]);

#ifdef __cplusplus
}
#endif
#endif
