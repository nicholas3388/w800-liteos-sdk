#ifndef DOHOME_MAIN_H
#define DOHOME_MAIN_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#ifdef __cplusplus
extern "C" {
#endif

DOHOME_CHAR_T *dohome_lib_get_product_sv(void);
DOHOME_CHAR_T *dohome_lib_get_product_hv(void);
DOHOME_CHAR_T *dohome_lib_get_product_pid(void);

dohome_op_ret dohome_lib_main(DOHOME_CHAR_T *pid);
DOHOME_UINT16_T dohome_lib_get_loop_period(void);

DOHOME_UINT8_T dohome_lib_is_verify(void);
#ifdef __cplusplus
}
#endif
#endif

