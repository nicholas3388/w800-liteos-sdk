#ifndef DOHOME_DP_TYPES_H
#define DOHOME_DP_TYPES_H


#ifdef __cplusplus
extern "C" {
#endif

#include <dohome_type.h>

//���ݵ�
typedef DOHOME_UINT8_T dh_obj_dp_tp_e;
#define DP_TP_VALUE 1
#define DP_TP_STR 2


typedef union {
    DOHOME_UINT32_T dp_value;             // valid when dp type is value
    DOHOME_UINT32_T dp_enum;              // valid when dp type is enum
    DOHOME_CHAR_T *dp_str;                 // valid when dp type is str
    DOHOME_INT8_T dp_bool;                 // valid when dp type is bool
    DOHOME_UINT32_T dp_bitmap;           // valid when dp type is bitmap
}dh_obj_dp_velue_u;

typedef struct {
    DOHOME_UINT8_T dpid;                // dp id
    dh_obj_dp_tp_e type;             // dp type
    dh_obj_dp_velue_u value;     // dp value
}dh_obj_dp_t;

typedef struct {
    DOHOME_UINT8_T dps_cnt;
    dh_obj_dp_t *dps_list;
}dh_obj_dps_t;



//------------------
// typedef DOHOME_INT8_T dh_cmd_tp_e;
// #define DT_CMD_DEVICE_INFO          0
// #define DT_CMD_SET_WIFI             1
// #define DT_CMD_SET_BLE              1
// #define DT_CMD_GET_DEVICE_STATUS    2
// #define DT_CMD_SET_DEVICE_STATUS    3
// #define DT_CMD_FACTORY_RESET        4
// #define DT_CMD_OTA                  5
// #define DT_CMD_HEARTBEAT_INFO       8
// #define DT_CMD_DEBUG                (9)
// #define DT_CMD_NULL                -1


#define DH_DP_NUM_ITEM_MAX_SIZE     4

#define DH_DP_STR_ITEM_MIN_SIZE     5
#define DH_DP_STR_ITEM_MAX_SIZE     520

#ifdef __cplusplus
}
#endif

#endif // DOHOME_TYPES_H#ifndef DOHOME_TYPES_H
