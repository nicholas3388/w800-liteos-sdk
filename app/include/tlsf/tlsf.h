#ifndef __DOHOME_TLSF_MEM_H__
#define __DOHOME_TLSF_MEM_H__

#include <dohome_type.h>
#ifdef __cplusplus
    extern "C" {
#endif
#ifdef DOHOME_TLSF_MEM_ISGLOBAL
#define DOHOME_TLSF_MEM_GLOBAL_FLAG extern
#else
#define DOHOME_TLSF_MEM_GLOBAL_FLAG 
#endif

DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_add_pool(void * tlsf, void * mem, DOHOME_SIZE_T bytes);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void tlsf_remove_pool(void * tlsf, void * mem);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_create(void * mem);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_create_with_pool(void * mem, DOHOME_SIZE_T bytes);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void tlsf_destroy(void * mem);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_get(void * mem);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_malloc(void * tlsf, DOHOME_SIZE_T size);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_memalign(void * tlsf, DOHOME_SIZE_T align, DOHOME_SIZE_T size);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void tlsf_free(void * tlsf, void * ptr);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void * tlsf_realloc(void * tlsf, void * ptr, DOHOME_SIZE_T size);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   void tlsf_info(void * tlsf, DOHOME_SIZE_T * mused, DOHOME_SIZE_T * mfree);
DOHOME_TLSF_MEM_GLOBAL_FLAG \
   DOHOME_SIZE_T dohome_lib_get_size_for_ptr(void * tlsf, void * ptr);
#ifdef __cplusplus
}
#endif

#endif // __DOHOME_TLSF_MEM_H__
