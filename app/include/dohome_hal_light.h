#ifndef DOHOME_HAL_LIGHT_H
#define DOHOME_HAL_LIGHT_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#ifdef __cplusplus
extern "C" {
#endif

#define DOHOME_LIGHT_NULL               (0)    
#define DOHOME_LIGHT_PWM_SELF           (0x01<<0)
#define DOHOME_LIGHT_IIC_SMx726         (0x01<<1)
#define DOHOME_LIGHT_IIC_SM2135         (0x01<<2)
#define DOHOME_LIGHT_IIC_BP1658CJ       (0x01<<3)
#define DOHOME_LIGHT_IIC_BP5758D        (0x01<<4)
#define DOHOME_LIGHT_MAX                (0x01<<5)
typedef DOHOME_UINT16_T dohome_light_type_t;


#define DOHOME_LIGHT_CHANNEL_1    0
#define DOHOME_LIGHT_CHANNEL_2    1
#define DOHOME_LIGHT_CHANNEL_3    2
#define DOHOME_LIGHT_CHANNEL_4    3
#define DOHOME_LIGHT_CHANNEL_5    4
typedef DOHOME_UINT8_T dohome_light_channel_t;


typedef struct {
    dohome_light_type_t type;
    dohome_light_channel_t channel;
    union {
        struct {
            DOHOME_UINT8_T io; 
            DOHOME_UINT8_T freq; 
        }pwm;
        struct {
            DOHOME_UINT8_T max_current; 
        }iic;
    }cfg;
}dohome_light_item_t;

typedef struct{
    struct {
        DOHOME_UINT8_T sda;
        DOHOME_UINT8_T scl;    
    }iic;
    struct {
        DOHOME_UINT16_T freq;     
    }pwm;
    DOHOME_UINT16_T max_duty;
}dohome_light_cfg_t;


dohome_op_ret dohome_hal_light_item_stop(dohome_light_item_t item);
dohome_op_ret dohome_hal_light_item_start(dohome_light_item_t item);

dohome_op_ret dohome_hal_light_item_set_duty(dohome_light_item_t item, DOHOME_UINT32_T duty);

dohome_op_ret dohome_hal_light_init(dohome_light_cfg_t led_cfg, dohome_light_item_t *item_list, DOHOME_UINT8_T item_num);

#ifdef __cplusplus
}
#endif
#endif
