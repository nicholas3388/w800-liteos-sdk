#ifndef __DOHOME_API_H__
#define __DOHOME_API_H__

#include "dohome_dp_type.h"
#include "dohome_type.h"
#include "dohome_error_code.h"

typedef enum {
    DOHOME_SYSTEM_EVENT = 0,
    DOHOME_WIFI_EVENT,
    DOHOME_BLE_EVENT,
    DOHOME_BIND_EVENT,
}dohome_event_type_t;


typedef enum{
    DOHOME_SYSTEM_EVENT_FACTORY_RESET = 0,
}dohome_event_system_t;

typedef enum{
    DOHOME_WIFI_EVENT_STA_CONNECT = 0,
    DOHOME_WIFI_EVENT_STA_CONNECT_FAIL,
    DOHOME_WIFI_EVENT_STA_GET_IP,
    DOHOME_WIFI_EVENT_STA_DISCONNECT,
    DOHOME_WIFI_EVENT_CFG_START,
    DOHOME_WIFI_EVENT_CFG_SUCCESS,
    DOHOME_WIFI_EVENT_CFG_FAIL,
    DOHOME_WIFI_EVENT_CFG_TIMEOUT,
}dohome_event_wifi_t;

typedef enum{
    DOHOME_BLE_EVENT_STA_CONNECT = 0,
    DOHOME_BLE_EVENT_STA_DISCONNECT,
    DOHOME_BLE_EVENT_CFG_START,
    DOHOME_BLE_EVENT_CFG_FAIL,
    DOHOME_BLE_EVENT_CFG_SUCCESS,
}dohome_event_ble_t;

typedef enum{
    DOHOME_BIND_EVENT_START,
    DOHOME_BIND_EVENT_HTTP_START_BIND,
	DOHOME_BIND_EVENT_HTTP_DNS_GET_IP,
	DOHOME_BIND_EVENT_HTTP_CONNECTED,
	DOHOME_BIND_EVENT_HTTP_REQ_ERROR,
	DOHOME_BIND_EVENT_HTTP_REQ_SUCCESS,
	DOHOME_BIND_EVENT_FAIL,
	DOHOME_BIND_EVENT_SUCCESS,
	DOHOME_BIND_EVENT_TIMEOUT,
}dohome_event_bind_t;

typedef void (*dohome_event_handler_t)(dohome_event_type_t event_type, DOHOME_UINT16_T event_id, void* event_data);

typedef dohome_op_ret (*dohome_cloud_set_dps_cb_t)(dh_obj_dps_t *dps, DOHOME_UINT8_T *cancel_resp);
typedef dohome_op_ret (*dohome_cloud_get_dps_cb_t)(DOHOME_CHAR_T all, dh_obj_dps_t *dps);

void* dohome_tlsfmem_malloc(DOHOME_SIZE_T bytes);
dohome_op_ret dohome_tlsfmem_free(void* p);

DOHOME_SIZE_T dohome_safe_strlen(DOHOME_CONST DOHOME_CHAR_T *str, DOHOME_SIZE_T max_len);
dohome_op_ret dohome_safe_strcpy(DOHOME_CHAR_T *dest, DOHOME_CONST DOHOME_CHAR_T *src, DOHOME_SIZE_T max_size);
DOHOME_UINT32_T dohome_random(DOHOME_UINT32_T min, DOHOME_UINT32_T max);

void dohome_wifi_close(void);
void dohome_wifi_start_connect(void);
void dohome_wifi_start_config(void);
void dohome_wifi_start_config_to_mode(DOHOME_UINT8_T mode);
void dohome_wifi_start_config_and_reboot(void);
DOHOME_UINT8_T dohome_wifi_has_config_mode(void);
void dohome_wifi_config_set_timeout(DOHOME_UINT16_T seconds);

DOHOME_UINT8_T dohome_wifi_has_config(void);
DOHOME_UINT8_T dohome_wifi_has_boot_start_config(void);
dohome_op_ret dohome_wifi_close_config_mode(void);

dohome_op_ret dohome_ble_init(void);
DOHOME_UINT8_T dohome_ble_has_config(void);
DOHOME_UINT8_T dohome_ble_has_boot_start_config(void);

void dohome_ble_adv_start_config(void);
void dohome_ble_adv_stop_config(void);

DOHOME_UINT8_T dohome_ble_has_config_mode(void);
dohome_op_ret dohome_ble_close_config_mode(void);

dohome_op_ret dohome_cloud_post_all_dps(void);
void dohome_cloud_reg_get_dps_cb(dohome_cloud_get_dps_cb_t get_dps_cb);
void dohome_cloud_reg_set_dps_cb(dohome_cloud_set_dps_cb_t set_dps_cb);
dohome_op_ret dohome_cloud_post_dps(DOHOME_UINT8_T *dpid_list, DOHOME_UINT8_T list_len);

dohome_op_ret dohome_event_reg_handler(dohome_event_type_t event_type, dohome_event_handler_t event_handler);

DOHOME_UINT32_T dohome_time_get_time(void);

dohome_op_ret dohome_kvs_del_env(const DOHOME_CHAR_T *key);
DOHOME_SIZE_T dohome_kvs_get_env_blob(DOHOME_CONST DOHOME_CHAR_T *key, void *value_buf, DOHOME_SIZE_T buf_len, DOHOME_SIZE_T *saved_value_len);
dohome_op_ret dohome_kvs_set_env_blob(DOHOME_CONST DOHOME_CHAR_T *key, DOHOME_CONST void *value_buf, DOHOME_SIZE_T buf_len);

dohome_op_ret dohome_lib_os_loop(void);
dohome_op_ret dohome_lib_timer_loop(void);
void dohome_lib_set_loop_period(DOHOME_UINT16_T period);

void dohome_lib_set_product_version(DOHOME_CHAR_T *sv, DOHOME_CHAR_T *hv);

dohome_op_ret dohome_lib_main(DOHOME_CHAR_T *pid);

void cozylife_sdk_info(void);

#endif