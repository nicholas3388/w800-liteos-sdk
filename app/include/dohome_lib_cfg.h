#ifndef __DOHOME_LIB_CFG_H__
#define __DOHOME_LIB_CFG_H__

#ifdef __cplusplus
extern "C" {
#endif

#define DOHOME_SDK_VERSION          "0.5.5"

#define DOHOME_HTTP_DOMAIN_NAME     "doiting.com"
#define DOHOME_HTTP_SECOND_LEVEL_DOMAIN_NAME     "api-us" 

#define DOHOME_HTTP_DEFAULT_IP     "47.252.10.9" 

#include <dohome_cfg.h> // there is include the cfg header file for platform

#ifndef DOHOME_CFG_USE_VERIFY
#define DOHOME_CFG_USE_VERIFY       0
#endif
/*****************************************************************************************
//          _                     _                        _     _                 
//         | |                   | |                      | |   (_)                
//    ___  | |__     ___    ___  | | __     ___    _ __   | |_   _    ___    _ __  
//   / __| | '_ \   / _ \  / __| | |/ /    / _ \  | '_ \  | __| | |  / _ \  | '_ \ 
//  | (__  | | | | |  __/ | (__  |   <    | (_) | | |_) | | |_  | | | (_) | | | | |
//   \___| |_| |_|  \___|  \___| |_|\_\    \___/  | .__/   \__| |_|  \___/  |_| |_|
//                                                | |                              
//                                                |_|                              
*/
#if ((!defined(DOHOME_PLATFORM_TYPE)) || DOHOME_PLATFORM_TYPE == 0)
    #error "DOHOME_PLATFORM_TYPE not set"
#endif
#if ((!defined(DOHOME_CHIP_NAME)) || DOHOME_CHIP_NAME == 0)
    #error "DOHOME_CHIP_NAME not set"
#endif
#if ((!defined(DOHOME_USE_PROTOCL_VERSION)))
    #error "DOHOME_USE_PROTOCL_VERSION not set"
#endif
#ifndef DOHOME_LIB_ASSERT
    #error "DOHOME_LIB_ASSERT(x) not set"
#endif
/*****************************************************************************************
//-----------------------------------------------------------------------------------------
//   _                                     _     _                 
//  | |                                   | |   (_)                
//  | |   ___     __ _      ___    _ __   | |_   _    ___    _ __  
//  | |  / _ \   / _` |    / _ \  | '_ \  | __| | |  / _ \  | '_ \ 
//  | | | (_) | | (_| |   | (_) | | |_) | | |_  | | | (_) | | | | |
//  |_|  \___/   \__, |    \___/  | .__/   \__| |_|  \___/  |_| |_|
//                __/ |           | |                              
//               |___/            |_|                              
*/
#ifndef DOHOME_LOG_LVL_ALL
#define DOHOME_LOG_LVL_ALL            DOHOME_LOG_LVL_DEBUG
#endif
#ifndef DOHOME_LOG_COLOR_ENABLE
#define DOHOME_LOG_COLOR_ENABLE       0
#endif

#ifndef DOHOME_LOG_UDP_ENABLE
#define DOHOME_LOG_UDP_ENABLE         1
#endif

#ifndef DOHOME_LOG_UDP_PORT
#define DOHOME_LOG_UDP_PORT               7789
#endif

#ifndef DOHOME_LOG_UDP_IP
#define DOHOME_LOG_UDP_IP                 "192.168.2.2"
#endif
/*-----------------------------------------------------------------------------------------
//           _           _      __                                                _     _                 
//          | |         | |    / _|                                              | |   (_)                
//   _ __   | |   __ _  | |_  | |_    ___    _ __   _ __ ___       ___    _ __   | |_   _    ___    _ __  
//  | '_ \  | |  / _` | | __| |  _|  / _ \  | '__| | '_ ` _ \     / _ \  | '_ \  | __| | |  / _ \  | '_ \ 
//  | |_) | | | | (_| | | |_  | |   | (_) | | |    | | | | | |   | (_) | | |_) | | |_  | | | (_) | | | | |
//  | .__/  |_|  \__,_|  \__| |_|    \___/  |_|    |_| |_| |_|    \___/  | .__/   \__| |_|  \___/  |_| |_|
//  | |                                                                  | |                              
//  |_|                                                                  |_|          
*/
#ifndef DT_DELIVERY_AREA
#define DT_DELIVERY_AREA            "00"
#endif

#ifndef DT_NAME_PREFIX
#define DT_NAME_PREFIX              "CozyLife_"
#endif

#if (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI)
#define DT_DEVICE_TYPE              "00"
#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
#define DT_DEVICE_TYPE              "01"
#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE)
#define DT_DEVICE_TYPE              "02"
#else
#error "DOHOME_PLATFORM_TYPE not set"
#endif
/*-----------------------------------------------------------------------------------------
//              _    __   _                     _     _                 
//             (_)  / _| (_)                   | |   (_)                
//  __      __  _  | |_   _      ___    _ __   | |_   _    ___    _ __  
//  \ \ /\ / / | | |  _| | |    / _ \  | '_ \  | __| | |  / _ \  | '_ \ 
//   \ V  V /  | | | |   | |   | (_) | | |_) | | |_  | | | (_) | | | | |
//    \_/\_/   |_| |_|   |_|    \___/  | .__/   \__| |_|  \___/  |_| |_|
//                                     | |                              
//                                     |_|                              
*/
#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))

    #ifndef DOHOME_CFG_NETWORk_USE_TASK
    #define DOHOME_CFG_NETWORk_USE_TASK                 0
    #endif

    #ifndef DOHOME_CFG_WIFI_AP_SERVER_IP
    #define DOHOME_CFG_WIFI_AP_SERVER_IP                "192.168.4.1"
    #endif

    #ifndef DOHOME_CFG_WIFI_CONN_LOOP_MS
    #define DOHOME_CFG_WIFI_CONN_LOOP_MS                20
    #endif

    #ifndef DOHOME_CFG_WIFI_CFG_AND_BIND_LOOP_MS
    #define DOHOME_CFG_WIFI_CFG_AND_BIND_LOOP_MS        200
    #endif

    #ifndef DOHOME_CFG_WIFI_SHORT_RECONN_TICK
    #define DOHOME_CFG_WIFI_SHORT_RECONN_TICK           100
    #endif

    #ifndef DOHOME_CFG_WIFI_LONG_RECONN_TICK
    #define DOHOME_CFG_WIFI_LONG_RECONN_TICK            (20*DOHOME_CFG_WIFI_SHORT_RECONN_TICK)
    #endif

    #ifndef DOHOME_CFG_WIFI_RETRY_INTERL_TICK
    #define DOHOME_CFG_WIFI_RETRY_INTERL_TICK           1000
    #endif

    #ifndef DOHOME_CFG_WIFI_CONN_RANDOM_MAXTICK
    #define DOHOME_CFG_WIFI_CONN_RANDOM_MAXTICK         20
    #endif

    #ifndef DOHOME_CFG_WIFI_LOOP_RECONNECT
    #define DOHOME_CFG_WIFI_LOOP_RECONNECT              0
    #endif

    #ifndef DOHOME_CFG_WIFI_CONN_FAIL_RECONNECT
    #define DOHOME_CFG_WIFI_CONN_FAIL_RECONNECT         1
    #endif

    #ifndef DOHOME_CFG_WIFI_CFG_TIMEOUT_TICK
    #define DOHOME_CFG_WIFI_CFG_TIMEOUT_TICK            300*3
    #endif

    #ifndef DOHOME_CFG_WIFI_CFG_EXTENSION_TICK
    #define DOHOME_CFG_WIFI_CFG_EXTENSION_TICK          300
    #endif

    #ifndef DOHOME_CFG_OTA_LOOP_MS
    #define DOHOME_CFG_OTA_LOOP_MS                      100
    #endif

    #ifndef DOHOME_CFG_HTTP_OTA_RETRY_MAXCNT
    #define DOHOME_CFG_HTTP_OTA_RETRY_MAXCNT            20
    #endif

    #ifndef DOHOME_CFG_HTTP_OTA_RECV_TIMEOUT_TICK
    #define DOHOME_CFG_HTTP_OTA_RECV_TIMEOUT_TICK       100
    #endif

    #ifndef DOHOME_CFG_HTTP_OTA_RETRY_CNT
    #define DOHOME_CFG_HTTP_OTA_RETRY_CNT               3
    #endif

    #ifndef DOHOME_CFG_TCP_CLI_LOOP_MS
    #define DOHOME_CFG_TCP_CLI_LOOP_MS                  1000*2
    #endif

    #ifndef DOHOME_CFG_TCP_CLI_MAX_RECONN_CNT
    #define DOHOME_CFG_TCP_CLI_MAX_RECONN_CNT           10
    #endif

    #ifndef DOHOME_CFG_UBINDING_TIMEOUT_TICK
    #define DOHOME_CFG_UBINDING_TIMEOUT_TICK            25
    #endif
    
    #ifndef DHOME_CFG_TCP_SRV_PORT
    #define DHOME_CFG_TCP_SRV_PORT                      5555
    #endif

    #ifndef DHOME_CFG_UDP_SRV_PORT
    #define DHOME_CFG_UDP_SRV_PORT                      6095
    #endif

    #ifndef DOHOME_CFG_TCP_SERVER_ONE_SIZE
    #define DOHOME_CFG_TCP_SERVER_ONE_SIZE              500
    #endif

    #ifndef DOHOME_CFG_TCP_SERVER_INIT_RETRY_MAX_CNT
    #define DOHOME_CFG_TCP_SERVER_INIT_RETRY_MAX_CNT    10
    #endif
#endif
/*-----------------------------------------------------------------------------------------
//                                                          _                     _     _                 
//                                                         | |                   | |   (_)                
//   _ __ ___     ___   _ __ ___    _ __     ___     ___   | |     ___    _ __   | |_   _    ___    _ __  
//  | '_ ` _ \   / _ \ | '_ ` _ \  | '_ \   / _ \   / _ \  | |    / _ \  | '_ \  | __| | |  / _ \  | '_ \ 
//  | | | | | | |  __/ | | | | | | | |_) | | (_) | | (_) | | |   | (_) | | |_) | | |_  | | | (_) | | | | |
//  |_| |_| |_|  \___| |_| |_| |_| | .__/   \___/   \___/  |_|    \___/  | .__/   \__| |_|  \___/  |_| |_|
//                                 | |                                   | |                              
//                                 |_|                                   |_|                              
*/
#ifndef DOHOME_LIB_MEM_POOL_NAME
#error "DOHOME_LIB_MEM_POOL_NAME not set"
#endif

#ifndef DOHOME_MEM_POOL_SIZE
#define DOHOME_MEM_POOL_SIZE          (20*1024)
#endif

#ifndef dohome_mem_init
#define dohome_mem_init dohome_tlsfmem_init
#endif
// dohome_tlsfmem_add_pool only tlsf used
#ifndef dohome_mem_add_pool
#define dohome_mem_add_pool dohome_tlsfmem_add_pool
#endif

#ifndef dohome_mem_remove_pool
#define dohome_mem_remove_pool dohome_tlsfmem_remove_pool
#endif

#ifndef dohome_mem_get_free_size
#define dohome_mem_get_free_size dohome_tlsfmem_get_free_size
#endif

#ifndef dohome_mem_get_used_size
#define dohome_mem_get_used_size dohome_tlsfmem_get_used_size
#endif
#ifndef dohome_mem_malloc
#define dohome_mem_malloc  dohome_tlsfmem_malloc
// #define dohome_mem_malloc(size)  dohome_tlsfmem_malloc_debug(size, 0, __FUNCTION__, __LINE__)
#endif
#ifndef dohome_mem_realloc
#define dohome_mem_realloc dohome_tlsfmem_realloc
#endif
#ifndef dohome_mem_calloc
#define dohome_mem_calloc dohome_tlsfmem_calloc
#endif
#ifndef dohome_mem_free
#define dohome_mem_free dohome_tlsfmem_free
// #define dohome_mem_free(ptr)  dohome_tlsfmem_free_debug(ptr, __FUNCTION__, __LINE__)
#endif
#ifndef dohome_mem_deinit
#define dohome_mem_deinit dohome_tlsfmem_deinit
#endif
//-----------------------------------------------------------------------------------------

#ifndef dohome_kvs_devid_key
#define dohome_kvs_devid_key            "dh_id"
#endif
#ifndef dohome_kvs_devkey_key
#define dohome_kvs_devkey_key           "dh_key"
#endif
#ifndef dohome_kvs_tcp_key
#define dohome_kvs_tcp_key              "dh_tcp"
#endif
#ifndef dohome_kvs_admin_key
#define dohome_kvs_admin_key            "dh_admin"
#endif
#ifndef dohome_kvs_productid_key
#define dohome_kvs_productid_key        "dh_pid"
#endif
#ifndef dohome_kvs_wifiinfo_key
#define dohome_kvs_wifiinfo_key         "dh_wfinfo"
#endif
#ifndef dohome_kvs_bootcfg_key
#define dohome_kvs_bootcfg_key          "dh_boot_cfg"
#endif
#ifndef dohome_kvs_udp_log_key
#define dohome_kvs_udp_log_key          "dh_udp_log"
#endif
#ifndef dohome_kvs_second_domain_key
#define dohome_kvs_second_domain_key    "dh_second_dm"
#endif

#if (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
#ifndef dohome_kvs_devbind_key
#define dohome_kvs_devbind_key         "dh_bind"
#endif
#endif

#ifndef dohome_json_memfree
#define dohome_json_memfree    dohome_mem_free
#endif

#ifdef __cplusplus
}
#endif
#endif // #ifndef __DOHOME_PUBLIC_CFG_H__