#ifndef DOHOME_HAL_BT_H
#define DOHOME_HAL_BT_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DEVICE_NAME_LEN         16
#define DEVICE_MAC_LEN          6
#define MIN(x,y) (((x)<(y))?(x):(y))
typedef enum {
    DOHOME_BT_EVENT_BLEENABLED,   /* 蓝牙初始化完成 */
    DOHOME_BT_EVENT_DISCONNECTED,   /* APP断开连接 */
    DOHOME_BT_EVENT_CONNECTED,      /* APP连接上设备 */
    DOHOME_BT_EVENT_RX_DATA,        /* 接收到APP数据*/
    DOHOME_BT_EVENT_ADV_READY,      /* start adv. */
}dohome_bt_cb_event_t;

typedef struct {
    DOHOME_UINT_T len;
    DOHOME_UCHAR_T *data;
} dohome_ble_data_buf_t;

typedef void (*DOHOME_BT_MSG_CB)(DOHOME_UINT16_T id, dohome_bt_cb_event_t event, dohome_ble_data_buf_t *buf);

typedef struct {
    char name[DEVICE_NAME_LEN];
    DOHOME_UCHAR_T link_num;
    DOHOME_BT_MSG_CB cb;
    dohome_ble_data_buf_t adv;
    dohome_ble_data_buf_t scan_rsp;
}dohome_bt_param_t;

/* dohome sdk definition of MAC info */
typedef struct
{
    DOHOME_UINT8_T mac[DEVICE_MAC_LEN]; /* mac address */
}ble_mac_t;

typedef enum {
    DOHOME_BT_SCAN_BY_NAME = 0x01,
    DOHOME_BT_SCAN_BY_MAC = 0x02,
}dohome_bt_scan_type_t;

typedef struct {
    dohome_bt_scan_type_t scan_type;
    char name[DEVICE_NAME_LEN];
    char bd_addr[6];
    signed char rssi;
    DOHOME_UCHAR_T channel;
    DOHOME_UCHAR_T timeout_s; /* second. */
}dohome_bt_scan_info_t;

typedef struct {
    DOHOME_CHAR_T mac[DEVICE_MAC_LEN];
    DOHOME_INT8_T rssi;
    DOHOME_UINT8_T *data;
    DOHOME_UINT8_T *name;
    DOHOME_UINT8_T name_len;
    DOHOME_UINT8_T len;
}dohome_bt_scan_odm_data_t;

typedef void (*dohome_get_mac_cb_t)(char *mac, uint8_t len);
typedef void (*dohome_bt_scan_odm_data_cb_t)(dohome_bt_scan_odm_data_t *odm_data);


/**
 * @brief 用于初始化蓝牙
 *
 * @param[in]       para     初始化参数
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_port_init(dohome_bt_param_t *para);

/**
 * @brief 用于释放化蓝牙资源
 *
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_port_deinit(void);

/**
 * @brief 用于断开蓝牙连接
 *
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_gap_disconnect(uint16_t conn);

/**
 * @brief 用于发送蓝牙数据
 *
 * @param[in]       data     发送数据
  * @param[in]      len      数据长度
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_send(uint16_t conn,const DOHOME_UCHAR_T *data, const DOHOME_UCHAR_T len);

/**
 * @brief 用于重置蓝牙广播内容
 *
 * @param[in]       adv
  * @param[in]      scan_resp
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_reset_adv(const dohome_ble_data_buf_t *adv, const dohome_ble_data_buf_t *scan_resp);

/**
 * @brief 用于启动蓝牙广播
 *
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_start_adv(void);

/**
 * @brief 用于停止蓝牙广播
 *
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_stop_adv(void);

/**
 * @brief 用于获取蓝牙信号强度
 *
 * @param[out]       rssi
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_get_rssi(signed char *rssi);

/**
 * @brief 用于扫描蓝牙信标
 *
 * @param[out]       rssi
 * @return int 0=成功，非0=失败
 */
int dohome_hal_bt_assign_scan(dohome_bt_scan_info_t *info);

void dohome_hal_set_mac_cb_func(dohome_get_mac_cb_t f);

dohome_op_ret dohome_hal_ble_get_mac(ble_mac_t *ble_mac);

void dohome_hal_bt_scan_enable(void);
void dohome_hal_bt_scan_disable(void);

int dohome_hal_bt_odm_scan(dohome_bt_scan_odm_data_cb_t scan_cb);

int dohome_hal_bt_scan(dohome_bt_scan_odm_data_cb_t scan_cb);
int dohome_hal_bt_scan_dohome_otp(dohome_bt_scan_odm_data_cb_t scan_cb);
int dohome_hal_bt_scan_cozylife_otp(dohome_bt_scan_odm_data_cb_t scan_cb);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif


