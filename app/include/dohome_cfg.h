#ifndef DOHOME_CFG_H
#define DOHOME_CFG_H

#ifdef __cplusplus
extern "C" {
#endif
#include "dohome_cfg_define.h"
#include <stdlib.h>

#define DOHOME_LOG_COLOR_ENABLE       1
#define DOHOME_LOG_UDP_ENABLE         1
#define DOHOME_LOG_LVL_ALL            DOHOME_LOG_LVL_DEBUG //DOHOME_LOG_LVL_LESS

#define DOHOME_USE_PROTOCL_VERSION    0
#define DOHOME_CHIP_NAME              DOHOME_CHIP_WM800
#define DOHOME_PLATFORM_TYPE          DOHOME_PLATFORM_WIFIBLE
#define DOHOME_LIB_MEM_POOL_NAME      DOHOME_LIB_MEM_NOT_USE

#undef TLSF_64BIT
#define DOHOME_MEM_POOL_SIZE          (10*1024)

#define DOHOME_CFG_NETWORK_USE_TASK   1

#define dohome_mem_init 0
// dohome_tlsfmem_add_pool only tlsf used
#define dohome_mem_add_pool 0
#define dohome_mem_remove_pool 0
#define dohome_mem_get_free_size 0
#define dohome_mem_get_used_size 0
//extern void *pvPortMalloc( size_t xWantedSize );
extern void *mem_alloc_debug(unsigned int size);
#define dohome_mem_malloc mem_alloc_debug //pvPortMalloc
//extern void vPortFree( void *pv );
extern void mem_free_debug(void *p);
#define dohome_mem_free mem_free_debug //vPortFree
#define dohome_mem_deinit 0

#define dohome_json_memfree    dohome_mem_free


// #define DOHOME_CFG_WIFI_LOOP_RECONNECT              1
// #define DOHOME_CFG_WIFI_CONN_FAIL_RECONNECT         0

#define DOHOME_LIB_ASSERT(x)     do{if(!(x)){DOHOME_LOG_RAW("ASSERT %s:%s(%d) %s",__FILE__,__FUNCTION__,__LINE__,#x);while(1);}}while(0)


//调试
#define SHELL_ENABLE                    0
#define SHELL_END_LINE_ENABLE           1

#if (SHELL_ENABLE == 1)
#undef DOHOME_LOG_COLOR_ENABLE
#define DOHOME_LOG_COLOR_ENABLE         1
#endif

#ifdef __cplusplus
}
#endif
#endif

