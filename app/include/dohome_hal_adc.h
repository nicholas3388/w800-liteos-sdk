#ifndef DOHOME_HAL_BT_H
#define DOHOME_HAL_BT_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


DOHOME_UINT16_T dohome_hal_get_adc_value(DOHOME_UINT8_T gpio);
dohome_op_ret dohome_hal_set_adc(DOHOME_UINT8_T gpio, DOHOME_UINT16_T freq);





#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif


