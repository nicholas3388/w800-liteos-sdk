#ifndef __DOHOME_PUBLIC_DEFINE_H__
#define __DOHOME_PUBLIC_DEFINE_H__


#define DOHOME_CHIP_WM800             1
#define DOHOME_CHIP_WM600             2
#define DOHOME_CHIP_BL602             3
#define DOHOME_CHIP_BK3432            4
#define DOHOME_CHIP_AC6369            5
#define DOHOME_CHIP_ESP32             6
#define DOHOME_CHIP_AC6969            7

#define DOHOME_PLATFORM_WIFI          1
#define DOHOME_PLATFORM_BLE           2
#define DOHOME_PLATFORM_WIFIBLE       3

#define DOHOME_LOG_LVL_ASSERT      0       // 严重错误，系统直接停止运行
#define DOHOME_LOG_LVL_LESS        1       // 最少打印
#define DOHOME_LOG_LVL_ERROR       3       // 错误信息，程序正常运行不应发生的信息
#define DOHOME_LOG_LVL_WARN        4       // 需要注意的信息
#define DOHOME_LOG_LVL_INFO        6       // 程序运行调试信息
#define DOHOME_LOG_LVL_DEBUG       7       // 开发调试信息

#define DOHOME_LIB_MEM_POOL_TLSF   1
#define DOHOME_LIB_MEM_POOL_XMEM   2      // 一个轻量级的内存管理组件，支持内存动态分配、回收与越界检测，只需简单的配置就能轻松导入工程，使用及其方便，可以有效地提高研发测试效率。同时，xmem支持多种个性化操作，可以更高效更便捷的管理内存。
#define DOHOME_LIB_MEM_NOT_USE       3

#endif