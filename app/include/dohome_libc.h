#ifndef _DOHOME_LIBC_H_
#define _DOHOME_LIBC_H_

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>

#include <dohome_type.h>
#include <dohome_error_code.h>

DOHOME_INT_T dohome_printf(DOHOME_CONST DOHOME_CHAR_T *fmt, ...);
DOHOME_INT_T dohome_vsnprintf(DOHOME_CHAR_T *buffer, size_t n, DOHOME_CONST DOHOME_CHAR_T *format, va_list ap);
#endif // _DOHOME_LIBC_H_