#ifndef _DOIT_HAL_MUSIC_H_
#define _DOIT_HAL_MUSIC_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

typedef struct {
    DOHOME_INT_T amplitude;
}dohome_music_point_t;

typedef enum{
    DOHOME_MUSIC_STATE_OK       = 0,    
    DOHOME_MUSIC_STATE_BUSY     = 1,   
}dohome_music_state_t;

dohome_op_ret dohome_hal_musicdym_set_adc(DOHOME_UINT16_T gpio, DOHOME_UINT16_T freq);
dohome_op_ret dohome_hal_musicdym_init(DOHOME_UINT16_T point_num);
dohome_music_state_t dohome_hal_musicdym_read_all(dohome_music_point_t *p);

void dohome_hal_musicdym_reset(void);

void dohome_hal_musicdym_start(void);
void dohome_hal_musicdym_stop(void);

#endif
