#ifndef _DOHOME_HAL_SYSTEM_H_
#define _DOHOME_HAL_SYSTEM_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

typedef enum{
    DOHOME_RST_POWER_OFF = 0,            //电源重启 
    DOHOME_RST_HARDWARE,                 //硬件复位 
    DOHOME_RST_SOFTWARE,                 //软件复位 
    DOHOME_RST_HARDWARE_WATCHDOG,        //硬件看门狗复位 
    DOHOME_RST_SOFTWARE_WATCHDOG,        //软件看门狗重启 
    DOHOME_RST_FATAL_EXCEPTION,          //异代码常 
    DOHOME_RST_DEEPSLEEP,                //深度睡眠 
    DOHOME_RST_OTHER,                    //其它原因 
    DOHOME_RST_UNSUPPORT,                //不支持 
}dohome_rst_reason_t;

DOHOME_UINT16_T dohome_hal_get_rand(DOHOME_UINT16_T range);
void dohome_hal_ticktack_cb(DOHOME_UINT16_T ticktack);
dohome_op_ret dohome_hal_sys_reset(void);
dohome_rst_reason_t dohome_hal_sys_get_rst_reason(void);
void dohome_xdelay_ms(DOHOME_UINT16_T ms);
dohome_op_ret dohome_hal_sys_init(void);
#endif