#ifndef DOHOME_HAL_PWR_MEASURE_H_
#define DOHOME_HAL_PWR_MEASURE_H_

#include "dohome_type.h"
#include "dohome_error_code.h"

typedef enum {
    DOHOME_PWR_MEASURE_TYPE_HLW8012 = 0,              
    DOHOME_PWR_MEASURE_TYPE_BL0937,              
    DOHOME_PWR_MEASURE_TYPE_MAX,        
}dohome_pwr_measure_type_t;

typedef enum {
    DOHOME_PWR_MEASURE_FAULT_SET_HIGH_CURRENT = 0,              
    DOHOME_PWR_MEASURE_FAULT_CLS_HIGH_CURRENT,              
    DOHOME_PWR_MEASURE_FAULT_SET_HIGH_VOLTAGE,              
    DOHOME_PWR_MEASURE_FAULT_CLS_HIGH_VOLTAGE,              
    DOHOME_PWR_MEASURE_FAULT_SET_HIGH_POWER,  
    DOHOME_PWR_MEASURE_FAULT_CLS_HIGH_POWER,            
    DOHOME_PWR_MEASURE_FAULT_SET_LOW_CURRENT,              
    DOHOME_PWR_MEASURE_FAULT_CLS_LOW_CURRENT,              
    DOHOME_PWR_MEASURE_FAULT_SET_LOW_VOLTAGE,              
    DOHOME_PWR_MEASURE_FAULT_CLS_LOW_VOLTAGE,              
    DOHOME_PWR_MEASURE_FAULT_SET_LOW_POWER,  
    DOHOME_PWR_MEASURE_FAULT_CLS_LOW_POWER,     
    DOHOME_PWR_MEASURE_FAULT_MAX,        
}dohome_pwr_measure_fault_t;

typedef struct {
    DOHOME_UINT8_T high_voltage_en;
    DOHOME_UINT16_T high_voltage_value;
    DOHOME_UINT8_T high_current_en;
    DOHOME_UINT16_T high_current_value;
    DOHOME_UINT8_T high_power_en;
    DOHOME_UINT16_T high_power_value;
    DOHOME_UINT8_T low_voltage_en;
    DOHOME_UINT16_T low_voltage_value;
    DOHOME_UINT8_T low_current_en;
    DOHOME_UINT16_T low_current_value;
    DOHOME_UINT8_T low_power_en;
    DOHOME_UINT16_T low_power_value;
}dohome_pwr_measure_fault_cfg_t;

typedef struct{
    dohome_pwr_measure_type_t type;
    union {
        struct {
            dohome_gpio_t fc_gpio;
            dohome_gpio_t fc1_gpio;    
            dohome_gpio_t sel_gpio;    
        }hlw8012;
        struct {
            dohome_gpio_t fc_gpio;
            dohome_gpio_t fc1_gpio;    
            dohome_gpio_t sel_gpio;     
        }bl0937;
    }ic; 
    dohome_pwr_measure_fault_cfg_t fault_cfg;
}dohome_pwr_measure_cfg_t;

typedef void (* dohome_pwr_measure_fault_cb_t)(dohome_pwr_measure_fault_t fault);

dohome_op_ret dohome_hal_pwr_measure_init(dohome_pwr_measure_cfg_t cfg, dohome_pwr_measure_fault_cb_t fault_cb);

dohome_op_ret dohome_hal_pwr_measure_set_default_voltage_cal(DOHOME_FLOAT_T voltage);
dohome_op_ret dohome_hal_pwr_measure_set_default_current_cal(DOHOME_FLOAT_T current);
dohome_op_ret dohome_hal_pwr_measure_set_default_power_cal(DOHOME_FLOAT_T power);

void dohome_hal_pwr_measure_correct_init(void);
void dohome_hal_pwr_measure_correct_for_expected(DOHOME_UINT16_T voltage, DOHOME_UINT16_T current, DOHOME_UINT16_T power);

dohome_op_ret dohome_hal_pwr_measure_set_fault_cfg(dohome_pwr_measure_fault_cfg_t fault_cfg);
dohome_op_ret dohome_hal_pwr_measure_get_fault_cfg(dohome_pwr_measure_fault_cfg_t *fault_cfg);

DOHOME_UINT16_T dohome_hal_pwr_measure_get_voltage_V(void);         //V
DOHOME_UINT16_T dohome_hal_pwr_measure_get_current_mA(void);        //mA
DOHOME_UINT16_T dohome_hal_pwr_measure_get_active_power_W(void);    //W
DOHOME_UINT16_T dohome_hal_pwr_measure_get_power_energy_Wh(void);   //KWh

DOHOME_UINT32_T dohome_hal_pwr_measure_get_power_energy_Ws(void);
void dohome_hal_pwr_measure_set_power_energy_Ws(DOHOME_UINT32_T Ws);

void dohome_hal_pwr_measure_reset_power_energy(void);

DOHOME_UINT16_T dohome_hal_pwr_measure_get_apparent_power_VA(void);
DOHOME_INT16_T dohome_hal_pwr_measure_get_get_power_factor(void);

#endif