#ifndef DOHOME_HAL_GPIO_H
#define DOHOME_HAL_GPIO_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#ifdef __cplusplus
extern "C" {
#endif



// #define dohome_gpio_t       DOHOME_UINT16_T

typedef enum {
	DOHOME_GPIO_OUTPUT,
    DOHOME_GPIO_INPUT
}dohome_gpio_inout_t;

typedef enum {
	DOHOME_GPIO_FLOATING,		/**< floating status */
    DOHOME_GPIO_PULLHIGH,       /**< pull low */
    DOHOME_GPIO_PULLLOW            /**< pull high */
}dohome_gpio_pull_t;


DOHOME_UINT8_T dohome_hal_gpio_read(dohome_gpio_t gpio);

void dohome_hal_gpio_write(dohome_gpio_t gpio, DOHOME_UINT8_T level);

void dohome_hal_gpio_init(dohome_gpio_t gpio, dohome_gpio_inout_t inout, dohome_gpio_pull_t pull);


#ifdef __cplusplus
}
#endif
#endif
