#ifndef DOHOME_TIMER_H
#define DOHOME_TIMER_H

#ifdef __cplusplus
extern "C" {
#endif
#include <dohome_error_code.h>
#ifdef DOHOME_HALTIMER_ISGLOBAL
#define DOHOME_HALTIMER_GLOBAL_FLAG extern
#else
#define DOHOME_HALTIMER_GLOBAL_FLAG
#endif

typedef void (* dohome_hal_timer_cbfunc_t)(void *arg);
DOHOME_HALTIMER_GLOBAL_FLAG \
    dohome_op_ret dohome_hal_timer_init(DOHOME_UINT8_T *timer_id, DOHOME_UINT32_T ms, DOHOME_UINT32_T repeat, dohome_hal_timer_cbfunc_t cbfunc);
DOHOME_HALTIMER_GLOBAL_FLAG \
    dohome_op_ret dohome_hal_timer_us_init(DOHOME_UINT8_T *timer_id, DOHOME_UINT32_T us, DOHOME_UINT32_T repeat, dohome_hal_timer_cbfunc_t cbfunc);
DOHOME_HALTIMER_GLOBAL_FLAG \
    dohome_op_ret dohome_hal_timer_start(DOHOME_UINT8_T timer_id);
DOHOME_HALTIMER_GLOBAL_FLAG \
    dohome_op_ret dohome_hal_timer_stop(DOHOME_UINT8_T timer_id);
#ifdef __cplusplus
}
#endif
#endif

