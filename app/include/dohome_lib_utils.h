#ifndef _DOHOME_LIB_UTILS_H_
#define _DOHOME_LIB_UTILS_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

DOHOME_SIZE_T dohome_safe_strlen(DOHOME_CONST DOHOME_CHAR_T *str, DOHOME_SIZE_T max_len);
#endif /* _DOHOME_LIB_UTILS_H_ */