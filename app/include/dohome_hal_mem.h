#ifndef DOHOME_MEM_API_H
#define DOHOME_MEM_API_H
#include <dohome_type.h>
#include <dohome_error_code.h>
#ifdef __cplusplus
extern "C" {
#endif
#ifdef DOHOME_MEM_API_ISGLOBAL
#define DOHOME_MEM_API_GLOBAL_FLAG extern
#else
#define DOHOME_MEM_API_GLOBAL_FLAG
#endif

DOHOME_MEM_API_GLOBAL_FLAG \
    void* dohome_hal_mem_alloc(DOHOME_UINT32_T bytes);
DOHOME_MEM_API_GLOBAL_FLAG \
    dohome_op_ret dohome_hal_mem_free(void* p);

#ifdef __cplusplus
}
#endif
#endif

