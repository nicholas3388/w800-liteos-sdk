#ifndef _DOHOME_HAL_WIFI_H_
#define _DOHOME_HAL_WIFI_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

#define SSID_MAX_LEN			33
#define PASSWD_MAX_LEN			65
//the MAC addr len
#define DT_MAC_ADDR_LEN 6

#define SCAN_MAX_AP 32

/* dohome sdk definition of IP info */
typedef struct
{
    DOHOME_UINT8_T ip[16];    /* ip addr:  xxx.xxx.xxx.xxx  */
    DOHOME_UINT8_T mask[16];  /* net mask: xxx.xxx.xxx.xxx  */
    DOHOME_UINT8_T gw[16];    /* gateway:  xxx.xxx.xxx.xxx  */
}dohome_net_ip_t;

/* dohome sdk definition of MAC info */
typedef struct
{
    DOHOME_UINT8_T mac[DT_MAC_ADDR_LEN]; /* mac address */
}dohome_net_mac_t;

typedef enum
{
    DOHOME_WIFI_IF_STATION = 0,     ///< station type
    DOHOME_WIFI_IF_SOFTAP,              ///< ap type
}dohome_wifi_if_t;

typedef enum
{
    DOHOME_WIFI_MODE_LOWPOWER = 0,   ///< wifi work in lowpower mode
    DOHOME_WIFI_MODE_SNIFFER,        ///< wifi work in sniffer mode
    DOHOME_WIFI_MODE_STATION,        ///< wifi work in station mode
    DOHOME_WIFI_MODE_SOFTAP,         ///< wifi work in ap mode
    DOHOME_WIFI_MODE_STATIONAP,      ///< wifi work in station+ap mode
}dohome_wifi_mode_t;

typedef enum
{
    DOHOME_WIFI_AUTH_OPEN = 0,      ///< open
    DOHOME_WIFI_AUTH_WEP,           ///< WEP
    DOHOME_WIFI_AUTH_WPA_PSK,       ///< WPA—PSK
    DOHOME_WIFI_AUTH_WPA2_PSK,      ///< WPA2—PSK
    DOHOME_WIFI_AUTH_WPA_WPA2_PSK,  ///< WPA/WPA2
}wifi_auth_mode_e;

typedef struct {
    DOHOME_UINT8_T ssid[SSID_MAX_LEN+1];       ///< ssid
    DOHOME_UINT8_T s_len;                       ///< len of ssid
    DOHOME_UINT8_T passwd[PASSWD_MAX_LEN+1];   ///< passwd
    DOHOME_UINT8_T p_len;                       ///< len of passwd
    DOHOME_UINT8_T chan;                        ///< channel. default:6
    wifi_auth_mode_e md;                      ///< encryption type
    DOHOME_UINT8_T ssid_hidden;                 ///< ssid hidden  default:0
    DOHOME_UINT8_T max_conn;                    ///< max sta connect nums default:3
    DOHOME_UINT16_T ms_interval;                ///< broadcast interval default:100
    dohome_net_ip_t net_ip;
}dohome_wifi_cfg_if_t;

typedef enum{
	DT_WIFI_EVENT_CONNECT = 0,
	DT_WIFI_EVENT_CONNECT_FAIL,
	DT_WIFI_EVENT_GET_IP,
	DT_WIFI_EVENT_DISCONNECT,
	DT_WIFI_EVENT_CLIENT_ONLINE
}dohome_wifi_hal_event_t;

typedef enum{
    DT_WIFI_ERR_CONNECTION_FAIL = 100,
	DT_WIFI_ERR_AP_NOT_FOUND,
	DT_WIFI_ERR_AUTH_FAIL,
	DT_WIFI_ERR_HANDSHAKE_FAIL,
}dohome_wifi_hal_err_t;

typedef struct
{
    DOHOME_CHAR_T ssid[SSID_MAX_LEN+1];   // AP ssid array
    DOHOME_UINT8_T ssid_len;  
    DOHOME_UINT8_T bssid[6];                // AP bssid
    DOHOME_UINT8_T channel;                 // AP channel
    DOHOME_INT16_T rssi;                    // AP rssi
}dohome_ap_info_t;


typedef void (*dohome_wifi_csan_cb_t)(dohome_ap_info_t *ap_list, DOHOME_UINT8_T num);

typedef void (*dohome_wifi_event_cb_t)(dohome_wifi_hal_event_t event, void *data);

dohome_op_ret dohome_hal_wifi_get_ip(DOHOME_CONST dohome_wifi_if_t wf, dohome_net_ip_t *ip);

dohome_op_ret dohome_hal_wifi_get_mac(DOHOME_CONST dohome_wifi_if_t wf, dohome_net_mac_t *mac);

dohome_op_ret dohome_hal_wifi_set_mode(DOHOME_CONST dohome_wifi_mode_t mode);
dohome_wifi_mode_t dohome_hal_wifi_get_mode(void);

dohome_op_ret dohome_hal_wifi_ap_start(DOHOME_CONST dohome_wifi_cfg_if_t *cfg);
dohome_op_ret dohome_hal_wifi_ap_stop(void);

dohome_op_ret dohome_hal_wifi_get_connect_rssi(DOHOME_INT8_T *rssi);

dohome_op_ret dohome_hal_wifi_station_connect(DOHOME_CONST char *ssid, DOHOME_CONST char *passwd);
dohome_op_ret dohome_hal_wifi_station_disconnect(void);

DOHOME_UINT8_T dohome_hal_wifi_get_connect_status(void);

void dohome_hal_wifi_set_event_cb(dohome_wifi_event_cb_t event_cb);

DOHOME_UINT8_T dohome_hal_wifi_get_sleep_flag(void);
void dohome_hal_wifi_set_sleep_flag(DOHOME_UINT8_T flag);

dohome_op_ret dohome_hal_wifi_scan_start(dohome_wifi_csan_cb_t csan_cb);

void dohome_hal_wifi_init(void);

#endif