#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>

#include <dohome_api.h>
#include "dohome_hal_uart.h"


void dohome_hal_putc(DOHOME_CONST DOHOME_CHAR_T c){
	printf("%c", c);
}



typedef struct {
    DOHOME_UINT8_T is_init;
    dohome_uart_recv_cb_t recv_cb;
} uart_ctrl_t;

uart_ctrl_t g_uart_ctrl[2] = {{.is_init = 0, .recv_cb = NULL}, {.is_init = 0, .recv_cb = NULL}};

static int __uart_rx_callback(void *p_arg)
{


    return 0;
}

dohome_op_ret dohome_hal_uart_init(dohome_uart_port_t port, dohome_uart_cfg_t uart_cfg, dohome_uart_recv_cb_t recv_cb){

    // uart_dev[port].config.uart_id = port;

    return OPRT_OK;
}

dohome_op_ret dohome_hal_uart_deinit(dohome_uart_port_t port){
    
    return OPRT_OK;
}

dohome_op_ret dohome_hal_uart_set_pin(dohome_uart_port_t port, dohome_uart_pin_cfg_t pin_cfg){


    return OPRT_OK;
}

DOHOME_UINT16_T dohome_hal_uart_send(dohome_uart_port_t port, DOHOME_CHAR_T *data, DOHOME_UINT16_T len){

    return 0;
}
