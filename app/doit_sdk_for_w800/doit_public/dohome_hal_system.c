#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_INFO
#define DOHOME_LOG_TAG          "hal_system"

#include "dohome_hal_system.h"
#include <dohome_log.h>
#include "stdlib.h"

#include "wm_crypto_hard.h"
#include "dohome_api.h"

dohome_rst_reason_t reason = DOHOME_RST_UNSUPPORT;

DOHOME_UINT16_T dohome_hal_get_rand(DOHOME_UINT16_T range)
{
	unsigned int out = 0;
	tls_crypto_random_init(0xFDA3C97A, CRYPTO_RNG_SWITCH_16);
	tls_crypto_random_bytes((unsigned char *)(&out), 4);
	tls_crypto_random_stop();
	out %= range;
    return out;
}

dohome_op_ret dohome_hal_sys_reset(void)
{
    tls_sys_reset();
    return OPRT_OK;
}

/*void set_rst_reason(BL_RST_REASON_E reset_reason){
    switch (reset_reason)
    {
    case BL_RST_POWER_OFF:
        reason = DOHOME_RST_POWER_OFF;
        break;
    case BL_RST_HARDWARE_WATCHDOG:
        reason = DOHOME_RST_HARDWARE_WATCHDOG;
        break;
    case BL_RST_FATAL_EXCEPTION:
        reason = DOHOME_RST_FATAL_EXCEPTION;
        break;
    case BL_RST_SOFTWARE_WATCHDOG:
        reason = DOHOME_RST_SOFTWARE_WATCHDOG;
        break;
    case BL_RST_SOFTWARE:
        reason = DOHOME_RST_SOFTWARE;
        break;
    default:
        break;
    }
}*/

dohome_rst_reason_t dohome_hal_sys_get_rst_reason(void){
    return reason;
}

DOHOME_STATIC DOHOME_UINT16_T ticktack_loop_cnt = 0;
void dohome_hal_ticktack_cb(DOHOME_UINT16_T ticktack){
    
#ifdef ENABLE_WDT  
    DOHOME_STATIC DOHOME_UINT8_T flag_wdt_init= 0;
    if (flag_wdt_init == 0) //执行一次
    {
        flag_wdt_init = 1;
        bl_wdt_init(4000);
    }
    if(ticktack_loop_cnt%(ticktack) == 0){
        // dns_set_server();
        bl_wdt_feed();
        DOHOME_LOG_D("wdt %d", ticktack_loop_cnt);
    }
#endif    

    ticktack_loop_cnt = (ticktack_loop_cnt+1)%(ticktack*10);
}
void dohome_xdelay_ms(DOHOME_UINT16_T ms){
    tls_os_time_delay(ms);
}

#include <lwip/tcpip.h>
#include <lwip/netifapi.h>
#include <lwip/dns.h>
// void dns_set_server(void){
//     ip_addr_t ip_dns_addr1,ip_dns_addr2,ip_dns_addr3,ip_dns_addr4;
//     ipaddr_aton("114.114.114.114", &ip_dns_addr1);
//     // ipaddr_aton("8.8.8.8", &ip_dns_addr2);
//     // ipaddr_aton("223.5.5.5", &ip_dns_addr3);
//     // ipaddr_aton("223.6.6.6", &ip_dns_addr4);
//     dns_setserver(0, &ip_dns_addr1);
//     // dns_setserver(1, &ip_dns_addr2);
//     // dns_setserver(2, &ip_dns_addr3);
//     // dns_setserver(3, &ip_dns_addr4);
// }

#if (defined(SHELL_ENABLE) && SHELL_ENABLE == 1)

#include <vfs.h>
#include <fdt.h>
#include <aos/kernel.h>
#include <aos/yloop.h>
#include <libfdt.h>
#include "../../../dohome_lib/dohome/inc/dohome_lib/dohome_shell.h"
#include "../../../dohome_lib/dohome/lib/letter-shell/shell.h"

static void console_cb_read(int fd, void *param)
{
    char buffer[64];  /* adapt to usb cdc since usb fifo is 64 bytes */
    int ret;

    ret = aos_read(fd, buffer, sizeof(buffer));
    if (ret > 0) {
        if (ret <= sizeof(buffer)) {
            int i = 0;
            while(ret--){
                dohome_shell_handler(buffer[i]);
                i++;
            }
        }
    }
}   


void shell_init()
{
    int fd_console = -1;
    uint32_t fdt = 0, offset = 0;

    extern int vfs_uart_init_simple_mode(uint8_t id, uint8_t pin_tx, uint8_t pin_rx, int baudrate, const char *path);
    vfs_uart_init_simple_mode(0, 16, 7, 2000000, "/dev/ttyS0");

    fd_console = aos_open("/dev/ttyS0", 0);
    if (fd_console >= 0) {
        aos_poll_read_fd(fd_console, console_cb_read, (void*)0x12345678);
    }
}


dohome_op_ret dohome_hal_shell_init(void)
{
    DOHOME_LOG_I("shell_init");

    shell_init();
    return OPRT_OK;
}

void shell_info(void)
{
    shellShowInfo(dohome_shell);
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC)|SHELL_CMD_PARAM_NUM(0)|SHELL_CMD_DISABLE_RETURN, shell_info, shell_info, shell txt info);


#endif