#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_INFO
#define DOHOME_LOG_TAG          "hal_gpio"

#include "dohome_hal_gpio.h"

#include <stdio.h>

#include "dohome_log.h"



DOHOME_UINT8_T dohome_hal_gpio_read(dohome_gpio_t gpio){
    DOHOME_UINT8_T level = 0;
	level = tls_gpio_read(gpio);
    return level;
}

void dohome_hal_gpio_write(dohome_gpio_t gpio, DOHOME_UINT8_T level){
	tls_gpio_write(gpio, level);
}

void dohome_hal_gpio_init(dohome_gpio_t gpio, dohome_gpio_inout_t inout, dohome_gpio_pull_t pull){
    tls_gpio_cfg(gpio, inout, pull);
}