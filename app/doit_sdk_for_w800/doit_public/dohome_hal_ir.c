#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_DEBUG
#define DOHOME_LOG_TAG          "hal_gpio"

#include "dohome_hal_remote.h"

#include <stdio.h>
#include <stdlib.h>

#include "dohome_api.h"
#include "dohome_log.h"


static uint8_t ir_is_init = 0;
static remote_ctrl_cb_t remote_ctrl_cb = NULL;

static uint32_t _ir_data = 0;
static uint16_t _ir_repeat = 0;
static uint32_t _ir_last_data = 0;

#define IR_NEC_ENABLED_REPEAT   1

#define IR_NEC_STATE_IDLE       0
#define IR_NEC_STATE_PRE        1
#define IR_NEC_STATE_CMD        2
#define IR_NEC_STATE_REPEAT     3

#define IR_NEC_BIT_NUM          8
#define IR_NEC_MAGIN_US         400
#define IR_NEC_TM_PRE_US        13500
#define IR_NEC_D1_TM_US         2250
#define IR_NEC_D0_TM_US         1120
#define IR_NEC_REP_TM1_US       20000
#define IR_NEC_REP_TM2_US       11250
#define IR_NEC_REP_LOW_US       2250
#define IR_NEC_REP_CYCLE        110000
  
#define IR_NEC_HEADER_HIGH_US   9000
#define IR_NEC_HEADER_LOW_US    4500
#define IR_NEC_DATA_HIGH_US     560
#define IR_NEC_DATA_LOW_1_US    1690
#define IR_NEC_DATA_LOW_0_US    560

static uint64_t time_us_stamp = 0;
static uint64_t system_tick_count = 0;

static int data_check(uint32_t data)
{
    return 0;
}

static inline int ir_get_addr(uint32_t val)
{
    return (val & 0xFFFF);
}

static inline int ir_get_cmd(uint32_t val)
{
    return ((val & 0xFF0000) >> 16);
}

void system_tick_clear()
{
	
}

void ststem_tick_init()
{
	
}

uint64_t system_tick_get()
{
    return 0;
}

void system_tick_start()
{
	
}

void sw_ir_interrupt_callback(void *pstnode)
{
    
}

/*static void event_ir_cb(input_event_t *event, void *private_data)
{
    
}*/

void infrared_loop(void *arg);

void dohome_infrared_init(dohome_gpio_t gpio, remote_ctrl_cb_t callback){
    DOHOME_LOG_L("dohome_hal_ir gpio = %d", gpio);

    
}

DOHOME_UINT16_T ir_long_time = 3000;

void dohome_infrared_set_long_press_time(DOHOME_UINT8_T time){
    ir_long_time = time;
}

void infrared_loop(void *arg){

    
}

