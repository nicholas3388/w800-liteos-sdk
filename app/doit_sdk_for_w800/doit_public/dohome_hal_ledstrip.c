#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_DEBUG
#define DOHOME_LOG_TAG          "hal_leds"


#include "dohome_hal_ledstrip.h"


#include <dohome_log.h>

#include "dohome_hal_mem.h"


// DOHOME_STATIC pixeltype_t pixels_type;
DOHOME_STATIC DOHOME_UINT16_T pixels_number;
DOHOME_STATIC pixel_color_type_t pixels_color_type;

DOHOME_STATIC uint32_t *pixels_buffer;
DOHOME_STATIC DOHOME_UINT32_T pixels_buffer_size;

dohome_op_ret dohome_hal_ledstrip_set_pixels_num(DOHOME_UINT16_T pixels_num){

    return OPRT_OK;
}

dohome_op_ret dohome_hal_ledstrip_set_color_type(pixel_color_type_t color_type){
    pixels_color_type = color_type;
    return OPRT_OK;
}

pixel_color_type_t dohome_hal_ledstrip_get_color_type(void){
    return pixels_color_type;
}

static uint8_t send_status = 0;
void irled_update(void *arg){
    

}

dohome_op_ret dohome_hal_ledstrip_set_gpio(DOHOME_UINT16_T gpio){
    return OPRT_OK;
}

dohome_op_ret dohome_hal_ledstrip_init(DOHOME_UINT16_T pixels_num, pixeltype_t type, pixel_color_type_t color_type)
{

    return OPRT_OK;
}


uint8_t byte_change(uint8_t data)
{

	return 0;
}


void dohome_hal_ledstrip_update(pixel_led_t *pixels)
{
    
}
