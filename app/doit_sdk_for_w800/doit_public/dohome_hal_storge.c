#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_DEBUG
#define DOHOME_LOG_TAG          "hal_storge"
#include <dohome_type.h>
#include <dohome_hal_storge.h>
#include <dohome_error_code.h>
#include <stdint.h>

#include <dohome_log.h>

void dohome_hal_storge_init(void){
	tls_fls_init();	//initialize flash driver
}

dohome_op_ret dohome_hal_storge_read(DOHOME_UINT32_T addr, DOHOME_UINT32_T *buf, size_t size){
    tls_fls_read(addr, buf, size);
    return OPRT_OK;
}

dohome_op_ret dohome_hal_storge_write(DOHOME_UINT32_T addr,DOHOME_CONST DOHOME_UINT32_T *buf, size_t size){
    tls_fls_write(addr, buf, size);
    return OPRT_OK;
}


dohome_op_ret dohome_hal_storge_erase(DOHOME_UINT32_T addr, size_t size){
	DOHOME_LOG_D("erase flash not support now!\n");
    return OPRT_OK;
}


void dohome_hal_storge_lock(void){
	__asm volatile("psrclr ie");
}


void dohome_hal_storge_unlock(void){
	__asm volatile("psrset ie");
}


dohome_op_ret dohome_hal_storge_verify_init(void){
    
    return OPRT_OK;
}
dohome_op_ret dohome_hal_storge_verify_write(DOHOME_UINT8_T *verify, DOHOME_UINT16_T len){


    return OPRT_OK;
}

dohome_op_ret dohome_hal_storge_verify_read(DOHOME_UINT8_T *verify, DOHOME_UINT16_T len){

    
    return OPRT_OK;
}

