#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_INFO
#define DOHOME_LOG_TAG          "hal_pwm"

#include "dohome_hal_pwm.h"

#include <stdint.h>
#include <stdio.h>

#include "dohome_log.h"



DOHOME_STATIC DOHOME_UINT32_T max_duty = 255;
DOHOME_STATIC DOHOME_UINT8_T pwm_channel[] = {0,1,2,3,4,0,0xff,0xff,3,0xff,0xff,1,2,0xff,4,0xff,0xff,2,0xff,0xff,0,1,2};

void dohome_hal_pwm_init(dohome_pwm_gpio_t gpio, DOHOME_UINT32_T freq, DOHOME_UINT32_T duty){

}


void dohome_hal_pwm_stop(dohome_pwm_gpio_t gpio){
    
}


void dohome_hal_pwm_start(dohome_pwm_gpio_t gpio){
    
}

void dohome_hal_pwm_set_duty(dohome_pwm_gpio_t gpio, DOHOME_UINT32_T duty){

    
}

void dohome_hal_pwm_set_max_duty(DOHOME_UINT32_T duty){

    max_duty = duty;
}