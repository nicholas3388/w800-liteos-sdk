#include "dohome_hal_mem.h"

#include <stdlib.h>

#include "dohome_log.h"

void *dohome_hal_mem_alloc(DOHOME_UINT32_T size){
    void *tmp_addr = 0;
    
    tmp_addr = pvPortMalloc(size);

    if(!tmp_addr){
        DOHOME_LOG_E("%x\n", OPRT_MALLOC_FAILED);
        return NULL;
    }

    return tmp_addr;
}

dohome_op_ret dohome_hal_mem_free(void *p){
    if(!p){
        return OPRT_COM_ERROR;
    }
    
    vPortFree(p);
    return OPRT_OK;
}
