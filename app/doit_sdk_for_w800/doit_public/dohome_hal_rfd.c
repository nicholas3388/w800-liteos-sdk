
#include "dohome_type.h"
#include "dohome_hal_remote.h"

#include <string.h>


/*--------------------------------------------------------------------------
* S_Queue
* 描述：  系统平台头文件
* 设计者：zifeng.lin
* 创建日期：2012年7月27日
* 版本号：VER-0.2
---------------------------------------------------------------------------*/

/* 结构声明 */
typedef struct
{
	DOHOME_UINT8_T *Head;
	DOHOME_UINT8_T *Tail;
	DOHOME_UINT8_T Buff[8 + 1];
}    Queue8;
typedef struct
{
	DOHOME_UINT8_T *Head;
	DOHOME_UINT8_T *Tail;
	DOHOME_UINT8_T Buff[16 + 1];
}   Queue16;
typedef struct
{
	DOHOME_UINT8_T *Head;
	DOHOME_UINT8_T *Tail;
	DOHOME_UINT8_T Buff[180 + 1];
}  Queue180;
typedef struct
{
	DOHOME_UINT8_T *Head;
	DOHOME_UINT8_T *Tail;
	DOHOME_UINT8_T Buff[256 + 1];
}  Queue256;
/*-------------------------------------------------------------------------------------------------------
*  函数声明
-------------------------------------------------------------------------------------------------------*/
void S_QueueEmpty(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT8_T *HBuff);
void S_QueueDataIn(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT8_T *HBuff, DOHOME_UINT16_T Len, DOHOME_UINT8_T *HData, DOHOME_UINT16_T DataLen);
DOHOME_UINT8_T S_QueueDataOut(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT8_T *HBuff, DOHOME_UINT16_T Len, DOHOME_UINT8_T *Data);
DOHOME_UINT16_T S_QueueDataLen(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT16_T Len);
DOHOME_UINT16_T S_QueueResLen(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT16_T Len);

/*-------------------------------------------------------------------------------------------------------
*  执行代码
-------------------------------------------------------------------------------------------------------*/
/********************************************************************************************************
*  Function: S_QueueEmpty
*  Object:  清空队列
*  输入： 队列头指针地址 队列尾指针地址 队列数据头地址
*  输出： 无
*  备注： 无
********************************************************************************************************/
void S_QueueEmpty(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT8_T *HBuff)
{
	*Head = HBuff;
	*Tail = HBuff;
}

/********************************************************************************************************
*  Function: S_QueueDataIn
*  Object:  数据进队列 可多字节操作
*  输入： 队列头指针地址 队列尾指针地址 队列数据头地址	队列长 待存数据区头指针  待存数据长
*  输出： 无
*  备注： 无
********************************************************************************************************/
void S_QueueDataIn(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT8_T *HBuff, DOHOME_UINT16_T Len, DOHOME_UINT8_T *HData, DOHOME_UINT16_T DataLen)
{
	DOHOME_UINT16_T num;
	for (num = 0; num < DataLen; num++, HData++)
	{
		**Tail = *HData;
		(*Tail)++;
		if (*Tail == HBuff + Len)
		{
			*Tail = HBuff;
		}
		if (*Tail == *Head)
		{
			if (++(*Head) == HBuff + Len)
			{
				*Head = HBuff;
			}
		}
	}
	//Derive_OpenEA();
}

/********************************************************************************************************
*  Function: S_QueueDataOut
*  Object:  数据出队列 单字节操作
*  输入： 队列头指针地址 队列尾指针地址 队列数据头地址	队列长 待存数据头指针
*  输出： 0队列空 1读成功
*  备注： 无
********************************************************************************************************/
DOHOME_UINT8_T S_QueueDataOut(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT8_T *HBuff, DOHOME_UINT16_T Len, DOHOME_UINT8_T *Data)
{
	DOHOME_UINT8_T back = 0;
	//Derive_CloseEA();
	*Data = 0;
	if (*Tail != *Head)
	{
		*Data = **Head;
		back = 1;
		if (++(*Head) == HBuff + Len)
		{
			*Head = HBuff;
		}
	}
	//Derive_OpenEA();
	return back;
}

/********************************************************************************************************
*  Function: S_QueueDataLen
*  Object:  询问队列长
*  输入： 队列头指针地址 队列尾指针地址 队列长
*  输出： 队列数据长
*  备注： 无
********************************************************************************************************/
DOHOME_UINT16_T S_QueueDataLen(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT16_T Len)
{
	//if(*Tail == *Head)
	//	return 0;
	if (*Tail > *Head)
	{
		return *Tail - *Head;
	}
	if (*Tail < *Head)
	{
		return *Tail + Len - *Head;
	}
	return 0;
}

/********************************************************************************************************
*  Function: S_QueueResLen
*  Object:  询问队列空余空间
*  输入： 队列头指针地址 队列尾指针地址 队列长
*  输出： 空余空间长
*  备注： 无
********************************************************************************************************/
DOHOME_UINT16_T S_QueueResLen(DOHOME_UINT8_T **Head, DOHOME_UINT8_T **Tail, DOHOME_UINT16_T Len)
{
	return Len - 1 - S_QueueDataLen(Head, Tail, Len);
}


/*-------------------------------------------------------------------------
* RFD
* 描述：  RF遥控按键解码
* 设计者：
* 创建日期：
*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------
*  内部声明
-------------------------------------------------------------------------------------------------------*/
//任务状态
enum
{
	RFD_READ_CLKLEN,             //测时钟的长
	RFD_READ_DATA                //解码读数
};

enum
{
    PARA_1527_POWER_1 = 0x0e,
    PARA_1527_POWER_2 = 0x89,
    PARA_1527_POWERON_1 = 0x02,  
    PARA_1527_POWERON_2 = 0x04,     
    PARA_1527_POWERON_3 = 0x01,    
    PARA_1527_POWERON_4 = 0x08,  
    PARA_1527_POWEROFF = 0x00,      //关闭
    PARA_1527_POWERON = 0x07,       //打开
};

//引导码HCLK : LCLK 时钟比区间范围(典型为30)
#define  RFD_TITLE_CLK_MINL  20
#define  RFD_TITLE_CLK_MAXL  44

//数据HCLK : LCLK 时钟比区间范围(典型为3)
#define  RFD_DATA_CLK_MINL   2
#define  RFD_DATA_CLK_MAXL   5

//重复值滤波时间
#define  RFD_NORMAL_DELDOUBLE_TIME    (T500MS+T100MS)

//波形结束标志
#define  RUNEND              0xff
//定义发送1clk时间长(单位us)
#define  RFD_CLK_SENDLEN     434 //7*62
//定义发送重复次数
#define  RFD_RESEND_NUM      15

//中断执行速度(x个us中断一次)
#define  RFD_INT_TIME        60

#define QueueEmpty(x)	   S_QueueEmpty((DOHOME_UINT8_T**)&(x).Head,(DOHOME_UINT8_T**)&(x).Tail,(DOHOME_UINT8_T*)(x).Buff)
#define QueueDataIn(x,y,z) S_QueueDataIn((DOHOME_UINT8_T**)&(x).Head,(DOHOME_UINT8_T**)&(x).Tail,(DOHOME_UINT8_T*)(x).Buff,sizeof((x).Buff),(DOHOME_UINT8_T*)(y),(z))
#define QueueDataOut(x,y)  S_QueueDataOut((DOHOME_UINT8_T**)&(x).Head,(DOHOME_UINT8_T**)&(x).Tail,(DOHOME_UINT8_T*)(x).Buff,sizeof((x).Buff),(y))
#define QueueDataLen(x)	   S_QueueDataLen((DOHOME_UINT8_T**)&(x).Head,(DOHOME_UINT8_T**)&(x).Tail,sizeof((x).Buff))
#define QueueResLen(x)	   S_QueueResLen((DOHOME_UINT8_T**)&(x).Head,(DOHOME_UINT8_T**)&(x).Tail,sizeof((x).Buff))

/*-------------------------------------------------------------------------------------------------------
*  资源定义
-------------------------------------------------------------------------------------------------------*/
//任务状态寄存器
DOHOME_UINT8_T RFDProState;
//定义一个队列存RFDIO采样状态
volatile Queue180 RFDBuff;

//RFD检测输出码队列
Queue16 RFDCodeBuff;

//RFD发送码队列
Queue8 RFDSendBuff;

//发送波形定义 每帧2字节 时间长(x*RFD_CLK_SENDLEN)+IO状态
//"Head"
const DOHOME_UINT8_T RFD_Head[] = {1, 1, 31, 0, RUNEND};
//"1"
const DOHOME_UINT8_T RFD_1[] = {3, 1, 1, 0, RUNEND};
//"0"
const DOHOME_UINT8_T RFD_0[] = {1, 1, 3, 0, RUNEND};

//发送控制域
DOHOME_UINT8_T RFD_ResendNum;
DOHOME_UINT8_T RFD_Addr[3];

DOHOME_UINT8_T StaFlag;

DOHOME_UINT8_T RFD_REC_Enable;

//extern DOHOME_UINT8_T Function_RunState;
/*-------------------------------------------------------------------------------------------------------
*  函数声明
-------------------------------------------------------------------------------------------------------*/
void RFDProInit(void);
void RFDPro(void);
void RFD_CodeHandle(DOHOME_UINT8_T *Code);
void RFD_SendHandle(void);
void RFD_Send(DOHOME_UINT8_T Cmd);
void RFD_SampTimerInterrupt_Handle(void);

void Drive_RFTimerON(void);         //开启RF应用定时器 用于脉冲采样 62us中断一次
DOHOME_UINT8_T Drive_RFDRx(void);              //RF串口RX-IO状态
void Drive_RFDTx(DOHOME_UINT8_T sta);          //RF串口TX-IO控制
void RFD_SampTimerInterrupt_Handle(void);    //应用定时器中断接入

/*-------------------------------------------------------------------------------------------------------
*  执行代码
-------------------------------------------------------------------------------------------------------*/

/********************************************************************************************************
*  Function: RFDProInit
*  Object: RFDProInit初始化
*  输入： 无
*  输出： 无
*  备注： 无
********************************************************************************************************/
void RFDProInit(void)
{
#if	DEBUG_EN
	printf("RFDInit..\r\n");
#endif
	//参数初始化
	RFD_REC_Enable = 0;
	RFDProState = RFD_READ_CLKLEN;
	QueueEmpty(RFDBuff);
	QueueEmpty(RFDCodeBuff);
	QueueEmpty(RFDSendBuff);
	RFD_ResendNum = 0;
	memset(RFD_Addr, 0, sizeof(RFD_Addr));
	//硬件初始化
	Drive_RFTimerON();
}

/********************************************************************************************************
*  Function: RFDPro
*  Object: RFDPro入口
*  输入： 无
*  输出： 无
*  备注： 无
********************************************************************************************************/
void RFDPro(void)
{
	//定义一个ClkTimeBuff测脉冲的宽
	Queue256 ClkTimeBuff;
	static DOHOME_UINT16_T Time1 = 0, Time2 = 0;
	static DOHOME_UINT8_T ReadDataFlag = 0;
	static DOHOME_UINT8_T Len, Code[3];
	//接收处理
	switch (RFDProState)
	{
	//测时钟的长
	case RFD_READ_CLKLEN:
		{
			DOHOME_UINT8_T Temp, Num;
			static DOHOME_UINT8_T Dsta = 0;
			static DOHOME_UINT16_T Count = 0;
			QueueEmpty(ClkTimeBuff);
			while (QueueDataOut(RFDBuff, &Temp))
			{
				Num = 8;
				//解析8个BIT 寻找跳变
				while (Num--)
				{
					Count++;
					if (Dsta)
					{
						//等待0
						if (!(Temp & 0x0080))
						{
							DOHOME_UINT8_T Data;
							Data = Count / (255 + 1);
							Data |= 0x0080;
							QueueDataIn(ClkTimeBuff, &Data, 1);
							Data = Count % (255 + 1);
							QueueDataIn(ClkTimeBuff, &Data, 1);
							Dsta = 0;
							Count = 0;
						}
					}
					else
					{
						//等待1
						if (Temp & 0x0080)
						{
							DOHOME_UINT8_T Data;
							Data = Count / (255 + 1);
							Data &= 0xFF7F;
							QueueDataIn(ClkTimeBuff, &Data, 1);
							Data = Count % (255 + 1);
							QueueDataIn(ClkTimeBuff, &Data, 1);
							Dsta = 1;
							Count = 0;
						}
					}
					Temp <<= 1;
				}
			}
		}
	//读码解数
	case RFD_READ_DATA:
		{
			while (QueueDataLen(ClkTimeBuff))
			{
				//读引导码
				if (!ReadDataFlag)
				{
					DOHOME_UINT8_T Temp;
					while (!Time1 || !Time2)
					{
						//装载HCLK TIMER
						if (!Time1)
						{
							while (QueueDataOut(ClkTimeBuff, &Temp))
							{
								if (Temp & 0x0080)
								{
									Temp &= 0xFF7F;
									Time1 = Temp * (255 + 1);
									QueueDataOut(ClkTimeBuff, &Temp);
									Time1 += Temp;
									Time2 = 0;
									break;
								}
								else
								{
									QueueDataOut(ClkTimeBuff, &Temp);
								}
							}
							if (!QueueDataLen(ClkTimeBuff))
							{
								break;
							}
						}
						//装载LCLK TIMER
						if (!Time2)
						{
							QueueDataOut(ClkTimeBuff, &Temp);
							Time2 = Temp * (255 + 1);
							QueueDataOut(ClkTimeBuff, &Temp);
							Time2 += Temp;
							//判断引导码
							if ((Time2 >= Time1 * RFD_TITLE_CLK_MINL) && (Time2 <= Time1 * RFD_TITLE_CLK_MAXL))
							{
								Time1 = 0;
								Time2 = 0;
								Len = 0;
								ReadDataFlag = 1;
								break;
							}
							else
							{
								Time1 = 0;
								Time2 = 0;
							}
						}
					}
				}
				//读数据码
				if (ReadDataFlag)
				{
					DOHOME_UINT8_T Temp;
					//装载HCLK TIMER
					if (!Time1)
					{
						if (QueueDataOut(ClkTimeBuff, &Temp))
						{
							Temp &= 0xFF7F;
							Time1 = Temp * (255 + 1);
							QueueDataOut(ClkTimeBuff, &Temp);
							Time1 += Temp;
							Time2 = 0;
						}
						else
						{
							break;
						}
					}
					//装载LCLK TIMER
					if (!Time2)
					{
						if (QueueDataOut(ClkTimeBuff, &Temp))
						{
							DOHOME_UINT8_T RecvSuccFlag;
							Time2 = Temp * (255 + 1);
							QueueDataOut(ClkTimeBuff, &Temp);
							Time2 += Temp;
							//判断数据码
							if ((Time1 > Time2 * RFD_DATA_CLK_MINL) && (Time1 <= Time2 * RFD_DATA_CLK_MAXL))
							{
								DOHOME_UINT8_T i, c = 0x0080;
								//'1'
								for (i = 0; i < Len % 8; i++)
								{
									c >>= 1;
									c &= 0xFF7F;
								}
								Code[Len / 8] |= c;
								RecvSuccFlag = 1;
							}
							else if ((Time2 > Time1 * RFD_DATA_CLK_MINL) && (Time2 <= Time1 * RFD_DATA_CLK_MAXL))
							{
								DOHOME_UINT8_T i, c = (DOHOME_UINT8_T)0xFF7F;
								//'0'
								for (i = 0; i < Len % 8; i++)
								{
									c >>= 1;
									c |= 0x0080;
								}
								Code[Len / 8] &= c;
								RecvSuccFlag = 1;
							}
							else
							{
								//error
								RecvSuccFlag = 0;
								ReadDataFlag = 0;
							}
							Time1 = 0;
							Time2 = 0;
							if (++Len == 24 && RecvSuccFlag)
							{

								ReadDataFlag = 0;
#if RFDPRO_DEBUG
//                                    printf("\r\nRFDPro: %02x %02x %02x  ",Code[0],Code[1],Code[2]);

								/*
								                                    DEBUG_VOUT("\n%x", Code[0]/(HALFBYTEMAX+1));
								                                    DEBUG_VOUT("%x", Code[0]%(HALFBYTEMAX+1));
								                                    DEBUG_VOUT("%x", Code[1]/(HALFBYTEMAX+1));
								                                    DEBUG_VOUT("%x", Code[1]%(HALFBYTEMAX+1));
								                                    DEBUG_VOUT("%x", Code[2]/(HALFBYTEMAX+1));
								                                    DEBUG_VOUT("%x", Code[2]%(HALFBYTEMAX+1));
								*/
#endif
								//解码处理
								RFD_CodeHandle(Code);

							}
						}
						else
						{
							break;
						}
					}
				}
			}
		}
	default:
		break;
	}
	//发送处理
	if (!RFD_ResendNum)
	{
		DOHOME_UINT8_T Temp;
		//读一个发送要求
		QueueDataOut(RFDSendBuff, &Temp);
		if (Temp == '*')
		{
			QueueDataOut(RFDSendBuff, &RFD_Addr[2]);
			QueueDataOut(RFDSendBuff, &RFD_Addr[1]);
			QueueDataOut(RFDSendBuff, &RFD_Addr[0]);
			RFD_ResendNum = RFD_RESEND_NUM;
#if	RFDPRO_DEBUG
			printf("Send: ");
			printf("%02x ", RFD_Addr[2]);
			printf("%02x ", RFD_Addr[1]);
			printf("%02x\r\n", RFD_Addr[0]);
			/*
			DEBUG_IOUT("\nSend:");
			DEBUG_VOUT("%x", RFD_Addr[2]/0x10);
			DEBUG_VOUT("%x", RFD_Addr[2]%0x10);
			DEBUG_VOUT("%x", RFD_Addr[1]/0x10);
			DEBUG_VOUT("%x", RFD_Addr[1]%0x10);
			DEBUG_VOUT("%x", RFD_Addr[0]/0x10);
			DEBUG_VOUT("%x", RFD_Addr[0]%0x10);
			*/
#endif
		}
	}
}

/********************************************************************************************************
*  Function: RFD_CodeHandle
*  Object: RFD采集键值进队列
*  输入： 无
*  输出： 无
*  备注： 无
********************************************************************************************************/
void RFD_CodeHandle(DOHOME_UINT8_T *Code)
{
	static DOHOME_UINT8_T CodeTemp[3] = {0, 0, 0};
	static DOHOME_UINT8_T Time_Wait = 0;
	// static DOHOME_UINT8_T rfd_file_count = 0;
	//滤去重复值

	if (CodeTemp[0] == *Code && CodeTemp[1] == *(Code + 1) && CodeTemp[2] == *(Code + 2))
	{
		// if(Time_Wait ++ < 5)
		// {
		// 	printf("-\r\n");
		// 	return;
		// }
	}

	Time_Wait = 0;
	memcpy(CodeTemp, Code, 3);
	QueueDataIn(RFDCodeBuff, (DOHOME_UINT8_T *)"*", 1);
	QueueDataIn(RFDCodeBuff, Code, 3);

	
// 	if(Time_Wait == 0)
// 	{

// 	}
// 	Time_Wait ++;
// 	if (CodeTemp[0] == *Code && CodeTemp[1] == *(Code + 1) && CodeTemp[2] == *(Code + 2))
// 	{
// 		/*
// 		if(S_GetTimer())
// 			{
// 					S_SetTimer(RFD_NORMAL_DELDOUBLE_TIME);
// 				//	if(!LedBusy[LED7]&& Function_RunState == 0)//RF信号指示灯判忙
// 						LEDProSend(LED7_Short2);
// 					return;
// 			}
// 		*/
// 		if (Time_Wait < 5)
// 		{
// //				printf("-");
// 			return;		//重复键值不处理
// 		}
// 		// rfd_file_count = 1;
// 	}

// 	Time_Wait = 0;

// 	memcpy(CodeTemp, Code, 3);
// 	//S_SetTimer(RFD_NORMAL_DELDOUBLE_TIME);
	//存码值
#if RFDPRO_DEBUG
			//printf("Recv: %02x %02x %02x\r\n",Code[0],Code[1],Code[2]);

//        	DEBUG_VOUT("\n%x", Code[0]/(HALFBYTEMAX+1));
//			DEBUG_VOUT("%x", Code[0]%(HALFBYTEMAX+1));
//			DEBUG_VOUT("%x", Code[1]/(HALFBYTEMAX+1));
//			DEBUG_VOUT("%x", Code[1]%(HALFBYTEMAX+1));
//			DEBUG_VOUT("%x", Code[2]/(HALFBYTEMAX+1));
//			DEBUG_VOUT("%x", Code[2]%(HALFBYTEMAX+1));
#endif
// 	// if (rfd_file_count == 1)
// 	// {
// 	// 	rfd_file_count = 2;
// 		QueueDataIn(RFDCodeBuff, (DOHOME_UINT8_T *)"*", 1);
// 		QueueDataIn(RFDCodeBuff, Code, 3);
// 	//}	

}


/********************************************************************************************************
*  Function: RFD_SampTimerInterrupt_Handle
*  Object: RFD采样时钟中断处理函数
*  输入： 无
*  输出： 无
*  备注： 62us中断一次
********************************************************************************************************/
void RFD_SampTimerInterrupt_Handle(void)
{
	static DOHOME_UINT8_T Temp, Count1 = 0;
	static DOHOME_UINT16_T Count2;
	//---------- Recv Handle ------------
	//每位占一个BIT
	Temp <<= 1;
	if (Drive_RFDRx())
	{
		Temp |= 0x0001;
	}
	else
	{
		Temp &= 0xFFFE;
	}
	//存一个字节
	if (++Count1 == 8)
	{
		Count1 = 0;
		QueueDataIn(RFDBuff, &Temp, 1);
	}

	//---------- Send Handle -------------
	if (Count2)
	{
		Count2--;
	}
	if (!Count2)
	{
		Count2 = RFD_CLK_SENDLEN / RFD_INT_TIME;
		//Send Control..
		RFD_SendHandle();
	}
}

/********************************************************************************************************
*  Function: RFD_CHOSE
*  Object: 片选地址位
*  输入： 无
*  输出： 无
*  备注： 无
********************************************************************************************************/
DOHOME_UINT8_T RFD_CHOSE(DOHOME_UINT32_T BitChose)
{
	if (BitChose >> 16)
	{
		if (RFD_Addr[2] & (BitChose >> 16))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if (BitChose >> 8)
	{
		if (RFD_Addr[1] & (BitChose >> 8))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		if (RFD_Addr[0]&BitChose)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

/********************************************************************************************************
*  Function: RFD_SendHandle
*  Object: RFD低层发送控制
*  输入： 无
*  输出： 无
*  备注： 无
********************************************************************************************************/
void RFD_SendHandle(void)
{
	static DOHOME_UINT8_T SendStep = 0, Timer = 0;
	static DOHOME_UINT8_T *pSend = (DOHOME_UINT8_T *)&RFD_Head;
	static DOHOME_UINT32_T BitChose;
	if (RFD_ResendNum)
	{
		switch (SendStep)
		{
		case 0:
			{
				//Send Head..
				while (!Timer)
				{
					if (*pSend == RUNEND)
					{
						BitChose = 0x00800000;
						//Read First Data..
						if (RFD_CHOSE(BitChose))
						{
							pSend = (DOHOME_UINT8_T *)&RFD_1;
							Timer = 0;
						}
						else
						{
							pSend = (DOHOME_UINT8_T *)&RFD_0;
							Timer = 0;
						}
						SendStep++;
						break;
					}
					else
					{
						Timer = *pSend;
						pSend++;
						Drive_RFDTx(*pSend);
						pSend++;
					}
				}
				if (Timer)
				{
					break;
				}
			}
		case 1:
			{
				//Send data..
				while (!Timer)
				{
					if (*pSend == RUNEND)
					{
						if (!BitChose)
						{
							//send over
							if (RFD_ResendNum)
							{
								RFD_ResendNum--;
							}
							Timer = 0;
							SendStep = 0;
							pSend = (DOHOME_UINT8_T *)&RFD_Head;
							break;
						}
						else
						{
							BitChose >>= 1;
							//Read Data continue
							if (RFD_CHOSE(BitChose))
							{
								pSend = (DOHOME_UINT8_T *)&RFD_1;
								Timer = 0;
							}
							else
							{
								pSend = (DOHOME_UINT8_T *)&RFD_0;
								Timer = 0;
							}
						}
					}
					else
					{
						Timer = *pSend;
						pSend++;
						Drive_RFDTx(*pSend);
						pSend++;
					}
				}
			}
		}
		//Timer计时处理..
		if (Timer)
		{
			Timer--;
		}
	}
}

#define PARA_NativeWirelessID   0x1834
void RFD_Send(DOHOME_UINT8_T Cmd)
{
	if (QueueResLen(RFDSendBuff))
	{
		DOHOME_UINT8_T temp;
//		if (PARA_WirelessEN == 0)
//		{
//			return;
//		}
		temp = '*';
		QueueDataIn(RFDSendBuff, &temp, 1);//数据头

		temp = PARA_NativeWirelessID / 0x100;
		QueueDataIn(RFDSendBuff, &temp, 1);//低地址

		temp = PARA_NativeWirelessID % 0x100;
		QueueDataIn(RFDSendBuff, &temp, 1);//高地址
		switch (Cmd)							 //指令
		{
		case PARA_1527_POWERON:
			temp = PARA_1527_POWERON;
			QueueDataIn(RFDSendBuff, &temp, 1);
			break;
		case PARA_1527_POWEROFF:
			temp = PARA_1527_POWEROFF;
			QueueDataIn(RFDSendBuff, &temp, 1);
			break;
		}
	}
}

/*****************************************END***********************************************************/

/*-------------------------------------------------------------------------------------------------------
*  实现函数
*  void Drive_RFTimerON(void)                   -> 开启RF应用定时器 用于脉冲采样 62us中断一次
*  DOHOME_UINT8_T Drive_RFDRx(void)                       -> RF串口RX-IO状态
*  void Drive_RFDTx(DOHOME_UINT8_T sta)                   -> RF串口TX-IO控制
*  void RFD_SampTimerInterrupt_Handle(void)      <- 应用定时器中断接入
-------------------------------------------------------------------------------------------------------*/
#include "dohome_hal_gpio.h"
#include "dohome_hal_timer.h"

#include "dohome_log.h"


// #include "hosal_timer.h"

// dohome_gpio_t _rx_gpio = 23; 

// static void int_timer_cb(void *arg)
// {
// 	RFD_SampTimerInterrupt_Handle();
// }

// //开启RF应用定时器 用于脉冲采样 62us中断一次
// void Drive_RFTimerON(void)
// {
//     dohome_hal_gpio_init(_rx_gpio, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLHIGH);

// 	hosal_timer_dev_t timer;
// 	timer.port = 1;
//     timer.config.period = RFD_INT_TIME; 
//     timer.config.reload_mode = TIMER_RELOAD_PERIODIC;
//     timer.config.cb = int_timer_cb;
//     timer.config.arg = NULL;
//     hosal_timer_init(&timer);
//     hosal_timer_start(&timer);
// }

// //RF串口RX-IO状态
// DOHOME_UINT8_T Drive_RFDRx(void)
// {
//     return dohome_hal_gpio_read(_rx_gpio);
// }

// //RF串口TX-IO控制
// void Drive_RFDTx(DOHOME_UINT8_T sta)
// {
//     return;
// }


dohome_gpio_t _rx_gpio = 23;


static void int_timer_cb(void)
{
    
}

void samp_timer_cb(void *arg)
{
    RFD_SampTimerInterrupt_Handle();
}

//开启RF应用定时器 用于脉冲采样 62us中断一次
void Drive_RFTimerON(void)
{
    
}

//RF串口RX-IO状态
DOHOME_UINT8_T Drive_RFDRx(void)
{
    return dohome_hal_gpio_read(_rx_gpio);
}

//RF串口TX-IO控制
void Drive_RFDTx(DOHOME_UINT8_T sta)
{
    return;
}


/*****************************************************************************************************************************/

remote_ctrl_cb_t _button_cb = NULL;

DOHOME_UINT8_T Function_CurrentRFD[4];

void dohome_rfd_loop(void *arg){
	
}

void dohome_rfd_init(dohome_gpio_t gpio, remote_ctrl_cb_t button_cb){

	
}