#include "dohome_hal_md5.h"

#include "mbedtls/md5.h"

dohome_op_ret dohome_hal_md5_gen(DOHOME_INT8_T *input, DOHOME_INT8_T ilen, DOHOME_UINT8_T output[16])
{
    // void mbedtls_md5( const unsigned char *input, size_t ilen, unsigned char output[16] )
    mbedtls_md5((const unsigned char *)input, ilen, output);
    return OPRT_OK;
}