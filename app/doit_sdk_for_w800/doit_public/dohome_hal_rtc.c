
#define LOG_LVL     LOG_LVL_DEBUG
#define LOG_TAG     "hal_rtc"



#include "dohome_log.h"

#include "dohome_hal_rtc.h"

#include <stdio.h>
#include <time.h>

#define USE_HW_TIMER		1

static int64_t g_time_offset = 0;
void dohome_hal_rtc_init(void)
{
// #if USE_HW_TIMER
// 	hal_timer_init();
// #endif
	return;

}

void dohome_hal_rtc_set_time(DOHOME_UINT32_T time) 
{
	time_t tick = (time_t)time;
	struct tm *tblock;
	tblock = localtime(&tick);
    /*tblock.tm_year = 17;
    tblock.tm_mon = 11;
    tblock.tm_mday = 20;
    tblock.tm_hour = 14;
    tblock.tm_min = 30;
    tblock.tm_sec = 0;*/
    tls_set_rtc(tblock);
}


DOHOME_UINT32_T dohome_hal_rtc_get_time(void)
{
	struct tm tblock;
	//int ticks = tls_os_get_time();
	tls_get_rtc(&tblock);
	return mktime(&tblock);
}
