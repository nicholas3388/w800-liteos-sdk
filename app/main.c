/***************************************************************************** 
* 
* File Name : main.c
* 
* Description: main 
* 
* Copyright (c) 2022 
* All rights reserved. 
* 
* Author : WangWei
* 
* Date : 2022-7-10
*****************************************************************************/ 
#include <string.h>
#include "wm_include.h"
#include "lwip/netif.h"
#include "wm_netif.h"
#include "ef_def.h"


extern void switch_template_main(void);


void UserMain(void)
{
#if DEMO_CONSOLE
	CreateDemoTask();
#else
	printf("\n <--- Use dohome library ---> \n");
	switch_template_main();
#endif

}

