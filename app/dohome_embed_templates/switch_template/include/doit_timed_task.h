#ifndef _DOIT_TIMER_H_
#define _DOIT_TIMER_H_

#include <dohome_type.h>
#include <dohome_error_code.h>
#include "doit_product_cfg.h"

#define DOIT_MAX_TIMER_LIST_NUM               CONFIG_SWITCH_NUM
#define DOIT_MAX_TIMER_LIST_ITEM_NUM          5

#define DOIT_TIMER_ONE_CLOSE        0
#define DOIT_TIMER_ONE_OPEN         1
#define DOIT_TIMER_REPEAT_CLOSE     2
#define DOIT_TIMER_REPEAT_OPEN      3

#define DOIT_MAX_COUNTDOWN_NUM      DOIT_MAX_TIMER_LIST_NUM

typedef void (*doit_timer_task_cb_t)(DOHOME_UINT8_T num, DOHOME_UINT8_T onoff);
typedef void (*doit_countdown_task_cb_t)(DOHOME_UINT8_T num);

typedef struct
{
    DOHOME_UINT8_T use;
    DOHOME_UINT8_T on;
    DOHOME_UINT8_T active;
    DOHOME_UINT8_T type;
    DOHOME_UINT32_T timestamp;
}doit_timer_t;

typedef struct
{
    doit_timer_t list[DOIT_MAX_TIMER_LIST_ITEM_NUM];
}doit_timer_task_t;


DOHOME_UINT32_T doit_get_countdown(DOHOME_UINT8_T num);
dohome_op_ret doit_set_countdown(DOHOME_UINT8_T num, DOHOME_UINT32_T second);
dohome_op_ret doit_reset_countdown(DOHOME_UINT8_T num);

DOHOME_CHAR_T *doit_get_timer_str(DOHOME_UINT8_T num);
dohome_op_ret doit_set_timer_to_str(DOHOME_UINT8_T num, DOHOME_CHAR_T *str);

dohome_op_ret doit_timer_list_save(void);
dohome_op_ret doit_timer_list_read(void);

void doit_timed_task_set_cb(doit_timer_task_cb_t cb);
void doit_countdown_task_set_cb(doit_countdown_task_cb_t cb);

void doit_reset_timed_task(void);

void doit_timed_task_loop(void);

dohome_op_ret doit_timed_task_init(void);

#endif