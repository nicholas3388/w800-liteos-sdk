#ifndef _BTN_REMOTE_FUN_H_
#define _BTN_REMOTE_FUN_H_

#include "btn_remote_define.h"
#include "flexible_button.h"
#include "doit_product_cfg.h" 

#if (defined(CONFIG_REMOTE_CONTROL_ENABLE) && CONFIG_REMOTE_CONTROL_ENABLE == 1)


#if (CONFIG_REMOTE_CONTROL_WAY == CONFIG_REMOTE_CONTROL_WAY_RF)
#if (CONFIG_REMOTE_CONTROL_MODEL == CONFIG_REMOTE_CONTROL_MODEL_RF_4_1)
void btn_remote_rf_4_1_init(void);
void btn_remote_rf_4_1_loop(void);
void btn_remote_rf_4_1_key_cb(DOHOME_UINT8_T switch_id, flex_button_event_t event, DOHOME_UINT8_T click_cnt);
#endif
#endif
#endif

//对switch的另外支持
    //对于旧的配置文件，提供默认的长按配网支持
#ifndef CONFIG_KEY_CONTROL_ENABLE
#define CONFIG_KEY_CONTROL_ENABLE 1
#define BTN_FUN_LONG_1                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#define BTN_FUN_LONG_2                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#define BTN_FUN_LONG_3                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#define BTN_FUN_LONG_4                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#endif
    //省配置BTN_CODE_1，直接使用SWITCH_KEY_PIN
#if (defined(CONFIG_KEY_PIN1) && CONFIG_KEY_PIN1 != GPIO_NOT_USE)
#define BTN_CODE_1                              CONFIG_KEY_PIN1
#endif
#if (defined(CONFIG_KEY_PIN2) && CONFIG_KEY_PIN2 != GPIO_NOT_USE)
#define BTN_CODE_2                              CONFIG_KEY_PIN2
#endif
#if (defined(CONFIG_KEY_PIN3) && CONFIG_KEY_PIN3 != GPIO_NOT_USE)
#define BTN_CODE_3                              CONFIG_KEY_PIN3
#endif
#if (defined(CONFIG_KEY_PIN4) && CONFIG_KEY_PIN4 != GPIO_NOT_USE)
#define BTN_CODE_4                              CONFIG_KEY_PIN4
#endif

#if (defined(CONFIG_KEY_CONTROL_ENABLE) && CONFIG_KEY_CONTROL_ENABLE == 1)
#if (defined(CONFIG_KEY_CONTROL_MODEL))
#if (CONFIG_KEY_CONTROL_MODEL == CONFIG_KEY_CONTROL_MODEL_1_1)
#if (defined(BTN_CODE_1))
#define BTN_FUN_LONG_1                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#else
    #error "CONFIG_KEY_CONTROL_MODEL_1_1 must be configured CONFIG_KEY1_PIN"
#endif
#elif (CONFIG_KEY_CONTROL_MODEL == CONFIG_KEY_CONTROL_MODEL_3_1)
#if (defined(BTN_CODE_1) && defined(BTN_CODE_2) && defined(BTN_CODE_3))
#define BTN_FUN_LONG_1                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#define BTN_FUN_LONG_2                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#define BTN_FUN_LONG_3                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#else
    #error "CONFIG_KEY_CONTROL_MODEL_3_1 must be configured CONFIG_KEY_CONTROL_PIN1 CONFIG_KEY_CONTROL_PIN2 CONFIG_KEY_CONTROL_PIN3"
#endif
#else
//自定义
#endif
#endif

#if (defined(CONFIG_ALL_CONTROL_ENABLE) && CONFIG_ALL_CONTROL_ENABLE == 1)
#if (CONFIG_ALL_CONTROL_KEY_PIN != GPIO_NOT_USE)
#define BTN_CODE_ALL                              CONFIG_ALL_CONTROL_KEY_PIN
#define BTN_FUN_LONG_ALL                          BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG
#endif
#endif
#endif

#if defined(BTN_CODE_1) 
#ifndef BTN_FUN_CLICK_1
#define BTN_FUN_CLICK_1
#endif
#ifndef BTN_FUN_DOUBLE_1
#define BTN_FUN_DOUBLE_1
#endif
#ifndef BTN_FUN_LONG_1
#define BTN_FUN_LONG_1
#endif
#define BTN_FUN_EXECUTION_1(event, a)  do{if(a == BTN_CODE_1){if(event == FLEX_BTN_PRESS_CLICK){BTN_FUN_CLICK_1}else if(event == FLEX_BTN_PRESS_DOUBLE_CLICK){BTN_FUN_DOUBLE_1}else if(event == FLEX_BTN_PRESS_LONG_START){BTN_FUN_LONG_1}}}while(0)
#else
#define BTN_FUN_EXECUTION_1(event, a)
#endif
#if defined(BTN_CODE_2) 
#ifndef BTN_FUN_CLICK_2
#define BTN_FUN_CLICK_2
#endif
#ifndef BTN_FUN_DOUBLE_2
#define BTN_FUN_DOUBLE_2
#endif
#ifndef BTN_FUN_LONG_2
#define BTN_FUN_LONG_2
#endif
#define BTN_FUN_EXECUTION_2(event, a)  do{if(a == BTN_CODE_2){if(event == FLEX_BTN_PRESS_CLICK){BTN_FUN_CLICK_2}else if(event == FLEX_BTN_PRESS_DOUBLE_CLICK){BTN_FUN_DOUBLE_2}else if(event == FLEX_BTN_PRESS_LONG_START){BTN_FUN_LONG_2}}}while(0)
#else
#define BTN_FUN_EXECUTION_2(event, a)
#endif
#if defined(BTN_CODE_3) 
#ifndef BTN_FUN_CLICK_3
#define BTN_FUN_CLICK_3
#endif
#ifndef BTN_FUN_DOUBLE_3
#define BTN_FUN_DOUBLE_3
#endif
#ifndef BTN_FUN_LONG_3
#define BTN_FUN_LONG_3
#endif
#define BTN_FUN_EXECUTION_3(event, a)  do{if(a == BTN_CODE_3){if(event == FLEX_BTN_PRESS_CLICK){BTN_FUN_CLICK_3}else if(event == FLEX_BTN_PRESS_DOUBLE_CLICK){BTN_FUN_DOUBLE_3}else if(event == FLEX_BTN_PRESS_LONG_START){BTN_FUN_LONG_3}}}while(0)
#else
#define BTN_FUN_EXECUTION_3(event, a)
#endif
#if defined(BTN_CODE_4) 
#ifndef BTN_FUN_CLICK_4
#define BTN_FUN_CLICK_4
#endif
#ifndef BTN_FUN_DOUBLE_4
#define BTN_FUN_DOUBLE_4
#endif
#ifndef BTN_FUN_LONG_4
#define BTN_FUN_LONG_4
#endif
#define BTN_FUN_EXECUTION_4(event, a)  do{if(a == BTN_CODE_4){if(event == FLEX_BTN_PRESS_CLICK){BTN_FUN_CLICK_4}else if(event == FLEX_BTN_PRESS_DOUBLE_CLICK){BTN_FUN_DOUBLE_4}else if(event == FLEX_BTN_PRESS_LONG_START){BTN_FUN_LONG_4}}}while(0)
#else
#define BTN_FUN_EXECUTION_4(event, a)
#endif
#if defined(BTN_CODE_ALL) 
#ifndef BTN_FUN_CLICK_ALL
#define BTN_FUN_CLICK_ALL
#endif
#ifndef BTN_FUN_DOUBLE_ALL
#define BTN_FUN_DOUBLE_ALL
#endif
#ifndef BTN_FUN_LONG_ALL
#define BTN_FUN_LONG_ALL
#endif
#define BTN_FUN_EXECUTION_ALL(event, a)  do{if(a == BTN_CODE_ALL){if(event == FLEX_BTN_PRESS_CLICK){BTN_FUN_CLICK_ALL}else if(event == FLEX_BTN_PRESS_DOUBLE_CLICK){BTN_FUN_DOUBLE_ALL}else if(event == FLEX_BTN_PRESS_LONG_START){BTN_FUN_LONG_ALL}}}while(0)
#else
#define BTN_FUN_EXECUTION_ALL(event, a)
#endif

#define BTN_FUN_EXECUTION(event, a) do{  \
    BTN_FUN_EXECUTION_1(event, a);       \
    BTN_FUN_EXECUTION_2(event, a);       \
    BTN_FUN_EXECUTION_3(event, a);       \
    BTN_FUN_EXECUTION_4(event, a);       \
    BTN_FUN_EXECUTION_ALL(event, a);     \
}while(0)

#endif