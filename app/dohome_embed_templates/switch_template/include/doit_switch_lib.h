#ifndef _DOIT_SWITCH_LIB_H_
#define _DOIT_SWITCH_LIB_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

#include "flexible_button.h"

#define DT_ALL_SWITCH_ID                    0xFC
#define DT_USB_SWITCH_ID                    0xFE

typedef DOHOME_UINT8_T                       dt_gpio_t;    

typedef enum{
    DT_SWITCH_BOOT_OFF = 0,
    DT_SWITCH_BOOT_ON = 1,
    DT_SWITCH_BOOT_MEMORY = 2,
}dt_switch_boot_state_e;

typedef enum{
    DT_SWITCH_LED_NONE = 0,
    DT_SWITCH_LED_RELAY = 1,
    DT_SWITCH_LED_POS = 2,
}dt_switch_led_state_e;

typedef struct{
    DOHOME_UINT8_T onoff;
}dt_switch_power_t;


typedef struct{
    dt_gpio_t    name;
    DOHOME_UINT8_T active_level;
}dt_gpio_cfg_t;

typedef struct{
    dt_gpio_cfg_t power_gpio;
    dt_gpio_cfg_t state_gpio;
    dt_gpio_cfg_t key_gpio;
}dt_switch_io_cfg_t;

dt_switch_boot_state_e doit_switch_get_boot_state(void);
dohome_op_ret doit_switch_set_boot_state(dt_switch_boot_state_e boot_state);

dt_switch_led_state_e doit_switch_get_led_state(void);
dohome_op_ret doit_switch_set_led_state(dt_switch_led_state_e led_state);

dohome_op_ret doit_switch_get_all_onoff(dt_switch_power_t *state);
dohome_op_ret doit_switch_set_all_onoff(DOHOME_UINT8_T onoff);
DOHOME_UINT8_T doit_switch_get_onoff(DOHOME_UINT8_T num);
dohome_op_ret doit_switch_set_onoff(DOHOME_UINT8_T num, DOHOME_UINT8_T onoff);

dohome_op_ret doit_switch_reverse(DOHOME_UINT8_T num);

void doit_switch_wifi_led_start_flash(void);
void doit_switch_wifi_led_stop_flash(void);
void doit_switch_wifi_led_flash_loop(void);
DOHOME_UINT8_T doit_switch_wifi_led_is_flash(void);
void doit_switch_wifi_led_set_status(DOHOME_UINT8_T status);

void doit_switch_button_scan(void);

void doit_switch_set_boot_onoff(dt_switch_power_t *list, dt_switch_boot_state_e boot_state);

dohome_op_ret doit_switch_btn_id_to_switch_id(DOHOME_UINT8_T btn_id, DOHOME_UINT8_T *switch_id);

void doit_switch_init(flex_button_response_callback keys_event_cb);


#endif