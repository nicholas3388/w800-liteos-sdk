#ifndef _DOIT_PRODUCT_H_
#define _DOIT_PRODUCT_H_

#include <dohome_type.h>
#include <dohome_dp_type.h>
#include <dohome_error_code.h>

#include "doit_product_cfg.h"
#include "dohome_cfg.h"

#include "doit_switch_lib.h"

#define PRODUCT_SWITCH_ONOFF               1
#define PRODUCT_SWITCH1_COUNTDOWN          2
#define PRODUCT_SWITCH1_NORMAL_TIME        3
#define PRODUCT_SWITCH2_COUNTDOWN          4
#define PRODUCT_SWITCH2_NORMAL_TIME        5
#define PRODUCT_SWITCH3_COUNTDOWN          6
#define PRODUCT_SWITCH3_NORMAL_TIME        7
#define PRODUCT_SWITCH4_COUNTDOWN          8
#define PRODUCT_SWITCH4_NORMAL_TIME        9
#define PRODUCT_SWITCH5_COUNTDOWN          10
#define PRODUCT_SWITCH5_NORMAL_TIME        11
#define PRODUCT_SWITCH6_COUNTDOWN          12
#define PRODUCT_SWITCH6_NORMAL_TIME        13
#define PRODUCT_SWITCH7_COUNTDOWN          14
#define PRODUCT_SWITCH7_NORMAL_TIME        15
#define PRODUCT_SWITCH8_COUNTDOWN          16
#define PRODUCT_SWITCH8_NORMAL_TIME        17
#define PRODUCT_RELAY_STATUS               18
#define PRODUCT_LED_STATUS                 19
#define PRODUCT_BACKLIGHT_STATUS           20
#define PRODUCT_CHILD_LOCK                 21
#define PRODUCT_NORMAL_TIME                22
#define PRODUCT_CYCLE_TIME                 23
#define PRODUCT_RANDOM_TIME                24
#define PRODUCT_SWITCH_INCHING             25
#define PRODUCT_ADD_ELE                    26
#define PRODUCT_CUR_CURRENT                27
#define PRODUCT_CUR_POWER                  28
#define PRODUCT_CUR_VOLTAGE                29
#define PRODUCT_FAULT                      30
#define PRODUCT_WAKE_ON_LAN                31
#define PRODUCT_OVER_CURRENT_PROTECTION    32


typedef struct{
    dt_switch_boot_state_e boot_state;
    dt_switch_power_t switch_list[CONFIG_SWITCH_NUM];
    dt_switch_led_state_e led_state;
#if (defined(CONFIG_POWER_MEASURE_ENABLE) && CONFIG_POWER_MEASURE_ENABLE == 1)
    DOHOME_UINT8_T overcurrent_en;
    DOHOME_UINT32_T energy_timestamp;
    DOHOME_UINT32_T energy_compensate;
#endif
}doit_product_status_t;

dohome_op_ret doit_product_save_status(void);
dohome_op_ret doit_product_read_status(doit_product_status_t *status);

dohome_op_ret doit_product_set_dps(dh_obj_dps_t *dps, DOHOME_UINT8_T *cancel_resp);
#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))
void doit_product_wifi_connect_status(void);
void doit_product_wifi_disconnect_status(void);
#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
void doit_product_ble_connect_status(void);
void doit_product_ble_disconnect_status(void);
#endif

void doit_product_stop_config_status(void);
void doit_product_start_config_status(void);

void doit_product_default_boot_status(void);

void doit_product_task_loop(void);

void doit_product_register_cb(void);

void doit_product_init(void);


#define _SWITCH_IO(io)   {.power_gpio.name = CONFIG_RELAY_PIN##io, .power_gpio.active_level = CONFIG_GPIO_POWER_ACTIVE_LEVEL,          \
                        .state_gpio.name = CONFIG_POWER_STATE_PIN##io, .state_gpio.active_level = CONFIG_GPIO_STATE_ACTIVE_LEVEL,     \
                        .key_gpio.name = CONFIG_KEY_PIN##io, .key_gpio.active_level = CONFIG_GPIO_KEY_ACTIVE_LEVEL},

#define _SWITCH_COUNTDOWN_DPID(num)   ,PRODUCT_SWITCH##num##_COUNTDOWN



#ifndef CONFIG_SWITCH_NUM
    #error check you cfg file
#endif

#if (CONFIG_SWITCH_NUM > 8)    
    #error check you cfg file
#endif

#if (CONFIG_SWITCH_NUM >= 1)
#if (defined(CONFIG_RELAY_PIN1) && defined(CONFIG_KEY_PIN1) && defined(CONFIG_POWER_STATE_PIN1)) 
    #define SWITCH_IO_1 _SWITCH_IO(1)
    #define COUNTDOWN_DPID_1 _SWITCH_COUNTDOWN_DPID(1)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM >= 2)
#if (defined(CONFIG_RELAY_PIN2) && defined(CONFIG_KEY_PIN2) && defined(CONFIG_POWER_STATE_PIN2)) 
    #define SWITCH_IO_2 _SWITCH_IO(2)
    #define COUNTDOWN_DPID_2 _SWITCH_COUNTDOWN_DPID(2)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM >= 3)
#if (defined(CONFIG_RELAY_PIN3) && defined(CONFIG_KEY_PIN3) && defined(CONFIG_POWER_STATE_PIN3)) 
    #define SWITCH_IO_3 _SWITCH_IO(3)
    #define COUNTDOWN_DPID_3 _SWITCH_COUNTDOWN_DPID(3)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM >= 4)
#if (defined(CONFIG_RELAY_PIN4) && defined(CONFIG_KEY_PIN4) && defined(CONFIG_POWER_STATE_PIN4)) 
    #define SWITCH_IO_4 _SWITCH_IO(4)
    #define COUNTDOWN_DPID_4 _SWITCH_COUNTDOWN_DPID(4)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM >= 5)
#if (defined(CONFIG_RELAY_PIN5) && defined(CONFIG_KEY_PIN5) && defined(CONFIG_POWER_STATE_PIN5)) 
    #define SWITCH_IO_5 _SWITCH_IO(5)
    #define COUNTDOWN_DPID_5 _SWITCH_COUNTDOWN_DPID(5)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM >= 6)
#if (defined(CONFIG_RELAY_PIN6) && defined(CONFIG_KEY_PIN6) && defined(CONFIG_POWER_STATE_PIN6)) 
    #define SWITCH_IO_6 _SWITCH_IO(6)
    #define COUNTDOWN_DPID_6 _SWITCH_COUNTDOWN_DPID(6)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM >= 7)
#if (defined(CONFIG_RELAY_PIN7) && defined(CONFIG_KEY_PIN7) && defined(CONFIG_POWER_STATE_PIN7)) 
    #define SWITCH_IO_7 _SWITCH_IO(7)
    #define COUNTDOWN_DPID_7 _SWITCH_COUNTDOWN_DPID(7)
#else
    #error check you cfg file
#endif
#endif
#if (CONFIG_SWITCH_NUM == 8)
#if (defined(CONFIG_RELAY_PIN8) && defined(CONFIG_KEY_PIN8) && defined(CONFIG_POWER_STATE_PIN8)) 
    #define SWITCH_IO_8 _SWITCH_IO(8)
    #define COUNTDOWN_DPID_8 _SWITCH_COUNTDOWN_DPID(8)
#else
    #error check you cfg file
#endif
#endif

#if (!defined(SWITCH_IO_1) && !defined(COUNTDOWN_DPID_1))
    #define SWITCH_IO_1
    #define COUNTDOWN_DPID_1
#endif
#if (!defined(SWITCH_IO_2) && !defined(COUNTDOWN_DPID_2))
    #define SWITCH_IO_2
    #define COUNTDOWN_DPID_2
#endif
#if (!defined(SWITCH_IO_3) && !defined(COUNTDOWN_DPID_3))
    #define SWITCH_IO_3
    #define COUNTDOWN_DPID_3
#endif
#if (!defined(SWITCH_IO_4) && !defined(COUNTDOWN_DPID_4))
    #define SWITCH_IO_4
    #define COUNTDOWN_DPID_4
#endif
#if (!defined(SWITCH_IO_5) && !defined(COUNTDOWN_DPID_5))
    #define SWITCH_IO_5
    #define COUNTDOWN_DPID_5
#endif
#if (!defined(SWITCH_IO_6) && !defined(COUNTDOWN_DPID_6))
    #define SWITCH_IO_6
    #define COUNTDOWN_DPID_6
#endif
#if (!defined(SWITCH_IO_7) && !defined(COUNTDOWN_DPID_7))
    #define SWITCH_IO_7
    #define COUNTDOWN_DPID_7
#endif
#if (!defined(SWITCH_IO_8) && !defined(COUNTDOWN_DPID_8))
    #define SWITCH_IO_8
    #define COUNTDOWN_DPID_8
#endif

#endif