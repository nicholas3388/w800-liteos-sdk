#ifndef _DOIT_UPLOAD_H_
#define _DOIT_UPLOAD_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

typedef void (*doit_upload_sync_cb_t)(void);
void doit_upload_register_third_party_sync_cb(doit_upload_sync_cb_t sync_cb);

void doit_update_staus_now(void);
void doit_update_staus_delay(void);

void doit_upload_cmd_now(DOHOME_UINT8_T cmd);

void doit_upload_task_loop(void);

#endif