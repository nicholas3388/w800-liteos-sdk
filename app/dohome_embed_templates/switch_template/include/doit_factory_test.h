#ifndef _DOIT_WIFI_FACTORY_H_
#define _DOIT_WIFI_FACTORY_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

#include "dohome_hal_wifi.h"

typedef void (*doit_test_scan_cb_t)(DOHOME_UINT8_T enable);

DOHOME_UINT8_T doit_factory_has_scan(void);
void doit_factory_stop(void);

void doit_factory_test_scan(DOHOME_UINT8_T scan_num, doit_test_scan_cb_t test_scan_cb);

void doit_factory_test_loop(void);

void doit_factory_test_init(DOHOME_UINT8_T scan_num, doit_test_scan_cb_t test_scan_cb);

void doit_factory_register_third_party_test(DOHOME_CHAR_T *ssid, doit_test_scan_cb_t test_scan_cb);

#endif