#ifndef _BTN_REMOTE_DEFINE_H_
#define _BTN_REMOTE_DEFINE_H_

#include <dohome_type.h>
#include <dohome_error_code.h>

dohome_op_ret btn_remote_fun_start_wifi_config(void);

#define BTN_REMOTE_FUN_NULL

#define BTN_REMOTE_FUN_LIGHT_START_WIFI_CONFIG     btn_remote_fun_start_wifi_config();       

#define CONFIG_REMOTE_CONTROL_WAY_RF                 1

#define CONFIG_REMOTE_CONTROL_MODEL_RF_4_1           1 //CL00332

#define CONFIG_KEY_CONTROL_MODEL_1_1                 1
#define CONFIG_KEY_CONTROL_MODEL_3_1                 2

#endif
