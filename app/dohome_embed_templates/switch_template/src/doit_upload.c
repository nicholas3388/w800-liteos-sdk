#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_DEBUG
#define DOHOME_LOG_TAG          "doit_upload"

#include "dohome_log.h"
#include "doit_upload.h"

#include "dohome_api.h"

#include "doit_product.h"

#define NOT_UPDTAE_TICK        0xFFFFFFFF

DOHOME_STATIC DOHOME_UINT32_T curr_tick = NOT_UPDTAE_TICK;

DOHOME_STATIC DOHOME_UINT32_T cmd_tick = NOT_UPDTAE_TICK;
DOHOME_STATIC DOHOME_UINT32_T now_save_tick = NOT_UPDTAE_TICK;
DOHOME_STATIC DOHOME_UINT32_T delay_save_tick = NOT_UPDTAE_TICK;
DOHOME_STATIC DOHOME_UINT32_T now_status_tick = NOT_UPDTAE_TICK;
DOHOME_STATIC DOHOME_UINT32_T delay_status_tick = NOT_UPDTAE_TICK;

DOHOME_STATIC DOHOME_UINT8_T update_cmd = 0;

void doit_update_staus_now(void){

#ifdef CONFIG_UPDATA_STATUS_NOW_TICK 
    now_status_tick = curr_tick + CONFIG_UPDATA_STATUS_NOW_TICK;
#else
    now_status_tick = curr_tick + 2;
#endif

#ifdef CONFIG_SAVE_STATUS_NOW_TICK 
    now_save_tick = curr_tick + CONFIG_SAVE_STATUS_DELAY_TICK;
#else
    now_save_tick = curr_tick + 4;
#endif
}

void doit_update_staus_delay(void){

#ifdef CONFIG_UPDATA_STATUS_DELAY_TICK 
    delay_status_tick = curr_tick + CONFIG_UPDATA_STATUS_DELAY_TICK;
#else
    delay_status_tick = curr_tick + 10;
#endif

#ifdef CONFIG_SAVE_STATUS_DELAY_TICK 
    delay_save_tick = curr_tick + CONFIG_SAVE_STATUS_NOW_TICK;
#else
    delay_save_tick = curr_tick + 12;
#endif
}


void doit_upload_cmd_now(DOHOME_UINT8_T cmd){
#ifdef CONFIG_UPDATA_STATUS_NOW_TICK 
    cmd_tick = curr_tick + CONFIG_UPDATA_STATUS_NOW_TICK;
#else
    cmd_tick = curr_tick + 2;
#endif
    update_cmd = cmd;
}

doit_upload_sync_cb_t third_party_sync_cb = NULL;

void doit_upload_register_third_party_sync_cb(doit_upload_sync_cb_t sync_cb){
    third_party_sync_cb = sync_cb;
}

void doit_upload_task_loop(void)
{
    if(cmd_tick != NOT_UPDTAE_TICK){
        if(curr_tick >= cmd_tick){
            cmd_tick = NOT_UPDTAE_TICK;
            DOHOME_UINT8_T dpid_list[1] = {0};
            dpid_list[0] = update_cmd;
            dohome_cloud_post_dps(dpid_list, 1);
        }
    }else if(now_status_tick != NOT_UPDTAE_TICK || delay_status_tick != NOT_UPDTAE_TICK){
        DOHOME_UINT8_T post = 0;
        if(curr_tick >= now_status_tick){
            post = 1;
            now_status_tick = NOT_UPDTAE_TICK;
        }else if(curr_tick >= delay_status_tick){
            post = 1;
            delay_status_tick = NOT_UPDTAE_TICK;
        }
        if(post){
            DOHOME_LOG_I("post_all_dps");
            dohome_cloud_post_all_dps();
            if(third_party_sync_cb){
                third_party_sync_cb();
            }
        }
    }else if(now_save_tick != NOT_UPDTAE_TICK || delay_save_tick != NOT_UPDTAE_TICK){
        DOHOME_UINT8_T save = 0;
        if(curr_tick >= now_save_tick){
            save = 1;
            now_save_tick = NOT_UPDTAE_TICK;
        }
        if(curr_tick >= delay_save_tick){
            save = 1;
            delay_save_tick = NOT_UPDTAE_TICK;
        }
        if(save){
            DOHOME_LOG_I("save_status");
            doit_product_save_status();
        }
    }
    curr_tick++;
}