
#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_DEBUG
#define DOHOME_LOG_TAG          "dohome_main"

#include <dohome_api.h>
#include <dohome_log.h>

#include "dohome_cfg.h"
#include "dohome_hal_wifi.h"
#include "dohome_hal_timer.h"
#include "dohome_hal_system.h"

#include "doit_upload.h"
#include "doit_product.h"

#include "doit_timed_task.h"
#include "doit_factory_test.h"


DOHOME_STATIC DOHOME_UINT16_T dev_res_cnt = 0;
DOHOME_STATIC DOHOME_UINT8_T in_test_mode = 0;
DOHOME_STATIC DOHOME_UINT8_T timer_id = 0;


#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE))
#define dohome_hal_ticktack_cb
#define dohome_hal_wifi_set_sleep_flag(a)   dohome_hal_sys_init()
#define dohome_wifi_has_config              dohome_ble_has_config
#define dohome_wifi_has_boot_start_config   dohome_ble_has_boot_start_config
#define dohome_wifi_start_config            dohome_ble_adv_start_config
#define dohome_wifi_start_connect           dohome_ble_adv_stop_config
#endif



#if CONFIG_USE_POWER_SWITCH_CONFIG
void doit_res_reset_cnt(void){
    DOHOME_LOG_I("res_reset_cnt");

    DOHOME_UINT16_T cnt = 0;
    dohome_kvs_set_env_blob("dt_reset_cnt", &cnt, sizeof(int));
}

DOHOME_STATIC void doit_res_cnt_init(void){

    DOHOME_SIZE_T len = 0;
    DOHOME_UINT16_T cnt = 0;;

    dohome_kvs_get_env_blob("dt_reset_cnt", &cnt, sizeof(cnt), &len);
    if(len <= 0){
        dev_res_cnt = 1;
        dohome_kvs_set_env_blob("dt_reset_cnt", &dev_res_cnt, sizeof(cnt));
    }else{
        dev_res_cnt = cnt+1;
        dohome_kvs_set_env_blob("dt_reset_cnt", &dev_res_cnt, sizeof(cnt));
    }
	
    DOHOME_LOG_I("doit_reset_cnt: %d", dev_res_cnt);
}

void doit_set_reset_cnt(DOHOME_UINT16_T cnt){

    DOHOME_LOG_I("save reset_cnt: %d", cnt);
    dohome_kvs_set_env_blob("dt_reset_cnt", &cnt, sizeof(cnt));
}

DOHOME_STATIC DOHOME_UINT16_T doit_get_reset_cnt(void){
    return dev_res_cnt;
}
#endif

DOHOME_STATIC void doit_factory_test_scan_cb(DOHOME_UINT8_T enable){
    DOHOME_LOG_I("factory_test_scan_cb: %d", enable);
    if(enable){
        DOHOME_LOG_I("start factory test");
        in_test_mode = 1;
        dohome_hal_timer_stop(timer_id);
    }else{
        in_test_mode = 0;
        DOHOME_LOG_I("start wifi config");
#if CONFIG_DEFAULT_WIFI_CONFIG_MODE
        dohome_wifi_start_config();
#endif
    }
    // must be return
}


DOHOME_STATIC DOHOME_UINT8_T os_main_new_run = 0;
void doit_timer_main_loop(void)
{
    DOHOME_STATIC DOHOME_UINT64_T loop_tick_ms = 0;

    dohome_lib_timer_loop();

	loop_tick_ms += DOIT_LOOP_PERIOD;
    loop_tick_ms = (loop_tick_ms)%(3600000);
}

#define OS_LOOP_SECOND_CNT              (1000/DOIT_LOOP_PERIOD)
void doit_os_main_loop(void)
{
    DOHOME_STATIC DOHOME_UINT64_T loop_tick_ms = 0;

    if(in_test_mode == 0){

        dohome_lib_os_loop();

        doit_product_task_loop();

        if(loop_tick_ms%500 == 0){
            doit_timed_task_loop();
        }

        if(loop_tick_ms%200 == 0){
            doit_upload_task_loop();
        }

#if DOHOME_LIB_MEM_POOL_NAME != DOHOME_LIB_MEM_NOT_USE
        DOHOME_STATIC DOHOME_UINT32_T tick = 0;
        DOHOME_STATIC DOHOME_UINT32_T last_mem_size = 0;
        DOHOME_UINT32_T mem_size = 0;
        if(tick++ % 10 == 0){
            mem_size = dohome_tlsfmem_get_used_size();
            if(last_mem_size != mem_size || tick > OS_LOOP_SECOND_CNT*10){
                tick = 0;
                last_mem_size = mem_size;
                DOHOME_LOG_I("doit_mem_curr_used_size: %d", mem_size);
            }
        }
#endif
    }

    //执行一次
#if CONFIG_USE_POWER_SWITCH_CONFIG
    DOHOME_STATIC DOHOME_UINT16_T os_boot_tick_ms = 0;
    if(os_boot_tick_ms < 5000){
        os_boot_tick_ms += DOIT_LOOP_PERIOD;
        if(os_boot_tick_ms >= 5000){
            doit_res_reset_cnt();
        }
    }
#endif

    dohome_hal_ticktack_cb(OS_LOOP_SECOND_CNT);

#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))        
    doit_factory_test_loop();
#endif

    loop_tick_ms += DOIT_LOOP_PERIOD;
    loop_tick_ms = (loop_tick_ms)%(3600000);
}

void cozylife_app_info(void){
    dohome_printf("cozylife_app_info\r\n");
    dohome_printf("Product Id: %.*s\r\n", dohome_safe_strlen(DT_PRODUCT_ID,7), DT_PRODUCT_ID);
    dohome_printf("Firmware Version: %.*s\r\n", dohome_safe_strlen(DT_SOFTWARE_VERSION,15), DT_SOFTWARE_VERSION);
    dohome_printf("Firmware Build %.*s %.*s\r\n\n", sizeof(__DATE__), __DATE__, sizeof(__TIME__), __TIME__);
}

void switch_template_main(){

    DOHOME_UINT16_T reset_cnt = 0;
    DOHOME_UINT16_T need_config = 0;
    DOHOME_UINT8_T dev_is_config = 0;
    dohome_op_ret ret = OPRT_COM_ERROR;

    cozylife_sdk_info();
    cozylife_app_info();

#if((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))
#ifdef CONFIG_WIFI_SLEEP
    dohome_hal_wifi_set_sleep_flag(CONFIG_WIFI_SLEEP);
#endif
#endif

    dohome_lib_set_product_version(DT_SOFTWARE_VERSION, DT_HARDWARE_VERSION);
    dohome_lib_set_loop_period(DOIT_LOOP_PERIOD);

    doit_product_register_cb();

    dohome_lib_main(DT_PRODUCT_ID);

    ret = dohome_hal_timer_init(&timer_id, DOIT_LOOP_PERIOD, 1, doit_timer_main_loop);
    if(ret == OPRT_OK){
        dohome_hal_timer_start(timer_id);
    }

    doit_product_init();
 
    dev_is_config = dohome_wifi_has_config();
    need_config = dohome_wifi_has_boot_start_config();
#if CONFIG_USE_POWER_SWITCH_CONFIG
    doit_res_cnt_init();
    reset_cnt = doit_get_reset_cnt();
#endif

    if(dev_is_config){       
        if(need_config == 1){
            DOHOME_LOG_I("need_config --> start wifi config");
            dohome_wifi_start_config();
#if CONFIG_USE_POWER_SWITCH_CONFIG
        }else if(reset_cnt >= CONFIG_POWER_SWITCH_COUNT){
            DOHOME_LOG_I("reset_cnt > 3 --> start wifi config");
            dohome_wifi_start_config();
#endif 
        }else{
            DOHOME_LOG_I("wifi is config --> start wifi connect");
            dohome_wifi_start_connect();
            doit_product_default_boot_status();
        }
    }else{
#if ((DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFI) || (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_WIFIBLE))        
#if CONFIG_USE_POWER_SWITCH_CONFIG
        if(reset_cnt >= CONFIG_POWER_SWITCH_COUNT){
            DOHOME_LOG_I("reset_cnt > 3 --> start wifi config");
            doit_factory_test_init(1, doit_factory_test_scan_cb);
            doit_product_start_config_status();
        }else
#endif
        if(need_config == 1){
            DOHOME_LOG_I("need_config --> start wifi config");
            dohome_wifi_start_config();
        }else{
            doit_factory_test_init(2, doit_factory_test_scan_cb);
#if CONFIG_DEFAULT_WIFI_CONFIG_MODE
            doit_product_start_config_status();
#else
            doit_product_default_boot_status();
#endif
        }
#elif (DOHOME_PLATFORM_TYPE == DOHOME_PLATFORM_BLE)
        DOHOME_LOG_I("dev is not config --> start dev ble adv");
        dohome_ble_adv_start_config();
#endif
    }

    while(1){
        doit_os_main_loop();
        dohome_xdelay_ms(DOIT_LOOP_PERIOD);
    }

}