#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_INFO
#define DOHOME_LOG_TAG          "btn_ir"

#include "btn_remote_fun.h"
#include "dohome_log.h"

#include "dohome_api.h"
#include "doit_product.h"
#include "doit_timed_task.h"

dohome_op_ret btn_remote_fun_start_wifi_config(void){
    DOHOME_LOG_I("btn_remote_fun_start_wifi_config");
    dohome_wifi_start_config_and_reboot();
    return OPRT_OK;
}

#if (defined(CONFIG_REMOTE_CONTROL_ENABLE) && CONFIG_REMOTE_CONTROL_ENABLE == 1)

#if (CONFIG_REMOTE_CONTROL_MODEL == CONFIG_REMOTE_CONTROL_MODEL_RF_4_1)

#include "doit_upload.h"
#include "doit_switch_lib.h"
#include "dohome_hal_gpio.h"
#include "dohome_hal_remote.h"

#define MAX_REMOTE_NUM              15

typedef struct {
    DOHOME_UINT16_T id;
    DOHOME_UINT32_T pair_code;
}pair_code_t;

DOHOME_STATIC pair_code_t pair_code_list[CONFIG_SWITCH_NUM][MAX_REMOTE_NUM] = {0};

dohome_op_ret cls_pair_code(DOHOME_UINT8_T switch_id){
    DOHOME_LOG_I("switch: %d   cls_pair_code", switch_id);

    DOHOME_UINT16_T i = 0, j = 0;
    
    for(j = 0; j < MAX_REMOTE_NUM; j++){
        pair_code_list[switch_id][j].id = 0;
    }

    dohome_kvs_set_env_blob("remote_pair", pair_code_list, sizeof(pair_code_list));
    return OPRT_OK;
}

dohome_op_ret set_pair_code(DOHOME_UINT8_T switch_id, DOHOME_UINT32_T pair_code){
    DOHOME_LOG_I("switch: %d   set_pair_code: 0x%08lx", switch_id, pair_code);

    DOHOME_UINT16_T i = 0, j = 0;
    DOHOME_UINT8_T code_same = 0;
    DOHOME_UINT8_T code_same_index = 0;
    DOHOME_UINT32_T min_id = -1, max_id = 0;


    min_id = pair_code_list[switch_id][0].id;
    max_id = pair_code_list[switch_id][0].id;

    for(j = 0; j < MAX_REMOTE_NUM; j++){
        min_id = pair_code_list[switch_id][j].id<min_id?pair_code_list[switch_id][j].id:min_id;
        max_id = pair_code_list[switch_id][j].id>max_id?pair_code_list[switch_id][j].id:max_id;
        if(pair_code_list[switch_id][j].pair_code == pair_code){
            code_same = 1;
            code_same_index = j;
        }
    }

    if(min_id == max_id){
        max_id = 0; 
    }

    if(code_same){
        pair_code_list[switch_id][code_same_index].id = max_id+1;
    }else{
        for (i = 0; i < MAX_REMOTE_NUM; i++)
        {
            if(pair_code_list[switch_id][i].id == min_id){
                pair_code_list[switch_id][i].id = max_id+1;
                pair_code_list[switch_id][i].pair_code = pair_code;
                break;
            }
        }
    }
    
    dohome_kvs_set_env_blob("remote_pair", pair_code_list, sizeof(pair_code_list));
    return OPRT_OK;
}

dohome_op_ret read_pair_code(){
    DOHOME_UINT16_T i = 0, j = 0;
    DOHOME_UINT16_T len = 0;
    len = dohome_kvs_get_env_blob("remote_pair", pair_code_list, sizeof(pair_code_list), NULL);
    if(len != sizeof(pair_code_list)){
        for(i = 0; i < CONFIG_SWITCH_NUM; i++){
            for(j = 0; j < MAX_REMOTE_NUM; j++){
                pair_code_list[i][j].id = 0;
            }
        }
    }
    return OPRT_OK;
}

void set_rf_led(DOHOME_UINT8_T level){

    if(level){
        dohome_hal_gpio_write(CONFIG_REMOTE_CONTROL_LED_PIN, CONFIG_REMOTE_CONTROL_LED_ACTIVE_LEVEL);
    }else{
        dohome_hal_gpio_write(CONFIG_REMOTE_CONTROL_LED_PIN, CONFIG_REMOTE_CONTROL_LED_ACTIVE_LEVEL?0:1);
    }
}

DOHOME_UINT8_T pair_switch = 0xFF;
DOHOME_UINT8_T pair_led_flash = 0;
void remote_btn_cb(remote_ctrl_event_t event, DOHOME_UINT16_T remote_addr, DOHOME_UINT16_T remote_code, DOHOME_INT16_T repeat){
    DOHOME_UINT16_T i = 0, j = 0;

    DOHOME_LOG_I("remote_btn_cb event: %d, remote_addr: %04x, remote_code: %04x, repeat: %d", event, remote_addr, remote_code, repeat);
    if(event == REMOTE_CTRL_PRESS_CLICK){
        if(pair_switch != 0xFF){
            set_rf_led(0);
            set_pair_code(pair_switch, (remote_addr<<16 | remote_code));
        
            for (i = 0; i < MAX_REMOTE_NUM; i++)
            {
                printf("%d   id: %d  code: %08lx\n", i, pair_code_list[pair_switch][i].id, pair_code_list[pair_switch][i].pair_code);
            }
            pair_switch = 0xFF;
        }else{
            DOHOME_UINT8_T update_staus = 0;
            DOHOME_UINT8_T switch_reverse[CONFIG_SWITCH_NUM] = {0};
            DOHOME_UINT32_T pair_code = (remote_addr<<16 | remote_code);

            for(i = 0; i < CONFIG_SWITCH_NUM; i++){
                switch_reverse[i] = 0;
            }
            for(i = 0; i < CONFIG_SWITCH_NUM; i++){
                for(j = 0; j < MAX_REMOTE_NUM; j++){
                    if(pair_code_list[i][j].id != 0 && pair_code_list[i][j].pair_code == pair_code){
                        switch_reverse[i] = 1;
                    }
                }
            }

            for(i = 0; i < CONFIG_SWITCH_NUM; i++){
                if(switch_reverse[i]){
                    update_staus = 1;
                    doit_switch_reverse(i);
                }
            }

            if(update_staus){
                doit_update_staus_now();
            }
        }
    }
}

void btn_remote_rf_4_1_key_cb(DOHOME_UINT8_T switch_id, flex_button_event_t event, DOHOME_UINT8_T click_cnt){
    DOHOME_LOG_I("rf_4_1_key_cb id: [%d]  event: [%d]  repeat: %d", switch_id, event, click_cnt);

    if(event == FLEX_BTN_PRESS_DOUBLE_CLICK){
        set_rf_led(1);
        pair_switch = switch_id;
    }else if(7 <= click_cnt && click_cnt <= 9){
        pair_led_flash = 1;
        cls_pair_code(switch_id);
    }   
}

void btn_remote_rf_4_1_loop(){
    DOHOME_STATIC DOHOME_UINT8_T pair_tick = 0;
    DOHOME_STATIC DOHOME_UINT8_T flash_tick = 0;
    DOHOME_STATIC DOHOME_UINT8_T flash_level = 0;
    if(pair_switch != 0xFF){
        pair_tick++;

        if(pair_tick >= 50){
            set_rf_led(0);
            pair_switch = 0xFF;
        }
    }else{
        pair_tick = 0;
    }

    if(pair_led_flash){
        flash_tick++;
        flash_level = (flash_level+1)%2;
        set_rf_led(flash_level);
        if(flash_tick > 8*2){
            flash_tick = 0;
            pair_led_flash = 0;
            set_rf_led(0);
        }
    }

}

void btn_remote_rf_4_1_init(void){
    dohome_rfd_init(CONFIG_REMOTE_CONTROL_PIN, remote_btn_cb);

    dohome_hal_gpio_init(CONFIG_REMOTE_CONTROL_LED_PIN, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
    set_rf_led(0);
}
#endif
#endif