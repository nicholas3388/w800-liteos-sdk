
#define DOHOME_LOG_LVL          DOHOME_LOG_LVL_DEBUG
#define DOHOME_LOG_TAG          "doit_switch"

#include "doit_switch_lib.h"

#include <dohome_log.h>
#include <dohome_hal_gpio.h>

#include "doit_product.h"
#include "doit_default_cfg.h"

dt_switch_io_cfg_t switch_cfg[CONFIG_SWITCH_NUM] = {
SWITCH_IO_1
SWITCH_IO_2
SWITCH_IO_3
SWITCH_IO_4
SWITCH_IO_5
SWITCH_IO_6
SWITCH_IO_7
SWITCH_IO_8
};


DOHOME_STATIC DOHOME_UINT8_T wifi_led_flash = 0;
DOHOME_STATIC DOHOME_UINT8_T wifi_led_is_realy = 0;
DOHOME_STATIC DOHOME_UINT8_T wifi_led_status = 0;
DOHOME_STATIC flex_button_response_callback keys_event_cb = NULL;
DOHOME_STATIC dt_switch_power_t switch_state[CONFIG_SWITCH_NUM] = {0};
DOHOME_STATIC dt_switch_boot_state_e switch_boot_state = DT_SWITCH_BOOT_OFF;
DOHOME_STATIC dt_switch_led_state_e switch_led_state = DT_SWITCH_LED_RELAY;

DOHOME_STATIC flex_button_t user_button[CONFIG_SWITCH_NUM];

#if (defined(CONFIG_USE_ONOFF_PLATE_CONFIG) && CONFIG_USE_ONOFF_PLATE_CONFIG == 1)
DOHOME_STATIC DOHOME_UINT32_T onoff_plate_time = 0;
DOHOME_STATIC DOHOME_UINT8_T onoff_plate_count = 0;
#endif

dohome_op_ret doit_switch_set_boot_state(dt_switch_boot_state_e boot_state){

    switch_boot_state = boot_state;
    return OPRT_OK;
}

dt_switch_boot_state_e doit_switch_get_boot_state(void){
    return switch_boot_state;
}

dt_switch_led_state_e doit_switch_get_led_state(void){
    return switch_led_state;
}

dohome_op_ret doit_switch_set_led_state(dt_switch_led_state_e led_state){

    DOHOME_UINT8_T num = 0;

    if(switch_led_state == led_state){
        return OPRT_OK;
    }

    switch_led_state = led_state;

    DOHOME_LOG_E("led_state: %d", switch_led_state);

    if(CONFIG_WIFI_STATE_PIN != GPIO_NOT_USE && wifi_led_is_realy == 0){
        if(led_state == DT_SWITCH_LED_NONE){
            dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, CONFIG_GPIO_STATE_ACTIVE_LEVEL?0:1);
        }else if(led_state == DT_SWITCH_LED_RELAY){
            doit_switch_wifi_led_set_status(wifi_led_status);
        }else if(led_state == DT_SWITCH_LED_POS){
            dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, CONFIG_GPIO_STATE_ACTIVE_LEVEL?1:0);
        }
    }else{
        for (num = 0; num < CONFIG_SWITCH_NUM; num++)
        {
            if(switch_cfg[num].state_gpio.name != GPIO_NOT_USE){
                if(led_state == DT_SWITCH_LED_NONE){
                    dohome_hal_gpio_write(switch_cfg[num].state_gpio.name, switch_cfg[num].state_gpio.active_level?0:1);
                }else if(led_state == DT_SWITCH_LED_RELAY){
                    if(switch_cfg[num].state_gpio.active_level){
                        dohome_hal_gpio_write(switch_cfg[num].state_gpio.name, switch_state[num].onoff?1:0);
                    }else{
                        dohome_hal_gpio_write(switch_cfg[num].state_gpio.name, switch_state[num].onoff?0:1);
                    }
                }else if(led_state == DT_SWITCH_LED_POS){
                    dohome_hal_gpio_write(switch_cfg[num].state_gpio.name, switch_cfg[num].state_gpio.active_level?1:0);
                }
            }
        }
    }
    
    return OPRT_OK;
}

dohome_op_ret doit_switch_get_all_onoff(dt_switch_power_t *state){

    memcpy(state, &switch_state, sizeof(switch_state));
    return OPRT_OK;
}
dohome_op_ret doit_switch_set_all_onoff(DOHOME_UINT8_T onoff){

    for (DOHOME_UINT8_T i = 0; i < CONFIG_SWITCH_NUM; i++){
        doit_switch_set_onoff(i, onoff);
    }
    return OPRT_OK;
}

DOHOME_UINT8_T doit_switch_get_onoff(DOHOME_UINT8_T num){ 
    if (num > CONFIG_SWITCH_NUM-1){
        return 0;
    }
    return switch_state[num].onoff;
}

dohome_op_ret doit_switch_set_onoff(DOHOME_UINT8_T num, DOHOME_UINT8_T onoff){

    if(num > CONFIG_SWITCH_NUM-1){
        return OPRT_COM_ERROR;
    }

    if(wifi_led_is_realy){
#if CONFIG_LED_IS_REALY_WIFICFG_ALWAYS_FLASH

#else
        wifi_led_flash = 0;
#endif
    }

    if(switch_cfg[num].power_gpio.name != GPIO_NOT_USE){
		DOHOME_LOG_D("active lv:%d, gpio:%d, onoff:%d\n", switch_cfg[num].power_gpio.active_level, switch_cfg[num].power_gpio.name, onoff);
        if(switch_cfg[num].power_gpio.active_level){
            dohome_hal_gpio_write(switch_cfg[num].power_gpio.name, onoff?1:0);
        }else{
            dohome_hal_gpio_write(switch_cfg[num].power_gpio.name, onoff?0:1);
        }
    }
    
    if(switch_cfg[num].state_gpio.name != GPIO_NOT_USE){
        if((wifi_led_is_realy == 0) || (wifi_led_is_realy == 1 && switch_led_state == DT_SWITCH_LED_RELAY)){
            if(switch_cfg[num].state_gpio.active_level){
                dohome_hal_gpio_write(switch_cfg[num].state_gpio.name, onoff?1:0);
            }else{
                dohome_hal_gpio_write(switch_cfg[num].state_gpio.name, onoff?0:1);
            }
        }
    }
    switch_state[num].onoff = onoff;
    return OPRT_OK;
}

dohome_op_ret doit_switch_reverse(DOHOME_UINT8_T switch_id){
    DOHOME_UINT8_T i = 0;
    DOHOME_UINT8_T value = 0;

    if(switch_id < CONFIG_SWITCH_NUM){
        if(doit_switch_get_onoff(switch_id)){
            doit_switch_set_onoff(switch_id, 0);
        }else{
            doit_switch_set_onoff(switch_id, 1);
        }
    }else{
#if (defined(CONFIG_ALL_CONTROL_ENABLE) && CONFIG_ALL_CONTROL_ENABLE == 1)
        if(switch_id == DT_ALL_SWITCH_ID){
            value = 0;
            for (i = 0; i < CONFIG_SWITCH_NUM; i++) {
                value |= doit_switch_get_onoff(i);
            }

            for (i = 0; i < CONFIG_SWITCH_NUM; i++) {
                doit_switch_set_onoff(i, value?0:1);
            }
        } 
#endif

#if (defined(CONFIG_USB_CONTROL_ENABLE) && CONFIG_USB_CONTROL_ENABLE == 1)
        if(switch_id == DT_USB_SWITCH_ID){
            if(dohome_hal_gpio_read(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN)){
                dohome_hal_gpio_write(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN, 0);
            }else{
                dohome_hal_gpio_write(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN, 1);
            }
        }   
#endif
    }
    return OPRT_OK;
}

DOHOME_UINT8_T doit_switch_wifi_led_is_flash(void){
    return wifi_led_flash;
}

void doit_switch_wifi_led_start_flash(void){
    wifi_led_flash = 1;
}

void doit_switch_wifi_led_stop_flash(void){
    wifi_led_flash = 0;
    if(wifi_led_is_realy){
        //更新灯的状态
        for (DOHOME_UINT8_T i = 0; i < CONFIG_SWITCH_NUM; i++){
            doit_switch_set_onoff(i, switch_state[i].onoff);
        }
    }else{
        wifi_led_status = 0;
        if(CONFIG_WIFI_STATE_PIN != GPIO_NOT_USE){
            if(CONFIG_GPIO_STATE_ACTIVE_LEVEL){
                dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, 0);
            }else{
                dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, 1);
            }
        }
    }
}

void doit_switch_wifi_led_set_status(DOHOME_UINT8_T status){
    if(wifi_led_is_realy == 0){
        wifi_led_status = status;
        if(CONFIG_WIFI_STATE_PIN != GPIO_NOT_USE){
            if(switch_led_state == DT_SWITCH_LED_RELAY){
#if (defined(CONFIG_WIFI_STATE_REVERSE) && CONFIG_WIFI_STATE_REVERSE == 1)
                status = status?0:1;    
#endif
                if(CONFIG_GPIO_STATE_ACTIVE_LEVEL){
                    dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, status?1:0);
                }else{
                    dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, status?0:1);
                }
            }
        }
    }
}

void doit_switch_wifi_led_flash_loop(void){
    DOHOME_STATIC DOHOME_UINT8_T wifi_led_statue = CONFIG_GPIO_STATE_ACTIVE_LEVEL;
    if(wifi_led_flash){
        if(CONFIG_WIFI_STATE_PIN != GPIO_NOT_USE){
            if(wifi_led_statue){
                dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, 0);
            }else{
                dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, 1);
            }
        }
        wifi_led_statue = (wifi_led_statue+1)%2;
    }
}
dohome_op_ret doit_switch_btn_id_to_switch_id(DOHOME_UINT8_T btn_id, DOHOME_UINT8_T *switch_id){
    DOHOME_UINT8_T i = 0;
    for (i = 0; i < CONFIG_SWITCH_NUM; i++){
        if(btn_id == switch_cfg[i].key_gpio.name){
            *switch_id = i;
            return OPRT_OK;
        }
    }

#ifdef CONFIG_ALL_CONTROL_KEY_PIN
    if(btn_id == CONFIG_ALL_CONTROL_KEY_PIN){
        *switch_id = DT_ALL_SWITCH_ID;
        return OPRT_OK;
    }
#endif

#ifdef CL_CWITCH_USB_CONTROL_KEY_PIN
    if(btn_id == CL_CWITCH_USB_CONTROL_KEY_PIN){
        *switch_id = DT_USB_SWITCH_ID;
        return OPRT_OK;
    }
#endif
    return OPRT_COM_ERROR;
}

DOHOME_STATIC uint8_t common_btn_read(void *arg)
{
    DOHOME_STATIC DOHOME_UINT8_T flag[CONFIG_SWITCH_NUM+2] = {0}; 
    flex_button_t *btn = (flex_button_t *)arg;
    DOHOME_UINT8_T id_index = 0;
    DOHOME_UINT8_T switch_id = 0;

    doit_switch_btn_id_to_switch_id(btn->id, &switch_id);
    if(switch_id == DT_ALL_SWITCH_ID){
        id_index = CONFIG_SWITCH_NUM;
    }else if(switch_id == DT_USB_SWITCH_ID){
        id_index = CONFIG_SWITCH_NUM+1;
    }else{
        id_index = switch_id;
    }
    if(flag[id_index] < 10){
        flag[id_index]++;
        return !CONFIG_GPIO_KEY_ACTIVE_LEVEL;
    }else{
        if(flag[id_index] == 10){
            if(dohome_hal_gpio_read(btn->id) == !CONFIG_GPIO_KEY_ACTIVE_LEVEL){
                flag[id_index] = 255;
            }
            return !CONFIG_GPIO_KEY_ACTIVE_LEVEL;
        }
    }
    return dohome_hal_gpio_read(btn->id);
}

DOHOME_STATIC uint8_t common_plate_btn_read(void *arg)
{
    DOHOME_STATIC DOHOME_UINT8_T flag = 0; 
    flex_button_t *btn = (flex_button_t *)arg;
    
    if(flag < 10){
        flag++;
        return 1;
    }else if(flag == 10){
        if(dohome_hal_gpio_read(btn->id) == 0){
            flag = 255;
        }else{
            flag = 128;
        }
    }

    if(flag == 255){
        return ((dohome_hal_gpio_read(btn->id)==0)?1:0);
    }else{
        return dohome_hal_gpio_read(btn->id);
    }
}

DOHOME_STATIC void common_btn_evt_cb(void *arg)
{
    flex_button_t *btn = (flex_button_t *)arg;

    DOHOME_LOG_I("id: [%d]  event: [%d]  repeat: %d", btn->id, btn->event, btn->click_cnt);

    DOHOME_UINT8_T switch_id = 0;
    switch (btn->event)
    {
    case FLEX_BTN_PRESS_CLICK:
        doit_switch_btn_id_to_switch_id(btn->id, &switch_id);
        doit_switch_reverse(switch_id);
        break;
    case FLEX_BTN_PRESS_LONG_HOLD:

        break;
    default:
        break;
    }

    if(keys_event_cb){
        keys_event_cb(btn);
    }

}

#if (defined(CONFIG_ONOFF_PLATE_CONTROL_ENABLE) && CONFIG_ONOFF_PLATE_CONTROL_ENABLE == 1)
DOHOME_STATIC void common_plate_btn_evt_cb(void *arg)
{
    flex_button_t *btn = (flex_button_t *)arg;

    DOHOME_LOG_D("id: [%d]  event: [%d]  repeat: %d", btn->id, btn->event, btn->click_cnt);

    flex_button_t plate_btn = {0};
    DOHOME_UINT8_T switch_id = 0;
    switch (btn->event)
    {
    case FLEX_BTN_PRESS_DOWN:
    case FLEX_BTN_PRESS_CLICK:
    case FLEX_BTN_PRESS_SHORT_UP:
    case FLEX_BTN_PRESS_LONG_HOLD_UP:
        plate_btn.id = switch_cfg[0].key_gpio.name;
        plate_btn.event = FLEX_BTN_PRESS_CLICK;
#if (defined(CONFIG_USE_ONOFF_PLATE_CONFIG) && CONFIG_USE_ONOFF_PLATE_CONFIG == 1)
        if(onoff_plate_time){
            onoff_plate_count++;
            if(onoff_plate_count >= CONFIG_ONOFF_PLATE_CFG_COUNT){
                onoff_plate_time = 0;
                plate_btn.event = FLEX_BTN_PRESS_LONG_START;
            }
        }else{
            onoff_plate_time = 1;
            onoff_plate_count = 1;
        }
        DOHOME_LOG_D("id: [%d]  plate_count: [%d] time: [%d]", btn->id, onoff_plate_count, onoff_plate_time);
#endif
        doit_switch_btn_id_to_switch_id(plate_btn.id, &switch_id);
        doit_switch_reverse(switch_id);
        if(keys_event_cb){
            keys_event_cb(&plate_btn);
        }
        break;
    default:
        break;
    }
}
#endif

void doit_switch_button_scan(void){
    flex_button_scan(DOIT_LOOP_PERIOD);
#if (defined(CONFIG_USE_ONOFF_PLATE_CONFIG) && CONFIG_USE_ONOFF_PLATE_CONFIG == 1)
    if(onoff_plate_time > 0){
        onoff_plate_time += DOIT_LOOP_PERIOD;
        if(onoff_plate_time > CONFIG_ONOFF_PLATE_CFG_CLS_TIME_MS){
            onoff_plate_time = 0;
            onoff_plate_count = 0;
        }
    }
#endif
}


void doit_switch_set_boot_onoff(dt_switch_power_t *list, dt_switch_boot_state_e boot_state){

    switch_boot_state = boot_state;
    for (DOHOME_UINT8_T i = 0; i < CONFIG_SWITCH_NUM; i++)
    {
        if(switch_boot_state == DT_SWITCH_BOOT_OFF){
            doit_switch_set_onoff(i, 0);
        }else if(switch_boot_state == DT_SWITCH_BOOT_ON){
            doit_switch_set_onoff(i, 1);
        }else if(switch_boot_state == DT_SWITCH_BOOT_MEMORY){
            doit_switch_set_onoff(i, list[i].onoff);
        }else{
            doit_switch_set_onoff(i, 0);
            switch_boot_state = DT_SWITCH_BOOT_OFF;
        }
    }
}


void doit_switch_init(flex_button_response_callback event_cb){

    keys_event_cb = event_cb;

    for (DOHOME_UINT8_T i = 0; i < CONFIG_SWITCH_NUM; i++)
    {
        if(switch_cfg[i].state_gpio.name == CONFIG_WIFI_STATE_PIN){
            wifi_led_is_realy = 1;
        }

        dohome_hal_gpio_init(switch_cfg[i].power_gpio.name, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
		DOHOME_LOG_D("GPIO init pin: %d, dir: %d, attr: %d", switch_cfg[i].power_gpio.name, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
        if(switch_cfg[i].power_gpio.active_level){
            dohome_hal_gpio_write(switch_cfg[i].power_gpio.name, 0);
        }else{
            dohome_hal_gpio_write(switch_cfg[i].power_gpio.name, 1);
        }

        if(switch_cfg[i].state_gpio.name != GPIO_NOT_USE){
            dohome_hal_gpio_init(switch_cfg[i].state_gpio.name, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
            if(switch_cfg[i].state_gpio.active_level){
                dohome_hal_gpio_write(switch_cfg[i].state_gpio.name, 0);
            }else{
                dohome_hal_gpio_write(switch_cfg[i].state_gpio.name, 1);
            }
        }

        // doit_switch_set_onoff(i, 0);

        if(switch_cfg[i].key_gpio.name != GPIO_NOT_USE){
            if(CONFIG_GPIO_KEY_ACTIVE_LEVEL){
                dohome_hal_gpio_init(switch_cfg[i].key_gpio.name, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLLOW);
            }else{
                dohome_hal_gpio_init(switch_cfg[i].key_gpio.name, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLHIGH);
            }
            
            user_button[i].id = switch_cfg[i].key_gpio.name;
            user_button[i].usr_button_read = common_btn_read;
            user_button[i].cb = common_btn_evt_cb;
            user_button[i].pressed_logic_level = CONFIG_GPIO_KEY_ACTIVE_LEVEL;
            user_button[i].short_press_start_tick = FLEX_MS_TO_SCAN_CNT(1500);
            user_button[i].long_press_start_tick = FLEX_MS_TO_SCAN_CNT(CONFIG_KEY_CONTROL_LONG_TIME_MS);
            user_button[i].long_hold_start_tick = FLEX_MS_TO_SCAN_CNT(CONFIG_KEY_CONTROL_LONG_TIME_MS+100);
            flex_button_register(&user_button[i]);
        }
    }

#if (defined(CONFIG_ALL_CONTROL_ENABLE) && CONFIG_ALL_CONTROL_ENABLE == 1)
    if(CONFIG_ALL_CONTROL_KEY_PIN != GPIO_NOT_USE){
        DOHOME_STATIC flex_button_t all_button = {0};

        if(CONFIG_GPIO_KEY_ACTIVE_LEVEL){
            dohome_hal_gpio_init(CONFIG_ALL_CONTROL_KEY_PIN, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLLOW);
        }else{
            dohome_hal_gpio_init(CONFIG_ALL_CONTROL_KEY_PIN, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLHIGH);
        }

        all_button.id = CONFIG_ALL_CONTROL_KEY_PIN;
        all_button.usr_button_read = common_btn_read;
        all_button.cb = common_btn_evt_cb;
        all_button.pressed_logic_level = CONFIG_GPIO_KEY_ACTIVE_LEVEL;
        all_button.short_press_start_tick = FLEX_MS_TO_SCAN_CNT(1500);
        all_button.long_press_start_tick = FLEX_MS_TO_SCAN_CNT(CONFIG_KEY_CONTROL_LONG_TIME_MS);
        all_button.long_hold_start_tick = FLEX_MS_TO_SCAN_CNT(CONFIG_KEY_CONTROL_LONG_TIME_MS+100);
        flex_button_register(&all_button);
    }
#endif

#if (defined(CONFIG_USB_CONTROL_ENABLE) && CONFIG_USB_CONTROL_ENABLE == 1)
    if(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN != GPIO_NOT_USE){
        dohome_hal_gpio_init(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
        if(CONFIG_USB_CONTROL_TYPE == DOIT_CONTROL_TYPE_ON){
            dohome_hal_gpio_write(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN, CONFIG_GPIO_POWER_ACTIVE_LEVEL);
        }else{
            dohome_hal_gpio_write(CONFIG_CL_SWITCH_USB_CONTROL_RELAY_PIN, CONFIG_GPIO_POWER_ACTIVE_LEVEL?0:1);
        }
    }

    if(CONFIG_USB_CONTROL_KEY_PIN != GPIO_NOT_USE){
        DOHOME_STATIC flex_button_t usb_button ={0};

        if(CONFIG_GPIO_KEY_ACTIVE_LEVEL){
            dohome_hal_gpio_init(CONFIG_USB_CONTROL_KEY_PIN, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLLOW);
        }else{
            dohome_hal_gpio_init(CONFIG_USB_CONTROL_KEY_PIN, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLHIGH);
        }

        usb_button.id = CONFIG_USB_CONTROL_KEY_PIN;
        usb_button.usr_button_read = common_btn_read;
        usb_button.cb = common_btn_evt_cb;
        usb_button.pressed_logic_level = CONFIG_GPIO_KEY_ACTIVE_LEVEL;
        usb_button.short_press_start_tick = FLEX_MS_TO_SCAN_CNT(1500);
        usb_button.long_press_start_tick = FLEX_MS_TO_SCAN_CNT(CONFIG_KEY_CONTROL_LONG_TIME_MS);
        usb_button.long_hold_start_tick = FLEX_MS_TO_SCAN_CNT(CONFIG_KEY_CONTROL_LONG_TIME_MS+100);
        flex_button_register(&usb_button);
    }
#endif

#if (defined(CONFIG_BACKLIGHT_ENABLE) && CONFIG_BACKLIGHT_ENABLE == 1)
    dohome_hal_gpio_init(CONFIG_BACKLIGHT_PIN, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
    if(CONFIG_BACKLIGHT_ACTIVE_LEVEL){
        dohome_hal_gpio_write(CONFIG_BACKLIGHT_PIN, CONFIG_BACKLIGHT_ACTIVE_LEVEL);
    }else{
        dohome_hal_gpio_write(CONFIG_BACKLIGHT_PIN, CONFIG_GPIO_POWER_ACTIVE_LEVEL?0:1);
    }
#endif

#if (defined(CONFIG_ONOFF_PLATE_CONTROL_ENABLE) && CONFIG_ONOFF_PLATE_CONTROL_ENABLE == 1)
    DOHOME_STATIC flex_button_t plate_button = {0};
    dohome_hal_gpio_init(PLATE_CODE_1, DOHOME_GPIO_INPUT, DOHOME_GPIO_PULLHIGH);
    plate_button.id = PLATE_CODE_1;
    plate_button.usr_button_read = common_plate_btn_read;
    plate_button.cb = common_plate_btn_evt_cb;
    plate_button.pressed_logic_level = 0;
    plate_button.short_press_start_tick = FLEX_MS_TO_SCAN_CNT(1500);
    plate_button.long_press_start_tick = FLEX_MS_TO_SCAN_CNT(3000);
    plate_button.long_hold_start_tick = FLEX_MS_TO_SCAN_CNT(3100);
    flex_button_register(&plate_button);
#endif
    
    if(wifi_led_is_realy == 0){
        if(CONFIG_WIFI_STATE_PIN != GPIO_NOT_USE){
            dohome_hal_gpio_init(CONFIG_WIFI_STATE_PIN, DOHOME_GPIO_OUTPUT, DOHOME_GPIO_FLOATING);
            if(CONFIG_GPIO_STATE_ACTIVE_LEVEL){
                dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, 0);
            }else{
                dohome_hal_gpio_write(CONFIG_WIFI_STATE_PIN, 1);
            }
            doit_switch_wifi_led_set_status(0);
        }
    }

}
